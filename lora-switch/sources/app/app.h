/**
 ******************************************************************************
 * @author: ThanNT
 * @date:   13/08/2016
 ******************************************************************************
**/

#ifndef APP_H
#define APP_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "app_eeprom.h"
#include "app_data.h"

/*****************************************************************************/
/*  life task define
 */
/*****************************************************************************/
/* define timer */
#define AC_LIFE_TASK_TIMER_LED_LIFE_INTERVAL		(1000)

/* define signal */
#define AC_LIFE_SYSTEM_CHECK						(0)

/*****************************************************************************/
/*  shell task define
 */
/*****************************************************************************/
/* define timer */

/* define signal */
#define AC_SHELL_LOGIN_CMD							(0)
#define AC_SHELL_REMOTE_CMD							(1)

/*****************************************************************************/
/*  time task define
 */
/*****************************************************************************/
/* define timer */
#define AC_RTC_UPDATE_TIME_INTERVAL					(120000)	/* 2' */
#define AC_RTC_INTINAL_INTERVAL						(100)		/* 100ms */

/* define signal */
#define AC_TIME_RTC_INTINAL							(1)
#define AC_TIME_RTC_UPDATE_STATUS					(2)
#define AC_TIME_STATUS_RES_OK						(3)
#define AC_TIME_STATUS_RES_ERR						(4)
#define AC_TIME_RTC_SETTING_REQ						(5)
#define AC_TIME_AC_SENSOR_TEMPERATURE_STATUS_RES	(6)

/* private define */
#define AC_RTC_STICK_TIME_MIN						(AC_RTC_UPDATE_TIME_INTERVAL / 60000)

/*****************************************************************************/
/* if task define
 */
/*****************************************************************************/
/* define timer */
#define AC_IF_TIMER_PACKET_TIMEOUT_INTERVAL			(500)

/* define signal */
#define AC_IF_PURE_MSG_IN							(1)
#define AC_IF_PURE_MSG_OUT							(2)
#define AC_IF_PURE_MSG_OUT_RES_OK					(3)
#define AC_IF_PURE_MSG_OUT_RES_NG					(4)
#define AC_IF_COMMON_MSG_IN							(5)
#define AC_IF_COMMON_MSG_OUT						(6)
#define AC_IF_COMMON_MSG_OUT_RES_OK					(7)
#define AC_IF_COMMON_MSG_OUT_RES_NG					(8)
#define AC_IF_PACKET_TIMEOUT						(9)

/* sx1276 task define*/
/*signal*/
#define LORA_SX1276_TXTIMEROUT						(0)
#define LORA_SX1276_RXTIMEROUT						(1)
#define LORA_SX1276_RXTIMEROUT_SYNCWORD				(2)

/* task sensor define*/
/*timer*/
#define LORA_SWITCH_SYNC_TIME_REQ_INTERVAL			(30*60*1000)
#define LORA_SWITCH_CHECK_SETTING_TIME_INTERVAL		(10000)
/*signal*/
#define LORA_SWITCH_INIT							(0)
#define LORA_SWITCH_SYNC_TIME_REQ					(1)
#define LORA_SWITCH_TXDONE							(2)
#define LORA_SWITCH_TXTIMEOUT						(3)
#define LORA_SWITCH_RXDONE							(4)
#define LORA_SWITCH_RXTIMEOUT						(5)
#define LORA_SWITCH_RXERR							(6)
#define LORA_SWITCH_FHSS_CHANGE_CHAN				(7)
#define LORA_SWITCH_CADDONE							(8)
#define LORA_SWITCH_CONTROL							(9)
#define LORA_SWITCH_STATUS							(10)
#define LORA_SWITCH_SET_TIME						(11)
#define LORA_SWITCH_CHECK_SETTING_TIMEOUT			(12)

/*********/


/*****************************************************************************/
/*  global define variable
 */
/*****************************************************************************/
#define APP_OK									(0x00)
#define APP_NG									(0x01)

#define APP_FLAG_OFF							(0x00)
#define APP_FLAG_ON								(0x01)

/*****************************************************************************/
/*  app function declare
 */
/*****************************************************************************/
extern const char* app_version;

extern int  main_app();

#ifdef __cplusplus
}
#endif

#endif //APP_H
