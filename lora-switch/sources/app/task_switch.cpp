#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"
#include "../ak/timer.h"
#include "../common/utils.h"

#include "../app/app.h"
#include "../app/app_dbg.h"
#include "../app/task_list.h"

#include "../sys/sys_ctrl.h"
#include "../platform/stm32l/io_cfg.h"

#include "../driver/ds1302/DS1302.h"

#include "../platform/stm32l/sx1276_cfg.h"
#include "../lora/radio.h"
#include "../lora/task_sx1276.h"

#include "task_switch.h"


#define MY_RX_TIMEOUT_VALUE                            0

/*!
 * Radio events function pointer
 */

static void on_tx_done( void );
static void on_rx_done( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr );
static void on_tx_timeout( void );
static void on_rx_timeout( void );
static void on_rx_error( void );

DS1302 rtc_ds1302;
static RadioEvents_t radioevents;


void task_switch(ak_msg_t* msg){

	switch (msg->sig) {

	case LORA_SWITCH_INIT:{
		APP_PRINT("LORA_SWITCH_INIT\n\n");

		//		Time t(2000,1,1,9,35,0,Time::Day::kSaturday);
		//		rtc_ds1302.time(t);

		radioevents.TxDone = on_tx_done;
		radioevents.RxDone = on_rx_done;
		radioevents.TxTimeout = on_tx_timeout;
		radioevents.RxTimeout = on_rx_timeout;
		radioevents.RxError = on_rx_error;

		Radio.Init( &radioevents );

		Radio.SetChannel( RF_FREQUENCY );

		Radio.SetTxConfig( MODEM_LORA, TX_OUTPUT_POWER, 0, LORA_BANDWIDTH,
						   LORA_SPREADING_FACTOR, LORA_CODINGRATE,
						   LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON,
						   true, 0, 0, LORA_IQ_INVERSION_ON, 3000 );

		Radio.SetRxConfig( MODEM_LORA, LORA_BANDWIDTH, LORA_SPREADING_FACTOR,
						   LORA_CODINGRATE, 0, LORA_PREAMBLE_LENGTH,
						   LORA_SYMBOL_TIMEOUT, LORA_FIX_LENGTH_PAYLOAD_ON,
						   0, true, 0, 0, LORA_IQ_INVERSION_ON, true );

		Radio.Rx( MY_RX_TIMEOUT_VALUE );


		/* gateway sync timer */
		timer_set(LORA_TASK_SWITCH_ID, LORA_SWITCH_SYNC_TIME_REQ, 1000, TIMER_ONE_SHOT);

	}
		break;

	case LORA_SWITCH_SYNC_TIME_REQ:{
		APP_DBG("LORA_SWITCH_SYNC_TIME\n\n");

		Time t = rtc_ds1302.time();

		APP_DBG("time.hour: %d\n", t.hr);
		APP_DBG("time.min: %d\n", t.min);
		APP_DBG("time.sec: %d\n\n", t.sec);

		lora_pkg_t s_pkg;
		s_pkg.hdr.app_id = STREET_LIGHT_APP_ID;
		s_pkg.hdr.src_addr = SWITCH_ADDR;
		s_pkg.hdr.des_addr = CONCENTRATOR_ADDR;
		s_pkg.hdr.type = SYNC_TYPE;
		s_pkg.data.sync_data.hour = t.hr;
		s_pkg.data.sync_data.min = t.min;
		Radio.Send((uint8_t*)&s_pkg, sizeof(lora_pkg_t));

		/* gateway sync timer */
		timer_set(LORA_TASK_SWITCH_ID, LORA_SWITCH_SYNC_TIME_REQ, LORA_SWITCH_SYNC_TIME_REQ_INTERVAL, TIMER_PERIODIC);

	}
		break;

	case LORA_SWITCH_SET_TIME:{
		APP_DBG("LORA_SWITCH_SET_TIME\n\n");

		lora_pkg_t r_pkg;
		mem_cpy(&r_pkg, get_data_common_msg(msg), sizeof(lora_pkg_t));

		APP_DBG("[%08x:%08x:%08x:%d][%d:%d]\n\n",
				r_pkg.hdr.app_id,
				r_pkg.hdr.src_addr,
				r_pkg.hdr.des_addr,
				r_pkg.hdr.type,
				r_pkg.data.sync_data.hour,
				r_pkg.data.sync_data.min);

		Time t(2099,1,1, (uint8_t)r_pkg.data.sync_data.hour, (uint8_t)r_pkg.data.sync_data.min,0,Time::Day::kMonday);
		rtc_ds1302.time(t);

	}
		break;

	case LORA_SWITCH_CHECK_SETTING_TIMEOUT:{
		APP_DBG("LORA_SWITCH_CHECK_SETTING_TIMEOUT\n\n");

		Time now = rtc_ds1302.time();
		APP_DBG("NOW:[%d:%d]\n\n",now.hr, now.min);

		ctr_data_t s_ctr;
		eeprom_read(EEPROM_SETTING_CTR_ADDR, (uint8_t*)&s_ctr, sizeof(ctr_data_t));

		if(s_ctr.mod == AUTO_MOD){
			//	APP_DBG("s_ctr.mod=AUTO_MOD\n\n");

			APP_DBG("READ EEP:[%d][%d:%d][%d:%d]\n\n",
					s_ctr.mod,
					s_ctr.mod_data.time_set.start_hour, s_ctr.mod_data.time_set.start_min,
					s_ctr.mod_data.time_set.end_hour, s_ctr.mod_data.time_set.end_min);

			uint32_t s_start_time = s_ctr.mod_data.time_set.start_hour*60 + s_ctr.mod_data.time_set.start_min;
			uint32_t s_end_time = s_ctr.mod_data.time_set.end_hour*60 + s_ctr.mod_data.time_set.end_min;
			uint32_t n_time = now.hr*60 + now.min;

			if(n_time >= s_end_time && n_time < s_start_time){
				if(read_status_relay()){
					relay_off();
					APP_DBG("RELAY OFF\n\n");
				}
			}
			else{
				if(!read_status_relay()){
					relay_on();
					APP_DBG("RELAY ON\n\n");
				}
			}

		}
		else {
			APP_DBG("READ EEP:[%d][%d]\n\n", s_ctr.mod, s_ctr.mod_data.power);

			if( s_ctr.mod_data.power == 1 ){
				if(!read_status_relay()){
					relay_on();
					APP_DBG("RELAY ON\n\n");
				}
			}
			else if(s_ctr.mod_data.power == 0){
				if(read_status_relay()){
					relay_off();
					APP_DBG("RELAY OFF\n\n");
				}
			}

		}

	}
		break;

	case LORA_SWITCH_STATUS:{
		APP_DBG("LORA_SWITCH_STATUS\n\n");

		lora_pkg_t r_pkg;
		mem_cpy(&r_pkg, get_data_common_msg(msg), sizeof(lora_pkg_t));

		//		APP_DBG("[%08x:%08x:%08x:%d][%d:%d]\n\n",
		//				r_pkg.hdr.app_id,
		//				r_pkg.hdr.src_addr,
		//				r_pkg.hdr.des_addr,
		//				r_pkg.hdr.type,
		//				r_pkg.data.sen_data.curr_sen,
		//				r_pkg.data.sen_data.ntc_sen);

		r_pkg.hdr.des_addr = CONCENTRATOR_ADDR;
		Radio.Send((uint8_t*)&r_pkg, sizeof(lora_pkg_t));
	}
		break;

	case LORA_SWITCH_CONTROL:{
		APP_DBG("LORA_SWITCH_CONTROL\n\n");

		lora_pkg_t r_pkg;
		mem_cpy(&r_pkg, get_data_common_msg(msg), sizeof(lora_pkg_t));

		APP_DBG("[%08x:%08x:%08x:%d][%d]\n\n",
				r_pkg.hdr.app_id,
				r_pkg.hdr.src_addr,
				r_pkg.hdr.des_addr,
				r_pkg.hdr.type,
				r_pkg.data.ctr_data.mod);

		eeprom_write(EEPROM_SETTING_CTR_ADDR, (uint8_t*)&r_pkg.data.ctr_data.mod, sizeof(ctr_data_t));

		if(r_pkg.data.ctr_data.mod == MANUAL_MOD){
			ctr_data_t r_ctr;
			eeprom_read(EEPROM_SETTING_CTR_ADDR, (uint8_t*)&r_ctr, sizeof(ctr_data_t));
			APP_DBG("Write Done, Read again:[%d][%d]\n\n",
					r_ctr.mod,
					r_ctr.mod_data.power);

			if( r_pkg.data.ctr_data.mod_data.power == 1 ){
				relay_on();
				APP_DBG("RELAY ON\n\n");
			}
			else if(r_pkg.data.ctr_data.mod_data.power == 0){
				relay_off();
				APP_DBG("RELAY OFF\n\n");
			}
		}
		else if(r_pkg.data.ctr_data.mod == AUTO_MOD){
			ctr_data_t r_ctr;
			eeprom_read(EEPROM_SETTING_CTR_ADDR, (uint8_t*)&r_ctr, sizeof(ctr_data_t));

			APP_DBG("Write Done, Read again:[%d][%d:%d][%d:%d]\n\n",
					r_ctr.mod,
					r_ctr.mod_data.time_set.start_hour, r_ctr.mod_data.time_set.start_min,
					r_ctr.mod_data.time_set.end_hour, r_ctr.mod_data.time_set.end_min);
		}

		/*response again*/
//		r_pkg.hdr.app_id = STREET_LIGHT_APP_ID;
		r_pkg.hdr.src_addr = SWITCH_ADDR;
		r_pkg.hdr.des_addr = CONCENTRATOR_ADDR;
//		r_pkg.hdr.type = CONTROL_TYPE;
		Radio.Send((uint8_t*)&r_pkg, sizeof(lora_pkg_t));

	}
		break;

	case LORA_SWITCH_RXDONE:{
		APP_DBG("LORA_SWITCH_RXDONE\n\n");

		//		APP_DBG("[%08x:%08x:%08x:%d]\n\n",
		//				r_pkg->hdr.app_id,
		//				r_pkg->hdr.src_addr,
		//				r_pkg->hdr.des_addr,
		//				r_pkg->hdr.type);

		lora_pkg_t r_pkg;
		mem_cpy(&r_pkg, get_data_common_msg(msg), sizeof(lora_pkg_t));

		if(r_pkg.hdr.app_id == STREET_LIGHT_APP_ID){
			if( r_pkg.hdr.des_addr == SWITCH_ADDR){

				if(r_pkg.hdr.type == SENSOR_TYPE){
					msg_inc_ref_count(msg);
					set_msg_sig(msg, LORA_SWITCH_STATUS);
					task_post(LORA_TASK_SWITCH_ID, msg);
				}
				else if(r_pkg.hdr.type == CONTROL_TYPE){
					msg_inc_ref_count(msg);
					set_msg_sig(msg, LORA_SWITCH_CONTROL);
					task_post(LORA_TASK_SWITCH_ID, msg);
				}
				else if(r_pkg.hdr.type == SYNC_TYPE){
					msg_inc_ref_count(msg);
					set_msg_sig(msg, LORA_SWITCH_SET_TIME);
					task_post(LORA_TASK_SWITCH_ID, msg);
				}
			}
		}

		Radio.Rx( MY_RX_TIMEOUT_VALUE );

	}
		break;

	case LORA_SWITCH_TXDONE:{
		APP_DBG("LORA_SWITCH_TXDONE\n\n");
		Radio.Rx( MY_RX_TIMEOUT_VALUE );
	}
		break;

	case LORA_SWITCH_TXTIMEOUT:{
		APP_DBG("LORA_SWITCH_TXTIMEOUT\n\n");
		Radio.Rx( MY_RX_TIMEOUT_VALUE );
	}
		break;

	case LORA_SWITCH_RXTIMEOUT:{
		APP_DBG("LORA_SWITCH_RXTIMEOUT\n\n");
		Radio.Rx( MY_RX_TIMEOUT_VALUE );
	}
		break;

	case LORA_SWITCH_RXERR:{
		APP_DBG("LORA_SWITCH_RXERR\n\n");
		Radio.Rx( MY_RX_TIMEOUT_VALUE );
	}
		break;

	default:
		break;
	}

}


void on_tx_done( void )
{
	Radio.Sleep( );
	ak_msg_t* msg = get_pure_msg();
	set_msg_sig(msg, LORA_SWITCH_TXDONE);
	task_post(LORA_TASK_SWITCH_ID, msg);
}

void on_tx_timeout( void )
{
	Radio.Sleep( );
	ak_msg_t* msg = get_pure_msg();
	set_msg_sig(msg, LORA_SWITCH_TXTIMEOUT);
	task_post(LORA_TASK_SWITCH_ID, msg);
}

void on_rx_done( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr )
{
	Radio.Sleep( );

	//APP_DBG("size:%d, rssi:%d, snr:%d\n\n", size, rssi, snr);

	ak_msg_t* smsg = get_common_msg();
	set_data_common_msg(smsg, payload, size);
	set_msg_sig(smsg, LORA_SWITCH_RXDONE);
	task_post(LORA_TASK_SWITCH_ID, smsg);
}

void on_rx_timeout( void )
{
	Radio.Sleep( );
	ak_msg_t* msg = get_pure_msg();
	set_msg_sig(msg, LORA_SWITCH_RXTIMEOUT);
	task_post(LORA_TASK_SWITCH_ID, msg);
}

void on_rx_error( void )
{
	Radio.Sleep( );
	ak_msg_t* msg = get_pure_msg();
	set_msg_sig(msg, LORA_SWITCH_RXERR);
	task_post(LORA_TASK_SWITCH_ID, msg);
}
