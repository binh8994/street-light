#include "../driver/button/button.h"

#include "app_bsp.h"
#include "app_dbg.h"

button_t btn_mode;
button_t btn_up;
button_t btn_down;

void btn_mode_callback(void* b) {
	button_t* me_b = (button_t*)b;
	switch (me_b->state) {
	case BUTTON_SW_STATE_RELEASE: {
		APP_DBG("[btn_mode_callback] BUTTON_SW_STATE_RELEASE\n");
	}
		break;

	case BUTTON_SW_STATE_SHORT_HOLD_PRESS: {
		APP_DBG("[btn_mode_callback] BUTTON_SW_STATE_SHORT_HOLD_PRESS\n");
	}
		break;

	case BUTTON_SW_STATE_SHORT_RELEASE_PRESS: {
		APP_DBG("[btn_mode_callback] BUTTON_SW_STATE_SHORT_RELEASE_PRESS\n");
	}
		break;

	case BUTTON_SW_STATE_LONG_PRESS: {
		APP_DBG("[btn_mode_callback] BUTTON_SW_STATE_LONG_PRESS\n");
	}
		break;

	default:
		break;
	}
}

void btn_up_callback(void* b) {
	button_t* me_b = (button_t*)b;
	switch (me_b->state) {
	case BUTTON_SW_STATE_RELEASE: {
		APP_DBG("[btn_up_callback] BUTTON_SW_STATE_RELEASE\n");
	}
		break;

	case BUTTON_SW_STATE_SHORT_HOLD_PRESS: {
		APP_DBG("[btn_up_callback] BUTTON_SW_STATE_SHORT_HOLD_PRESS\n");
	}
		break;

	case BUTTON_SW_STATE_SHORT_RELEASE_PRESS: {
		APP_DBG("[btn_up_callback] BUTTON_SW_STATE_SHORT_RELEASE_PRESS\n");
	}
		break;

	case BUTTON_SW_STATE_LONG_PRESS: {
		APP_DBG("[btn_up_callback] BUTTON_SW_STATE_LONG_PRESS\n");
	}
		break;

	default:
		break;
	}
}

void btn_down_callback(void* b) {
	button_t* me_b = (button_t*)b;
	switch (me_b->state) {
	case BUTTON_SW_STATE_RELEASE: {
		APP_DBG("[btn_down_callback] BUTTON_SW_STATE_RELEASE\n");
	}
		break;

	case BUTTON_SW_STATE_SHORT_HOLD_PRESS: {
		APP_DBG("[btn_down_callback] BUTTON_SW_STATE_SHORT_HOLD_PRESS\n");
	}
		break;

	case BUTTON_SW_STATE_SHORT_RELEASE_PRESS: {
		APP_DBG("[btn_down_callback] BUTTON_SW_STATE_SHORT_RELEASE_PRESS\n");
	}
		break;

	case BUTTON_SW_STATE_LONG_PRESS: {
		APP_DBG("[btn_down_callback] BUTTON_SW_STATE_LONG_PRESS\n");
	}
		break;

	default:
		break;
	}
}
