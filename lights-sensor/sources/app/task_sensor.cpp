#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"
#include "../ak/timer.h"

#include "../app/app.h"
#include "../app/task_list.h"

#include "../sys/sys_ctrl.h"
#include "../platform/stm32l/io_cfg.h"
#include "app.h"
#include "app_dbg.h"

#include "../platform/stm32l/sx1276_cfg.h"
#include "../lora/radio.h"
#include "../lora/task_sx1276.h"

#include "task_sensor.h"


#define MY_RX_TIMEOUT_VALUE                            0

/*!
 * Radio events function pointer
 */

static void on_tx_done( void );
static void on_rx_done( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr );
static void on_tx_timeout( void );
static void on_rx_timeout( void );
static void on_rx_error( void );

static RadioEvents_t radioevents;
static EnergyMonitor ct_sensor;
//static THERMISTOR ntc_sensor(NTC_ADC_CHANEL, 10000, 3435, 10000);

OWire ds2411;
uint8_t rom_buf[8] = {0x00, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00};

uint16_t current = 0;
uint16_t temperature = 0;


void task_sensor(ak_msg_t* msg){

	switch (msg->sig) {

	case LORA_SENSOR_INIT:{

		APP_PRINT("LORA_SENSOR_INIT\n\n");

		//		/* read ROM from DS2411 */
		//		OWInit(&ds2411,DS2411_IO_PORT,DS2411_IO_PIN);
		//		if(OWReset(&ds2411)){
		//			APP_PRINT("DS2411 OK\n");
		//			OWWrite(&ds2411, 0x33);
		//			OWRead_bytes(&ds2411,rom_buf,8);

		//			APP_PRINT("Family code: %02x\n", rom_buf[0]);
		//			APP_PRINT("ROM code: %02x %02x %02x %02x %02x %02x\n", rom_buf[1], rom_buf[2], rom_buf[3], rom_buf[4], rom_buf[5], rom_buf[6]);
		//			APP_PRINT("CRC: %02x\n\n", rom_buf[7]);
		//		}
		//		else {
		//			APP_PRINT("DS2411 FAIL\n\n");
		//		}

		//		ntc_sensor.begin();

		/* set chanel for CT sensor */
		ct_sensor.current(CT_ADC_CHANEL, (double)2000/(double)220); /* 2000/R */

		radioevents.TxDone = on_tx_done;
		radioevents.RxDone = on_rx_done;
		radioevents.TxTimeout = on_tx_timeout;
		radioevents.RxTimeout = on_rx_timeout;
		radioevents.RxError = on_rx_error;

		Radio.Init( &radioevents );

		Radio.SetChannel( RF_FREQUENCY );

		Radio.SetTxConfig( MODEM_LORA, TX_OUTPUT_POWER, 0, LORA_BANDWIDTH,
						   LORA_SPREADING_FACTOR, LORA_CODINGRATE,
						   LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON,
						   true, 0, 0, LORA_IQ_INVERSION_ON, 3000 );

		Radio.SetRxConfig( MODEM_LORA, LORA_BANDWIDTH, LORA_SPREADING_FACTOR,
						   LORA_CODINGRATE, 0, LORA_PREAMBLE_LENGTH,
						   LORA_SYMBOL_TIMEOUT, LORA_FIX_LENGTH_PAYLOAD_ON,
						   0, true, 0, 0, LORA_IQ_INVERSION_ON, true );

		//timer_set(LORA_TASK_SENSOR_ID, LORA_SENSOR_CYCLE, LORA_SENSOR_CYCLE_INTERVAL, TIMER_PERIODIC);
		Radio.Rx( MY_RX_TIMEOUT_VALUE );
	}
		break;

	case LORA_SENSOR_CYCLE:{

		APP_PRINT("LORA_SENSOR_CYCLE\n\n");

		current = (uint16_t)(1000 * ct_sensor.calcIrms(LORA_SAMPLE_CT_SENSOR));
		APP_PRINT("Current: %d\n\n", current);

		//temperature = ntc_sensor->read();
		//APP_PRINT("Temperature: %d\n\n", temperature);

		lora_pkg_t s_pkg;
		s_pkg.hdr.app_id = STREET_LIGHT_APP_ID;
		s_pkg.hdr.src_addr = 0x00000002;
		s_pkg.hdr.des_addr = SWITCH_ADDR;
		s_pkg.hdr.type = SENSOR_TYPE;
		s_pkg.data.sen_data.curr_sen = current;
		s_pkg.data.sen_data.ntc_sen = temperature;


//		Radio.SetTxConfig( MODEM_LORA, TX_OUTPUT_POWER, 0, LORA_BANDWIDTH,
//						   LORA_SPREADING_FACTOR, LORA_CODINGRATE,
//						   LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON,
//						   true, 0, 0, LORA_IQ_INVERSION_ON, 3000 );

		Radio.Send( (uint8_t*)&s_pkg, sizeof(lora_pkg_t) );

	}
		break;

	case LORA_SENSOR_TXDONE:{
		APP_DBG("LORA_SENSOR_TXDONE\n\n");
	}
		break;

	case LORA_SENSOR_TXTIMEOUT:{
		APP_DBG("LORA_SENSOR_TXTIMEOUT\n\n");
	}
		break;

	case LORA_SENSOR_RXDONE:{
		APP_DBG("LORA_SENSOR_RXDONE\n\n");
		Radio.Rx( MY_RX_TIMEOUT_VALUE );
	}
		break;

	case LORA_SENSOR_RXTIMEOUT:{
		APP_DBG("LORA_SENSOR_RXTIMEOUT\n\n");
		Radio.Rx( MY_RX_TIMEOUT_VALUE );
	}
		break;

	case LORA_SENSOR_RXERR:{
		APP_DBG("LORA_SENSOR_RXERR\n\n");
		Radio.Rx( MY_RX_TIMEOUT_VALUE );
	}
		break;

	default:
		break;
	}

}


void on_tx_done( void )
{
	Radio.Sleep( );
	ak_msg_t* msg = get_pure_msg();
	set_msg_sig(msg, LORA_SENSOR_TXDONE);
	task_post(LORA_TASK_SENSOR_ID, msg);
}

void on_tx_timeout( void )
{
	Radio.Sleep( );
	ak_msg_t* msg = get_pure_msg();
	set_msg_sig(msg, LORA_SENSOR_TXTIMEOUT);
	task_post(LORA_TASK_SENSOR_ID, msg);
}

void on_rx_done( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr )
{
	Radio.Sleep( );
	ak_msg_t* msg = get_pure_msg();
	set_msg_sig(msg, LORA_SENSOR_RXDONE);
	task_post(LORA_TASK_SENSOR_ID, msg);

	//lora_pkg_t r_pkg;
	//memcpy((uint8_t*)&r_pkg, payload, sizeof(lora_pkg_t));
	//APP_DBG("size:%d, rssi:%d, snr:%d\n\n", size, rssi, snr);

	//APP_DBG("[%08x:%08x:%02x:%04x:%d:%d]\n\n",
	//		r_pkg.hdr.src_addr,
	//		r_pkg.hdr.des_addr,
	//		r_pkg.hdr.type,
	//		r_pkg.hdr.chksum,
	//		r_pkg.data.sen_data.curr_sen,
	//		r_pkg.data.sen_data.ntc_sen);
}

void on_rx_timeout( void )
{
	Radio.Sleep( );
	ak_msg_t* msg = get_pure_msg();
	set_msg_sig(msg, LORA_SENSOR_RXTIMEOUT);
	task_post(LORA_TASK_SENSOR_ID, msg);
}

void on_rx_error( void )
{
	Radio.Sleep( );
	ak_msg_t* msg = get_pure_msg();
	set_msg_sig(msg, LORA_SENSOR_RXERR);
	task_post(LORA_TASK_SENSOR_ID, msg);
}
