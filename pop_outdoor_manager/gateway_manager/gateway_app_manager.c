#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define GET_CMD_BUFFER_LEN		128

int main() {
	int number_apps_is_opened = 0;
	FILE *fp;
	char ret_str[GET_CMD_BUFFER_LEN];
	memset(ret_str, 0, GET_CMD_BUFFER_LEN);

	fp = popen("pidof /usr/local/bin/outdoor_master | wc -w", "r");
	if (fp == NULL) {
		exit(1);
	}

	while (fgets(ret_str, GET_CMD_BUFFER_LEN - 1, fp) != NULL);

	pclose(fp);

	number_apps_is_opened = atoi(ret_str);

	switch (number_apps_is_opened) {
	case 0: {
		system("echo \"restart service outdoor_master $(date)\" >> /root/fpt_gateway/log/gateway_app_manager.txt");
		system("/usr/local/bin/outdoor_master");
	}
		break;

	case 1: {
		/* printf("app is running OK\n"); */
	}
		break;

	default: {
		system("echo \"remove service outdoor_master $(date)\" >> /root/fpt_gateway/log/gateway_app_manager.txt");
		system("kill $(pidof /usr/local/bin/outdoor_master | awk '{print $1}')");
	}
		break;
	}

	return 0;
}
