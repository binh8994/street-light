#include <stdarg.h>

#include "../platform/msp430g2553.h"

#include "serial.h"
#include "app.h"

serial_t serial;
lora_message_t lora_msg;
static void rx_frame_parser(uint8_t data);
static uint8_t rx_frame_state = SOP_STATE;

usb_stick_msg_frame_mng_t rev_usb_stick_msg_frame_mng;

void serial_init() {
	P1SEL		|= BIT1 + BIT2 ;	// P1.1 = RXD, P1.2=TXD
	P1SEL2		|= BIT1 + BIT2 ;	// P1.1 = RXD, P1.2=TXD
	UCA0CTL1	|= UCSSEL_2;		// SMCLK
	UCA0BR0		= 104;				// 1MHz 9600
	UCA0BR1		= 0;				// 1MHz 9600
	UCA0MCTL	= UCBRS0;			// Modulation UCBRSx = 1
	UCA0CTL1	&= ~UCSWRST;		// **Initialize USCI state machine**
	IE2			|= UCA0RXIE;		// Enable USCI_A0 RX interrupt
}

void __attribute__ ((interrupt(USCIAB0RX_VECTOR))) USCI0RX_ISR (void) {
	uint8_t c = 0;
	uint8_t *data;
	data=(uint8_t*)&lora_msg;

	c = UCA0RXBUF;
	rx_frame_parser(c);
}

void serial_write(uint8_t* data, uint16_t len) {
	uint8_t i;
	for (i = 0; i < len; i++) {
		while (!(IFG2&UCA0TXIFG));
		UCA0TXBUF = data[i];
	}
}

void xputc(uint8_t c) {
	if (c == (uint8_t)'\n') {
		while (!(IFG2&UCA0TXIFG));
		UCA0TXBUF = '\r';
	}

	while (!(IFG2&UCA0TXIFG));
	UCA0TXBUF = c;
}

void serial_printf(const char* fmt, ...) {
	va_list va_args;
	uint32_t num, digit;
	int32_t i;
	int32_t zero_padding = 0;
	int32_t format_lenght = 0;
	int32_t base;
	int32_t minus;
	int8_t num_stack[11];
	uint8_t* ps;

	va_start(va_args, fmt);

	while (*fmt) {
		switch (*fmt) {
		case '%':
			zero_padding = 0;
			if (fmt[1] == '0') {
				zero_padding = 1;
				++fmt;
			}
			format_lenght = 0;
			while (*++fmt) {
				switch (*fmt) {
				case '%':
					xputc(*fmt);
					goto next_loop;

				case 'c':
					xputc(va_arg(va_args, int32_t));
					goto next_loop;

				case 'd':
				case 'X':
				case 'x':
					minus = 0;
					num = va_arg(va_args, uint32_t);
					if (*fmt == 'd') {
						base = 10;
						if (num & (uint32_t)0x80000000) {
							num = -(int32_t)num;
							minus = 1;
						}
					} else {
						base = 16;
					}
					for (digit = 0; digit < sizeof(num_stack);) {
						num_stack[digit++] = num%base;
						num /= base;
						if (num == 0) break;
					}
					if (minus) num_stack[digit++] = 0x7F;
					if (format_lenght > digit) {
						int8_t paddingint8_t = ' ';
						format_lenght -= digit;
						if (zero_padding)
							paddingint8_t = '0';
						while (format_lenght--) {
							xputc(paddingint8_t);
						}
					}
					for (i = digit-1; i >= 0; i--) {
						if (num_stack[i] == 0x7F) {
							xputc('-');
						} else if (num_stack[i] > 9) {
							xputc(num_stack[i]-10 + 'A');
						} else {
							xputc(num_stack[i] + '0');
						}
					}
					goto next_loop;

				case 's':
					ps = va_arg(va_args, uint8_t*);
					while(*ps) {
						xputc(*ps++);
					}
					goto next_loop;

				default:
					if (*fmt >= '0' && *fmt <= '9') {
						format_lenght = format_lenght*10 + (*fmt-'0');
					} else {
						goto exit;
					}
				}

			}
			if (*fmt == 0) {
				goto exit;
			}

		default:
			xputc(*fmt);
			break;
		}
next_loop:
		fmt++;
	}
exit:
	va_end(va_args);
}

void rx_frame_parser(uint8_t data) {
	switch (rx_frame_state) {
	case SOP_STATE: {
		if (IPCPU_SOP_CHAR == data) {
			rx_frame_state = DATA_STATE;
			rev_usb_stick_msg_frame_mng.data_index = 0;
		}
	}
		break;

	case DATA_STATE: {
		((uint8_t*)&(rev_usb_stick_msg_frame_mng.usb_stick_msg_frame.lora_message))[rev_usb_stick_msg_frame_mng.data_index++] = data;

		if (rev_usb_stick_msg_frame_mng.data_index == sizeof(lora_message_t)) {
			rx_frame_state = FCS_STATE;
		}
	}
		break;

	case FCS_STATE: {
		rx_frame_state = SOP_STATE;

		rev_usb_stick_msg_frame_mng.usb_stick_msg_frame.frame_fcs = data;

		if (rev_usb_stick_msg_frame_mng.usb_stick_msg_frame.frame_fcs == usb_stick_frame_calcfcs((uint8_t*)&rev_usb_stick_msg_frame_mng.usb_stick_msg_frame.lora_message)) {
			lora_post_message(&rev_usb_stick_msg_frame_mng.usb_stick_msg_frame.lora_message);
		}
		else {
			/* TODO: handle checksum incorrect */
		}
	}
		break;

	default:
		break;
	}
}


