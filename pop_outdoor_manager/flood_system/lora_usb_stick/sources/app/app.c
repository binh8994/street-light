#include <string.h>
#include <stdlib.h>
#include "../platform/msp430g2553.h"
#include "../platform/spi/spi.h"

#include "app.h"
#include "serial.h"
#include "lora_sx1276.h"

#define LORA_NODE_REGISTER					(0)
#define LORA_NODE_REPORT					(1)
#define LORA_NODE_KEEP_ALIVE				(2)

#define FLOOD_SENSOR_ACTIVE					(0x01)
#define FLOOD_SENSOR_INACTIVE				(0x02)

static uint32_t counter = 0;

void main(void) {
	Freq_Sel		= 0x00;										//433M
	Power_Sel		= 0x00;
	Lora_Rate_Sel	= 0x06;
	BandWide_Sel	= 0x07;

	/* stop watchdog timer */
	WDTCTL = WDTPW + WDTHOLD;

	/* set clock 1Mhz */
	DCOCTL  = 0;
	BCSCTL1 = CALBC1_1MHZ;
	DCOCTL  = CALDCO_1MHZ;
	BCSCTL2 &= ~(DIVS_0);

	/*init spi*/
	spi_initialize();

	/* init console */
	serial_init();

	/*init lora*/
	sx1276_config();


	P1IE  |=  BIT3;							// P1.3 interrupt enabled
	P1IES &= ~BIT3;							// P1.3 Lo-Hi edge
	P1REN |= BIT3;							// Enable Pull Up on SW2 (P1.3)
	P1OUT |= BIT3;
	P1IFG &= ~BIT3;							// P1.3 IFG cleared

	/* configure LED */
	P2DIR |= BIT3;

	/* interrupts enabled */
	__enable_interrupt();

	sx1276_lora_entry_rx();

	while(1) {

	}
}

/* Calculate frame FCS */
uint8_t usb_stick_frame_calcfcs(uint8_t *data_ptr) {
	uint8_t xor_result = 0;
	uint8_t i;
	for (i = 0; i < sizeof(lora_message_t); i++, data_ptr++) {
		xor_result = xor_result ^ *data_ptr;
	}
	return xor_result;
}

void __attribute__ ((interrupt(PORT1_VECTOR))) Port_1 (void) {
	usb_stick_msg_frame_mng_t st_usb_stick_msg_frame_mng;
	uint8_t addr;
	uint8_t packet_size;
	lora_message_t rcv_msg;

	P1IFG &= ~BIT3; /* clear interrupt flag */

	sx1276_read(LR_RegFifoRxCurrentaddr, (uint8_t *) &addr);
	sx1276_write(LR_RegFifoAddrPtr, addr);

	if (sx1276_spread_factor_tbl[Lora_Rate_Sel] == 6) {
		packet_size = 21;
	}
	else {
		sx1276_read(LR_RegRxNbBytes, &packet_size);
	}
	sx1276_read_buffer(0x00, (uint8_t*)&rcv_msg, sizeof(lora_message_t));
	sx1276_lora_clear_irq();

	/* asm usb message */
	st_usb_stick_msg_frame_mng.usb_stick_msg_frame.frame_sop = IPCPU_SOP_CHAR;
	memcpy(&st_usb_stick_msg_frame_mng.usb_stick_msg_frame.lora_message, (uint8_t*)&rcv_msg, sizeof(lora_message_t));
	st_usb_stick_msg_frame_mng.usb_stick_msg_frame.frame_fcs = usb_stick_frame_calcfcs((uint8_t*)&st_usb_stick_msg_frame_mng.usb_stick_msg_frame.lora_message);

	serial_write((uint8_t*)&st_usb_stick_msg_frame_mng, sizeof(usb_stick_msg_frame_t));
}

void lora_post_message(lora_message_t *msg) {
	uint8_t data2 = 0;
	sx1276_lora_entry_tx();

	sx1276_write_buffer(0x00, (uint8_t *)msg, sizeof(lora_message_t));
	sx1276_write(LR_RegOpMode, 0x8b);

	while (!(data2 & 0x08)) {
		sx1276_read(LR_RegIrqFlags, (uint8_t *) &data2);
	}

	sx1276_lora_clear_irq();
	sx1276_standby();
	sx1276_lora_entry_rx();
}
