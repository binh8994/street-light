#ifndef __CONSOLE_H__
#define __CONSOLE_H__

#include <stdint.h>

#include "app.h"

#define MAX_LENGTH_BUFFER			32

typedef struct {
	uint8_t index;
	uint8_t improgress;
} serial_t;
extern serial_t serial;

extern void serial_init();
extern void serial_task();
extern void serial_write(uint8_t* data, uint16_t len);
extern void serial_printf(const char* fmt, ...);

#endif //__CONSOLE_H__
