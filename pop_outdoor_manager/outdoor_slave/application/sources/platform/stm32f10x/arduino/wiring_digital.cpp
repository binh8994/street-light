#include "Arduino.h"

#include "app_dbg.h"
#include "../io_cfg.h"
#include "../../../sys/sys_dbg.h"

void pinMode(uint8_t pin, uint8_t mode) {
	switch (pin) {

	default:
		FATAL("AR", 0x1F);
		break;
	}
}

void digitalWrite(uint8_t pin, uint8_t pinval) {
	switch (pin) {

	default:
		FATAL("AR", 0x2F);
		break;
	}
}

uint8_t digitalRead(uint8_t pin) {
	uint8_t pinval = 0;
	switch (pin) {

	default:
		FATAL("AR", 0x3F);
		break;
	}

	return pinval;
}
