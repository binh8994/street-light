/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   05/09/2016
 * @Update:
 * @AnhHH: Add io function for sth11 sensor.
 ******************************************************************************
**/
#ifndef __IO_CFG_H__
#define __IO_CFG_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "system_stm32f10x.h"
#include "core_cm3.h"

/*
 * define pin for arduino pinMode/digitalWrite/digitalRead
 * NOTE: define value MUST be deferrent
 */

/*****************************************************************************
 *Pin map led life
******************************************************************************/
#define LED_LIFE_IO_PIN					(GPIO_Pin_7)
#define LED_LIFE_IO_PORT				(GPIOE)
#define LED_LIFE_IO_CLOCK				(RCC_APB2Periph_GPIOE)

/*****************************************************************************
 *Pin map relay
******************************************************************************/
#define RELAY_1_ON_IO_PIN				(GPIO_Pin_15)
#define RELAY_1_OFF_IO_PIN				(GPIO_Pin_14)
#define RELAY_2_ON_IO_PIN				(GPIO_Pin_13)
#define RELAY_2_OFF_IO_PIN				(GPIO_Pin_12)
#define RELAY_3_ON_IO_PIN				(GPIO_Pin_11)
#define RELAY_3_OFF_IO_PIN				(GPIO_Pin_10)
#define RELAY_4_ON_IO_PIN				(GPIO_Pin_9)
#define RELAY_4_OFF_IO_PIN				(GPIO_Pin_8)

#define RELAY_IO_PORT					(GPIOD)
#define RELAY_IO_CLOCK					(RCC_APB2Periph_GPIOD)

/*****************************************************************************
 *Pin map Flash W2508
******************************************************************************/
#define FLASH_CS_IO_PIN					(GPIO_Pin_4)
#define FLASH_CS_IO_PORT				(GPIOA)
#define FLASH_CS_IO_CLOCK				(RCC_APB2Periph_GPIOA)

/****************************************************************************
 *Pin map HS1101
*****************************************************************************/
#define HS1101_PIN						(GPIO_Pin_5)
#define HS1101_PORT						(GPIOC)
#define HS1101_CLOCK					(RCC_APB2Periph_GPIOC)

/****************************************************************************
 *Pin map CT - thermistor sensor
*****************************************************************************/
#define CT_THER_ADC_CLOCK				(RCC_APB2Periph_ADC1)

#define CT_ADC_PORT						(GPIOB)
#define CT_ADC_IO_CLOCK					(RCC_APB2Periph_GPIOB)

#define CT_ADC_PIN						(GPIO_Pin_1)
#define CT_ADC_CHANEL					(ADC_Channel_9)

/****************************************************************************
 *Pin map CT sensor
*****************************************************************************/
#define THER_ADC_PORT_F					(GPIOA)
#define THER_F_ADC_IO_CLOCK				(RCC_APB2Periph_GPIOA)

#define THER_ADC_PORT_S					(GPIOC)
#define THER_S_ADC_IO_CLOCK				(RCC_APB2Periph_GPIOC)

#define THER_1_ADC_PIN					(GPIO_Pin_5)
#define THER_2_ADC_PIN					(GPIO_Pin_6)
#define THER_3_ADC_PIN					(GPIO_Pin_7)
#define THER_4_ADC_PIN					(GPIO_Pin_4)

#define THER_0_ADC_CHANEL				(ADC_Channel_5)
#define THER_1_ADC_CHANEL				(ADC_Channel_6)
#define THER_2_ADC_CHANEL				(ADC_Channel_7)
#define THER_4_ADC_CHANEL				(ADC_Channel_14)

/****************************************************************************
 *UART interface data register
*****************************************************************************/
#define USART_IF							USART2
#define USART_IF_CLK						RCC_APB1Periph_USART2
#define USART_IF_IRQn						USART2_IRQn

#define USART_IF_TX_PIN						GPIO_Pin_2
#define USART_IF_TX_GPIO_PORT				GPIOA
#define USART_IF_TX_GPIO_CLK				RCC_APB2Periph_GPIOA
#define USART_IF_TX_SOURCE					GPIO_PinSource2

#define USART_IF_RX_PIN						GPIO_Pin_3
#define USART_IF_RX_GPIO_PORT				GPIOA
#define USART_IF_RX_GPIO_CLK				RCC_APB2Periph_GPIOA
#define USART_IF_RX_SOURCE					GPIO_PinSource3

/****************************************************************************
 *port in
*****************************************************************************/
#define PORT_IN_PIN_1						(GPIO_Pin_8)
#define PORT_IN_PIN_2						(GPIO_Pin_9)
#define PORT_IN_PIN_3						(GPIO_Pin_10)
#define PORT_IN_PIN_4						(GPIO_Pin_11)
#define PORT_IN_PIN_5						(GPIO_Pin_12)
#define PORT_IN_PIN_6						(GPIO_Pin_13)

#define PORT_IN_PIN_7						(GPIO_Pin_12)
#define PORT_IN_PIN_8						(GPIO_Pin_13)
#define PORT_IN_PIN_9						(GPIO_Pin_14)
#define PORT_IN_PIN_10						(GPIO_Pin_15)
#define PORT_IN_PIN_11						(GPIO_Pin_8)
#define PORT_IN_PIN_12						(GPIO_Pin_9)

/****************************************************************************
 *port out
*****************************************************************************/
#define PORT_OUT_SW_PW						(GPIO_Pin_13)	/*portD*/

#define PORT_OUT_PIN_1						(GPIO_Pin_14)
#define PORT_OUT_PIN_2						(GPIO_Pin_15)

#define PORT_OUT_PIN_3						(GPIO_Pin_7)	/*portC*/
#define PORT_OUT_PIN_4						(GPIO_Pin_8)
#define PORT_OUT_PIN_5						(GPIO_Pin_9)

#define PORT_OUT_PIN_6						(GPIO_Pin_8)	/*portA*/
#define PORT_OUT_PIN_7						(GPIO_Pin_11)
#define PORT_OUT_PIN_8						(GPIO_Pin_12)

#define PORT_OUT_PIN_9						(GPIO_Pin_12)	/*portC*/

#define PORT_OUT_PIN_10						(GPIO_Pin_0)	/*portD*/
#define PORT_OUT_PIN_11						(GPIO_Pin_1)
#define PORT_OUT_PIN_12						(GPIO_Pin_2)

/******************************************************************************
* button function
*******************************************************************************/
extern void io_button_mode_init();
extern void io_button_up_init();
extern void io_button_down_init();

extern uint8_t io_button_mode_read();
extern uint8_t io_button_up_read();
extern uint8_t io_button_down_read();

/******************************************************************************
* led status function
*******************************************************************************/
extern void led_life_init();
extern void led_life_on();
extern void led_life_off();

/******************************************************************************
* flash IO function
*******************************************************************************/
extern void io_flash_ctrl_init();
extern void flash_cs_low();
extern void flash_cs_high();
extern uint8_t flash_transfer(uint8_t);

/******************************************************************************
* adc function
* + themistor sensor
*
* Note: MUST be enable internal clock for adc module.
*******************************************************************************/
/* configure adc peripheral */
extern void io_adc_start_cfg(void);

/* adc configure for CT sensor */
extern void io_adc_ct_cfg();
extern uint16_t adc_ct_io_read(uint8_t);

/* adc configure for thermistor sensor */
extern void io_adc_thermistor_cfg();
extern uint16_t adc_thermistor_io_read(uint8_t);

/*****************************************************************************
 *io timer hs1101
******************************************************************************/
extern void io_hs1101_timer_cfg();
extern void io_hs1101_timer_enable();
extern void io_hs1101_timer_disable();

extern void io_hs1101_init();
extern void io_hs1101_irq_disable();
extern void io_hs1101_irq_enable();
extern uint8_t io_hs1101_read();

/*****************************************************************************
 *io uart for if_sl_cpu
******************************************************************************/
extern void io_uart_interface_cfg();
extern void io_uart_interface_put_char(uint8_t);
extern uint8_t io_uart_interface_get_char();

/*****************************************************************************
 *io pwm timer 3 for control speed fan
******************************************************************************/
extern void io_pwm_timer3_cfg();
extern void io_pwm_timer3_enable();
extern void io_pwm_timer3_disable();
extern void io_pwm_timer3_chanel_0_enable();
extern void io_pwm_timer3_chanel_0_disable();
extern void io_pwm_timer3_set_duty_cycle(uint16_t);


/*****************************************************************************
 *io pwm timer 4 for control speed fan
******************************************************************************/
extern void io_pwm_timer4_cfg();
extern void io_pwm_timer4_enable();
extern void io_pwm_timer4_disable();
extern void io_pwm_timer4_chanel_enable(uint8_t);
extern void io_pwm_timer4_chanel_disable(uint8_t);
extern void io_pwm_timer4_set_duty_cycle(uint8_t, uint16_t);

/*****************************************************************************
 *port_in & port_out
******************************************************************************/
extern void io_port_in_cfg();
extern uint8_t io_get_general_input_status(uint8_t);
extern void io_port_out_cfg();
extern void io_set_pin_port_out(uint8_t);
extern void io_reset_pin_port_out(uint8_t);
extern uint8_t io_read_status_pin_port_out(uint8_t pin);
extern void io_set_switch_power_status(uint8_t);
extern uint8_t io_read_switch_power_status();
#ifdef __cplusplus
}
#endif

#endif //__IO_CFG_H__
