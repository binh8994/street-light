/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   05/09/2016
 * @Update:
 * @AnhHH: Add io function for sth11 sensor.
 ******************************************************************************
**/
#include <stdint.h>
#include <stdbool.h>

#include "io_cfg.h"
#include "stm32.h"
#include "arduino/Arduino.h"

#include "../sys/sys_dbg.h"

#include "../common/utils.h"
#include "../app/app_dbg.h"

/******************************************************************************
* led status function
*******************************************************************************/
void led_life_init() {
	GPIO_InitTypeDef        GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(LED_LIFE_IO_CLOCK, ENABLE);

	GPIO_InitStructure.GPIO_Pin = LED_LIFE_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(LED_LIFE_IO_PORT, &GPIO_InitStructure);
}

void led_life_on() {
	GPIO_SetBits(LED_LIFE_IO_PORT, LED_LIFE_IO_PIN);
}

void led_life_off() {
	GPIO_ResetBits(LED_LIFE_IO_PORT, LED_LIFE_IO_PIN);
}

/******************************************************************************
* flash IO config
*******************************************************************************/
void io_flash_ctrl_init() {
	GPIO_InitTypeDef        GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(FLASH_CS_IO_CLOCK, ENABLE);

	GPIO_InitStructure.GPIO_Pin = FLASH_CS_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init(FLASH_CS_IO_PORT, &GPIO_InitStructure);
}

void flash_cs_low() {
	GPIO_ResetBits(FLASH_CS_IO_PORT, FLASH_CS_IO_PIN);
}

void flash_cs_high() {
	GPIO_SetBits(FLASH_CS_IO_PORT, FLASH_CS_IO_PIN);
}

uint8_t flash_transfer(uint8_t data) {
	unsigned long rxtxData = data;

	/* waiting send idle then send data */
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET);
	SPI_I2S_SendData(SPI1, (uint8_t)rxtxData);

	/* waiting conplete rev data */
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET);
	rxtxData = (uint8_t)SPI_I2S_ReceiveData(SPI1);

	return (uint8_t)rxtxData;
}

/******************************************************************************
* adc function
* + CT sensor
* + themistor sensor
* Note: MUST be enable internal clock for adc module.
*******************************************************************************/
void io_adc_start_cfg(void) {
	ADC_InitTypeDef  ADC_InitStructure;

	RCC_ADCCLKConfig(RCC_PCLK2_Div6);

	/* Enable ADC1 clock so that we can talk to it */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

	ADC_DeInit(ADC1);
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfChannel = 1;
	ADC_Init(ADC1, &ADC_InitStructure);
	ADC_Cmd(ADC1, ENABLE);

	/* Enable ADC1 reset calibaration register */
	ADC_ResetCalibration(ADC1);
	while(ADC_GetResetCalibrationStatus(ADC1));

	/* Start ADC1 calibaration */
	ADC_StartCalibration(ADC1);
	while(ADC_GetCalibrationStatus(ADC1));
}

void io_adc_ct_cfg() {
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(CT_ADC_IO_CLOCK , ENABLE);

	GPIO_InitStructure.GPIO_Pin = CT_ADC_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init(CT_ADC_PORT, &GPIO_InitStructure);
}

uint16_t adc_ct_io_read(uint8_t chanel) {
	ADC_RegularChannelConfig(ADC1, chanel, 1, ADC_SampleTime_7Cycles5);

	ADC_SoftwareStartConvCmd(ADC1, ENABLE);
	while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);

	return ADC_GetConversionValue(ADC1);
}

void io_adc_thermistor_cfg() {
	GPIO_InitTypeDef    GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(THER_F_ADC_IO_CLOCK | THER_S_ADC_IO_CLOCK, ENABLE);

	GPIO_InitStructure.GPIO_Pin = THER_1_ADC_PIN | THER_2_ADC_PIN | THER_3_ADC_PIN ;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init(THER_ADC_PORT_F, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = THER_4_ADC_PIN ;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init(THER_ADC_PORT_S, &GPIO_InitStructure);
}

uint16_t adc_thermistor_io_read(uint8_t chanel) {
	ADC_RegularChannelConfig(ADC1, chanel, 1, ADC_SampleTime_7Cycles5);

	ADC_SoftwareStartConvCmd(ADC1, ENABLE);
	while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);

	return ADC_GetConversionValue(ADC1);
}

/*****************************************************************************
 *io timer hs1101
******************************************************************************/
void io_hs1101_timer_cfg() {
	TIM_TimeBaseInitTypeDef  timer_100us;
	NVIC_InitTypeDef NVIC_InitStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

	TIM_DeInit(TIM2);
	timer_100us.TIM_Prescaler = (SystemCoreClock/1000000) - 1;		/* prescale = 143 -> fc = 1Mhz*/
	timer_100us.TIM_Period = 10 - 1;								/* T = (1/10000000)*100 = 100(us)*/
	timer_100us.TIM_ClockDivision = 0;
	timer_100us.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM2, &timer_100us);

	TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
	TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
	TIM_Cmd(TIM2,ENABLE);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);

	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

void io_hs1101_timer_enable() {
	ENTRY_CRITICAL();
	TIM_Cmd(TIM2, ENABLE);
	EXIT_CRITICAL();
}

void io_hs1101_timer_disable() {
	ENTRY_CRITICAL();
	TIM_Cmd(TIM2, DISABLE);
	EXIT_CRITICAL();
}

void io_hs1101_init() {
	GPIO_InitTypeDef        GPIO_InitStructure;
	EXTI_InitTypeDef        EXTI_InitStruct;
	NVIC_InitTypeDef        NVIC_InitStruct;

	RCC_APB2PeriphClockCmd(HS1101_CLOCK, ENABLE);

	/* IRQ -> PC8 */
	GPIO_InitStructure.GPIO_Pin = HS1101_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(HS1101_PORT, &GPIO_InitStructure);

	GPIO_EXTILineConfig(GPIO_PortSourceGPIOC, GPIO_PinSource5);

	EXTI_InitStruct.EXTI_Line    = EXTI_Line5;
	EXTI_InitStruct.EXTI_Mode    = EXTI_Mode_Interrupt;
	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStruct);

	NVIC_InitStruct.NVIC_IRQChannel = EXTI9_5_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);
}

void io_hs1101_irq_disable() {
	EXTI_InitTypeDef EXTI_InitStruct;
	EXTI_InitStruct.EXTI_Line    = EXTI_Line5;
	EXTI_InitStruct.EXTI_Mode    = EXTI_Mode_Interrupt;
	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStruct.EXTI_LineCmd = DISABLE;
	EXTI_Init(&EXTI_InitStruct);
}

void io_hs1101_irq_enable() {
	EXTI_InitTypeDef EXTI_InitStruct;
	EXTI_InitStruct.EXTI_Line    = EXTI_Line5;
	EXTI_InitStruct.EXTI_Mode    = EXTI_Mode_Interrupt;
	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStruct);
}

uint8_t io_hs1101_read() {
	if (GPIO_ReadInputDataBit(HS1101_PORT, HS1101_PIN) != 0) {
		return 0x01;
	}
	else {
		return 0x00;
	}
}

void io_uart_interface_cfg() {
	USART_InitTypeDef USART_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Enable GPIO clock */
	RCC_APB2PeriphClockCmd(USART_IF_TX_GPIO_CLK, ENABLE);

	/* Enable USART clock */
	RCC_APB1PeriphClockCmd(USART_IF_CLK, ENABLE);

	/* Configure USART Tx and Rx as alternate function push-pull */
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Pin   = USART_IF_TX_PIN;
	GPIO_Init(USART_IF_TX_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin  = USART_IF_RX_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(USART_IF_RX_GPIO_PORT, &GPIO_InitStructure);

	/* USART2 configuration */
	USART_DeInit(USART_IF);
	USART_InitStructure.USART_BaudRate   = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits   = USART_StopBits_1;
	USART_InitStructure.USART_Parity     = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART_IF, &USART_InitStructure);

	/* NVIC configuration */
	/* Configure the Priority Group to 2 bits */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

	/* Enable the USARTx Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = USART_IF_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	USART_ClearITPendingBit(USART_IF,USART_IT_RXNE);
	USART_ITConfig(USART_IF, USART_IT_RXNE, ENABLE);

	/* Enable USART */
	USART_Cmd(USART_IF, ENABLE);
}

void io_uart_interface_put_char(uint8_t c) {
	USART_SendData(USART_IF, (uint8_t)c);
	while (USART_GetFlagStatus(USART_IF, USART_FLAG_TXE) == RESET);
}

uint8_t io_uart_interface_get_char() {
	uint8_t c = 0;

	if(USART_GetITStatus(USART_IF, USART_IT_RXNE) == SET) {
		c = (uint8_t)USART_ReceiveData(USART_IF);
	}

	return c;
}

/*****************************************************************************
 * PWM Timer3 Config * Sub Fan Ctrl
******************************************************************************/

void io_pwm_timer3_cfg() {
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	TIM_OCInitTypeDef TIM_OCInitStructure;

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
    GPIO_PinRemapConfig(GPIO_FullRemap_TIM3, ENABLE);

    TIM_DeInit(TIM3);
	TIM_TimeBaseStructure.TIM_Prescaler = 0;
	TIM_TimeBaseStructure.TIM_Period = 0xFFFF;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable;
	TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_High;
	TIM_OCInitStructure.TIM_Pulse = 0;

    TIM_OC1Init(TIM3, &TIM_OCInitStructure);
    TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Enable);

	TIM_ARRPreloadConfig(TIM8, ENABLE);

	/* TIM3 enable counter */
    TIM_Cmd(TIM3, ENABLE);
    TIM_CtrlPWMOutputs(TIM3, ENABLE);

	TIM3->CCR1 = (65535 / 100) * 100;
}

void io_pwm_timer3_enable() {
	ENTRY_CRITICAL();
    TIM_Cmd(TIM3, ENABLE);
	EXIT_CRITICAL();
}

void io_pwm_timer3_disable() {
	ENTRY_CRITICAL();
    TIM_Cmd(TIM3, DISABLE);
	EXIT_CRITICAL();
}

void io_pwm_timer3_chanel_0_enable() {
    TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Enable);
}

void io_pwm_timer3_chanel_0_disable() {
    TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Disable);
}

void io_pwm_timer3_set_duty_cycle( uint16_t duty_cycle) {
    TIM3->CCR1 = (65535 / 100) * duty_cycle;

}

/*****************************************************************************
 * PWM Timer4 Config * Fan Ctrl
******************************************************************************/
void io_pwm_timer4_cfg() {
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	TIM_OCInitTypeDef TIM_OCInitStructure;

	//GPIO_PinRemapConfig(GPIO_Remap_TIM4, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	TIM_DeInit(TIM4);
	TIM_TimeBaseStructure.TIM_Prescaler = 0;
	TIM_TimeBaseStructure.TIM_Period = 0xFFFF;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);

	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable;
	TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_High;
	TIM_OCInitStructure.TIM_Pulse = 0;

	TIM_OC1Init(TIM4, &TIM_OCInitStructure);
	TIM_OC1PreloadConfig(TIM4, TIM_OCPreload_Enable);

	TIM_OC2Init(TIM4, &TIM_OCInitStructure);
	TIM_OC2PreloadConfig(TIM4, TIM_OCPreload_Enable);

	TIM_OC3Init(TIM4, &TIM_OCInitStructure);
	TIM_OC3PreloadConfig(TIM4, TIM_OCPreload_Enable);

	TIM_OC4Init(TIM4, &TIM_OCInitStructure);
	TIM_OC4PreloadConfig(TIM4, TIM_OCPreload_Enable);

	TIM_ARRPreloadConfig(TIM4, ENABLE);

	/* TIM4 enable counter */
	TIM_Cmd(TIM4, ENABLE);
	TIM_CtrlPWMOutputs(TIM4, ENABLE);
}

void io_pwm_timer4_enable() {
	ENTRY_CRITICAL();
	TIM_Cmd(TIM4, ENABLE);
	EXIT_CRITICAL();
}

void io_pwm_timer4_disable() {
	ENTRY_CRITICAL();
	TIM_Cmd(TIM4, DISABLE);
	EXIT_CRITICAL();
}

void io_pwm_timer4_chanel_enable(uint8_t chanel) {
	switch (chanel) {
	case 0:
		TIM_OC1PreloadConfig(TIM4, TIM_OCPreload_Enable);
		break;

	case 1:
		TIM_OC2PreloadConfig(TIM4, TIM_OCPreload_Enable);
		break;

	case 2:
		TIM_OC3PreloadConfig(TIM4, TIM_OCPreload_Enable);
		break;

	case 3:
		TIM_OC4PreloadConfig(TIM4, TIM_OCPreload_Enable);
		break;

	default :
		break;
	}
}

void io_pwm_timer4_chanel_disable(uint8_t chanel) {
	switch (chanel) {
	case 0:
		TIM_OC1PreloadConfig(TIM4, TIM_OCPreload_Disable);
		break;

	case 1:
		TIM_OC2PreloadConfig(TIM4, TIM_OCPreload_Disable);
		break;

	case 2:
		TIM_OC3PreloadConfig(TIM4, TIM_OCPreload_Disable);
		break;

	case 3:
		TIM_OC4PreloadConfig(TIM4, TIM_OCPreload_Disable);
		break;

	default :
		break;
	}
}

void io_pwm_timer4_set_duty_cycle(uint8_t chanel, uint16_t duty_cycle) {
	switch (chanel) {
	case 0:
		TIM4->CCR1 = (65535 / 100) * duty_cycle;
		break;

	case 1:
		TIM4->CCR2 = (65535 / 100) * duty_cycle;
		break;

	case 2:
		TIM4->CCR3 = (65535 / 100) * duty_cycle;
		break;

	case 3:
		TIM4->CCR4 = (65535 / 100) * duty_cycle;
		break;

	default :
		break;
	}
}

/*****************************************************************************
 *port_in & port_out
******************************************************************************/
void io_port_in_cfg() {
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOD | RCC_APB2Periph_GPIOE, ENABLE);

	GPIO_InitStructure.GPIO_Pin = PORT_IN_PIN_1 |PORT_IN_PIN_2 | PORT_IN_PIN_3 |
			PORT_IN_PIN_4 | PORT_IN_PIN_5 | PORT_IN_PIN_6;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
	GPIO_Init(GPIOE, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = PORT_IN_PIN_7 |PORT_IN_PIN_8 | PORT_IN_PIN_9 |PORT_IN_PIN_10;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = PORT_IN_PIN_11 |PORT_IN_PIN_12;
	GPIO_Init(GPIOD, &GPIO_InitStructure);
}

uint8_t io_get_general_input_status(uint8_t pin) {
	uint8_t pin_value = 0;

	switch (pin) {
	case 0:
		pin_value = GPIO_ReadInputDataBit(GPIOE, PORT_IN_PIN_1);
		break;

	case 1:
		pin_value = GPIO_ReadInputDataBit(GPIOE, PORT_IN_PIN_2);
		break;

	case 2:
		pin_value = GPIO_ReadInputDataBit(GPIOE, PORT_IN_PIN_3);
		break;

	case 3:
		pin_value = GPIO_ReadInputDataBit(GPIOE, PORT_IN_PIN_4);
		break;

	case 4:
		pin_value = GPIO_ReadInputDataBit(GPIOE, PORT_IN_PIN_5);
		break;

	case 5:
		pin_value = GPIO_ReadInputDataBit(GPIOE, PORT_IN_PIN_6);
		break;

	case 6:
		pin_value = GPIO_ReadInputDataBit(GPIOB, PORT_IN_PIN_7);
		break;

	case 7:
		pin_value = GPIO_ReadInputDataBit(GPIOB, PORT_IN_PIN_8);
		break;

	case 8:
		pin_value = GPIO_ReadInputDataBit(GPIOB, PORT_IN_PIN_9);
		break;

	case 9:
		pin_value = GPIO_ReadInputDataBit(GPIOB, PORT_IN_PIN_10);
		break;

	case 10:
		pin_value = GPIO_ReadInputDataBit(GPIOD, PORT_IN_PIN_11);
		break;

	case 11:
		pin_value = GPIO_ReadInputDataBit(GPIOD, PORT_IN_PIN_12);
		break;

	default:
		break;

	}

	return pin_value;
}

void io_port_out_cfg() {
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD, ENABLE);

	GPIO_InitStructure.GPIO_Pin = PORT_OUT_SW_PW |PORT_OUT_PIN_1 | PORT_OUT_PIN_2 |
			PORT_OUT_PIN_10 | PORT_OUT_PIN_11 | PORT_OUT_PIN_12;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOD, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = PORT_OUT_PIN_3 | PORT_OUT_PIN_4 |PORT_OUT_PIN_5 |
			PORT_OUT_PIN_9;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = PORT_OUT_PIN_6 | PORT_OUT_PIN_7 |PORT_OUT_PIN_8;
	GPIO_Init(GPIOA, &GPIO_InitStructure);


	GPIO_ResetBits(GPIOD, PORT_OUT_PIN_1);
	GPIO_ResetBits(GPIOD, PORT_OUT_PIN_2);
	GPIO_ResetBits(GPIOC, PORT_OUT_PIN_3);
	GPIO_ResetBits(GPIOC, PORT_OUT_PIN_4);
	GPIO_ResetBits(GPIOC, PORT_OUT_PIN_5);
	GPIO_ResetBits(GPIOA, PORT_OUT_PIN_6);
	GPIO_ResetBits(GPIOA, PORT_OUT_PIN_7);
	GPIO_ResetBits(GPIOA, PORT_OUT_PIN_8);
	GPIO_ResetBits(GPIOC, PORT_OUT_PIN_9);
	GPIO_ResetBits(GPIOD, PORT_OUT_PIN_10);
	GPIO_ResetBits(GPIOD, PORT_OUT_PIN_11);
	GPIO_ResetBits(GPIOD, PORT_OUT_PIN_12);
}

void io_set_pin_port_out(uint8_t pin) {
	switch (pin) {
	case 0:
		GPIO_SetBits(GPIOD, PORT_OUT_PIN_1);
		break;

	case 1:
		GPIO_SetBits(GPIOD, PORT_OUT_PIN_2);
		break;

	case 2:
		GPIO_SetBits(GPIOC, PORT_OUT_PIN_3);
		break;

	case 3:
		GPIO_SetBits(GPIOC, PORT_OUT_PIN_4);
		break;

	case 4:
		GPIO_SetBits(GPIOC, PORT_OUT_PIN_5);
		break;

	case 5:
		GPIO_SetBits(GPIOA, PORT_OUT_PIN_6);
		break;

	case 6:
		GPIO_SetBits(GPIOA, PORT_OUT_PIN_7);
		break;

	case 7:
		GPIO_SetBits(GPIOA, PORT_OUT_PIN_8);
		break;

	case 8:
		GPIO_SetBits(GPIOC, PORT_OUT_PIN_9);
		break;

	case 9:
		GPIO_SetBits(GPIOD, PORT_OUT_PIN_10);
		break;

	case 10:
		GPIO_SetBits(GPIOD, PORT_OUT_PIN_11);
		break;

	case 11:
		GPIO_SetBits(GPIOD, PORT_OUT_PIN_12);
		break;

	default:
		break;
	}
}

void io_reset_pin_port_out(uint8_t pin) {
	switch (pin) {
	case 0:
		GPIO_ResetBits(GPIOD, PORT_OUT_PIN_1);
		break;

	case 1:
		GPIO_ResetBits(GPIOD, PORT_OUT_PIN_2);
		break;

	case 2:
		GPIO_ResetBits(GPIOC, PORT_OUT_PIN_3);
		break;

	case 3:
		GPIO_ResetBits(GPIOC, PORT_OUT_PIN_4);
		break;

	case 4:
		GPIO_ResetBits(GPIOC, PORT_OUT_PIN_5);
		break;

	case 5:
		GPIO_ResetBits(GPIOA, PORT_OUT_PIN_6);
		break;

	case 6:
		GPIO_ResetBits(GPIOA, PORT_OUT_PIN_7);
		break;

	case 7:
		GPIO_ResetBits(GPIOA, PORT_OUT_PIN_8);
		break;

	case 8:
		GPIO_ResetBits(GPIOC, PORT_OUT_PIN_9);
		break;

	case 9:
		GPIO_ResetBits(GPIOD, PORT_OUT_PIN_10);
		break;

	case 10:
		GPIO_ResetBits(GPIOD, PORT_OUT_PIN_11);
		break;

	case 11:
		GPIO_ResetBits(GPIOD, PORT_OUT_PIN_12);
		break;

	default:
		break;
	}
}
uint8_t io_read_status_pin_port_out(uint8_t pin){
	uint8_t pin_value = 0;
	switch (pin) {
	case 0:
		pin_value = GPIO_ReadOutputDataBit(GPIOD, PORT_OUT_PIN_1);
		break;

	case 1:
		pin_value = GPIO_ReadOutputDataBit(GPIOD, PORT_OUT_PIN_2);
		break;

	case 2:
		pin_value = GPIO_ReadOutputDataBit(GPIOC, PORT_OUT_PIN_3);
		break;

	case 3:
		pin_value = GPIO_ReadOutputDataBit(GPIOC, PORT_OUT_PIN_4);
		break;

	case 4:
		pin_value = GPIO_ReadOutputDataBit(GPIOC, PORT_OUT_PIN_5);
		break;

	case 5:
		pin_value = GPIO_ReadOutputDataBit(GPIOA, PORT_OUT_PIN_6);
		break;

	case 6:
		pin_value = GPIO_ReadOutputDataBit(GPIOA, PORT_OUT_PIN_7);
		break;

	case 7:
		pin_value = GPIO_ReadOutputDataBit(GPIOA, PORT_OUT_PIN_8);
		break;

	case 8:
		pin_value = GPIO_ReadOutputDataBit(GPIOC, PORT_OUT_PIN_9);
		break;

	case 9:
		pin_value = GPIO_ReadOutputDataBit(GPIOD, PORT_OUT_PIN_10);
		break;

	case 10:
		pin_value = GPIO_ReadOutputDataBit(GPIOD, PORT_OUT_PIN_11);
		break;

	case 11:
		pin_value = GPIO_ReadOutputDataBit(GPIOD, PORT_OUT_PIN_12);
		break;

	default:
		break;
	}
	return pin_value;
}

void io_set_switch_power_status(uint8_t status) {
	if (status == 0) {
		GPIO_ResetBits(GPIOD, PORT_OUT_SW_PW);
	}
	else {
		GPIO_SetBits(GPIOD, PORT_OUT_SW_PW);
	}
}
uint8_t io_read_switch_power_status() {
	uint8_t pin_stt = GPIO_ReadOutputDataBit(GPIOD, PORT_OUT_SW_PW);
	return pin_stt;
}
