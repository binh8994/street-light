CFLAGS		+= -I./sources/driver/led
CFLAGS		+= -I./sources/driver/flash
CFLAGS		+= -I./sources/driver/fuzzy_logic
CFLAGS		+= -I./sources/driver/hs1101
CFLAGS		+= -I./sources/driver/kalman

CPPFLAGS	+= -I./sources/driver/thermistor
CPPFLAGS	+= -I./sources/driver/EmonLib
CPPFLAGS	+= -I./sources/driver/exor
CPPFLAGS	+= -I./sources/driver/dev_cpu_serial_if
CPPFLAGS	+= -I./sources/driver/fan
CPPFLAGS	+= -I./sources/driver/general_input
CPPFLAGS	+= -I./sources/driver/general_output

VPATH += sources/driver/led
VPATH += sources/driver/thermistor
VPATH += sources/driver/EmonLib
VPATH += sources/driver/flash
VPATH += sources/driver/fuzzy_logic
VPATH += sources/driver/kalman
VPATH += sources/driver/hs1101
VPATH += sources/driver/exor
VPATH += sources/driver/dev_cpu_serial_if
VPATH += sources/driver/fan
VPATH += sources/driver/general_input
VPATH += sources/driver/general_output

SOURCES += sources/driver/led/led.c
SOURCES += sources/driver/flash/flash.c
SOURCES += sources/driver/fuzzy_logic/fuzzy_logic.c
SOURCES += sources/driver/hs1101/hs1101.c
SOURCES += sources/driver/kalman/kalman.c

SOURCES_CPP += sources/driver/thermistor/thermistor.cpp
SOURCES_CPP += sources/driver/EmonLib/EmonLib.cpp
SOURCES_CPP += sources/driver/exor/exor.cpp
SOURCES_CPP += sources/driver/dev_cpu_serial_if/dev_cpu_serial_if.cpp
SOURCES_CPP += sources/driver/fan/fan.cpp
SOURCES_CPP += sources/driver/general_input/general_input.cpp
SOURCES_CPP += sources/driver/general_output/general_output.cpp
