#include "fan.h"

void fan_init(fan_t* fan, uint8_t pwm_chanel, uint8_t speed) {
	fan->pwm_chanel		= pwm_chanel;
	fan->speed_control	= speed;
	fan->speed_feedback = 0;

	io_pwm_timer4_set_duty_cycle(fan->pwm_chanel, (uint8_t ) fan->speed_control);
}

void fan_enable_control(fan_t* fan) {
	io_pwm_timer4_chanel_enable(fan->pwm_chanel);
}

void fan_disable_control(fan_t* fan) {
	fan->speed_feedback = 0;
	io_pwm_timer4_chanel_disable(fan->pwm_chanel);
}

void fan_speed_control(fan_t* fan, uint8_t speed) {
	fan->speed_control = speed;
	fan->speed_feedback = speed;
	io_pwm_timer4_set_duty_cycle(fan->pwm_chanel, fan->speed_control);
}
