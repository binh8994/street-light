#ifndef __FAN_H__
#define __FAN_H__

#include <stdint.h>

#include "../sys/sys_ctrl.h"
#include "../sys/sys_io.h"
#include "../sys/sys_dbg.h"

#define FAN_ENABLE					(0x00)
#define FAN_DISABLE					(0x01)

typedef struct {
	uint8_t pwm_chanel;
	uint8_t speed_control;
	uint16_t speed_feedback;
} fan_t;

extern void fan_init(fan_t*, uint8_t, uint8_t);
extern void fan_enable_control(fan_t*);
extern void fan_disable_control(fan_t*);
extern void fan_speed_control(fan_t*, uint8_t);

#endif // __FAN_H__
