#ifndef __HS1101_H__
#define __HS1101_H__

#if defined (__cplusplus)
extern "C" {
#endif

#include <stdint.h>
#include <math.h>
#include <stdio.h>


#define		HS1101_DRIVER_OK						(0x00)
#define		HS1101_DRIVER_NG						(0x01)

#define		HS1101_CAPTURE_DISABLE					(0x00)
#define		HS1101_CAPTURE_ENABLE					(0x01)

#define RH_MAX					(200)
#define RH_MIN					(0)

const uint16_t hs1101_table[101] = {7456, 7438, 7421, 7404, 7387, 7370, 7354, 7338, 7322, 7306,
									7291, 7276, 7261, 7246, 7231, 7217, 7203, 7189, 7175, 7161,
									7147, 7134, 7120, 7107, 7094, 7081, 7068, 7055, 7043, 7030,
									7018, 7005, 6993, 6980, 6968, 6956, 6944, 6931, 6919, 6907,
									6895, 6883, 6871, 6859, 6847, 6835, 6823, 6811, 6799, 6787,
									6775, 6763, 6750, 6738, 6726, 6714, 6701, 6689, 6676, 6664,
									6651, 6638, 6626, 6613, 6600, 6587, 6574, 6560, 6547, 6534,
									6520, 6506, 6493, 6479, 6465, 6457, 6437, 6422, 6408, 6393,
									6378, 6364, 6349, 6333, 6318, 6303, 6287, 6271, 6256, 6240,
									6223, 6207, 6191, 6174, 6157, 6140, 6123, 6106, 6089, 6071,
									6051
};

typedef struct {
	uint8_t		capture;
	uint8_t		pulse_al;
	uint32_t	time_conter;
	uint32_t	frequency;

} hs1101_t;

extern void hs1101_init(hs1101_t*);
extern uint8_t hs1101_read(hs1101_t*);
extern void hs1101_inc_timer_polling(hs1101_t*);
extern void hs1101_capture_timer_polling(hs1101_t*);

#ifdef __cplusplus
}
#endif

#endif //__HS1101_H__
