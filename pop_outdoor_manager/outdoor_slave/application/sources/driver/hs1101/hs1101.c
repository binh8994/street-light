#include "hs1101.h"

#include "../ak/port.h"
#include "../app/app_dbg.h"

#include "../sys/sys_io.h"
#include "../sys/sys_ctrl.h"

void hs1101_init(hs1101_t* hs1101) {
	hs1101->capture			= HS1101_CAPTURE_DISABLE;
	hs1101->pulse_al		= 0;
	hs1101->time_conter		= 0;
	hs1101->frequency		= 0;

	io_hs1101_init();
	io_hs1101_timer_cfg();
}

uint8_t hs1101_read(hs1101_t* hs1101) {
	return (532 - (20 * hs1101->frequency) / 281);
}

void hs1101_inc_timer_polling(hs1101_t* hs1101) {
	ENTRY_CRITICAL();

	if (hs1101->capture	== HS1101_CAPTURE_ENABLE) {
		hs1101->time_conter	++;
	}

	EXIT_CRITICAL();
}

void hs1101_capture_timer_polling(hs1101_t* hs1101) {
	ENTRY_CRITICAL();

	if (hs1101->pulse_al < 19) {
		hs1101->capture	 = HS1101_CAPTURE_ENABLE;
		hs1101->pulse_al = hs1101->pulse_al + 1;
	}
	else if (hs1101->pulse_al == 19) {
		hs1101->capture	= HS1101_CAPTURE_DISABLE;
		hs1101->frequency = 2000000 / hs1101->time_conter;
		hs1101->time_conter	= 0;
		hs1101->pulse_al = 0;
		hs1101->capture	= HS1101_CAPTURE_ENABLE;
	}

	EXIT_CRITICAL();
}
