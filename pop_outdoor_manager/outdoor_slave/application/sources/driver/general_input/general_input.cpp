#include "general_input.h"

uint8_t get_general_input_status(uint8_t pin) {
	return (uint8_t)((io_get_general_input_status(pin) == 0) ? 0 : 1);
}
