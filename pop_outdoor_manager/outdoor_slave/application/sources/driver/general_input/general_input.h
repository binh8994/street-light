#ifndef __GENERAL_INPUT_H__
#define __GENERAL_INPUT_H__

#include <stdint.h>

#include "../sys/sys_ctrl.h"
#include "../sys/sys_io.h"
#include "../sys/sys_dbg.h"


extern uint8_t get_general_input_status(uint8_t);

#endif // __GENERAL_INPUT_H__
