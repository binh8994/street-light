#ifndef __DEV_CPU_SERIAL_IF_H__
#define __DEV_CPU_SERIAL_IF_H__

#include <stdint.h>
#include <stdbool.h>

#include "../sys/sys_ctrl.h"
#include "../sys/sys_io.h"
#include "../sys/sys_dbg.h"

extern void dev_serial_if_init();
extern void dev_cpu_serial_if_putc(uint8_t);
extern uint8_t dev_cpu_serial_getc();

#endif // __DEV_CPU_SERIAL_IF_H__
