#include "dev_cpu_serial_if.h"

void dev_serial_if_init() {
	io_uart_interface_cfg();
}

void dev_cpu_serial_if_putc(uint8_t c) {
	io_uart_interface_put_char(c);
}

uint8_t dev_cpu_serial_getc() {
	return io_uart_interface_get_char();
}
