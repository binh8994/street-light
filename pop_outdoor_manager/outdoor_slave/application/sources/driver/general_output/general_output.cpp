#include "general_output.h"

void set_general_output_status(uint8_t pin, uint8_t status) {
	if (status == 0) {
		io_set_pin_port_out(pin);
	}
	else {
		io_reset_pin_port_out(pin);
	}
}

uint8_t get_general_output_status(uint8_t pin) {
	return io_read_status_pin_port_out(pin);
}

void set_switch_power_status(uint8_t status) {
	io_set_switch_power_status(status);
}

uint8_t get_switch_power_status() {
	return io_read_switch_power_status();
}
