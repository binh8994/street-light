#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"

#include "../sys/sys_dbg.h"
#include "../sys/sys_ctrl.h"

#include "../common/utils.h"

#include "../driver/general_output/general_output.h"

#include "app.h"
#include "app_dbg.h"
#include "app_data.h"
#include "app_if.h"

#include "task_list.h"
#include "task_list_if.h"
#include "task_life.h"
#include "task_sensor.h"
#include "task_pop_ctrl.h"
#include "task_if.h"
#include "task_cpu_serial_if.h"

fan_t pop_fan_status[SL_TOTAL_FAN_POP];

tsm_tbl_t tsm_pop_ctrl;

#define POP_HANDLE_STATE_SAVE_ENERGY	1
#define POP_HANDLE_STATE_NORMAL			2
#define POP_HANDLE_STATE_CRITICAL		3
static uint8_t pop_handle_state = POP_HANDLE_STATE_NORMAL;

static uint8_t get_pop_handle_state();
static void set_pop_handle_state(uint8_t state);

static void sl_pop_ctrl_mode_switch_req(ak_msg_t* msg);
static void sl_pop_ctrl_control_req(ak_msg_t* msg);
static void sl_pop_ctrl_control_info_req(ak_msg_t* msg);
static void sl_pop_ctrl_auto_control_req(ak_msg_t* msg);

static uint8_t get_inside_temperature();
static uint8_t get_outside_temperature();

static void set_pop_ctrl_status(sl_pop_fan_ctrl_t* sl_pop_fan_ctrl);
static void get_pop_ctrl_status(sl_pop_fan_ctrl_t* sl_pop_fan_ctrl);

tsm_t pop_ctrl_auto[] = {
	{ SL_POP_CTRL_MODE_SWITCH_REQ		,	POP_CTRL_AUTO_STATE		,	sl_pop_ctrl_mode_switch_req		},
	{ SL_POP_CTRL_CONTROL_REQ			,	POP_CTRL_AUTO_STATE		,	TSM_FUNCTION_NULL				},
	{ SL_POP_CTRL_CONTROL_INFO_REQ		,	POP_CTRL_AUTO_STATE		,	sl_pop_ctrl_control_info_req	},
	{ SL_POP_CTRL_AUTO_CONTROL_REQ		,	POP_CTRL_AUTO_STATE		,	sl_pop_ctrl_auto_control_req	},
};

tsm_t pop_ctrl_manual[] = {
	{ SL_POP_CTRL_MODE_SWITCH_REQ		,	POP_CTRL_MANUAL_STATE	,	sl_pop_ctrl_mode_switch_req		},
	{ SL_POP_CTRL_CONTROL_REQ			,	POP_CTRL_MANUAL_STATE	,	sl_pop_ctrl_control_req			},
	{ SL_POP_CTRL_CONTROL_INFO_REQ		,	POP_CTRL_MANUAL_STATE	,	sl_pop_ctrl_control_info_req	},
	{ SL_POP_CTRL_AUTO_CONTROL_REQ		,	POP_CTRL_MANUAL_STATE	,	TSM_FUNCTION_NULL				},
};

tsm_t* tsm_pop_ctrl_table[] {
	pop_ctrl_auto,
	pop_ctrl_manual
};

void task_pop_ctrl(ak_msg_t* msg) {
	tsm_dispatch(&tsm_pop_ctrl, msg);
}

void pop_ctrl_on_state(tsm_state_t state) {
	switch(state) {
	case POP_CTRL_AUTO_STATE: {
		APP_DBG("[pop_ctrl_on_state] POP_CTRL_AUTO_STATE\n");
	}
		break;

	case POP_CTRL_MANUAL_STATE: {
		APP_DBG("[pop_ctrl_on_state] POP_CTRL_MANUAL_STATE\n");
	}
		break;

	default:
		break;
	}
}

void sl_pop_ctrl_mode_switch_req(ak_msg_t* msg) {
	APP_DBG("[pop_ctrl_state: %d ] SL_POP_CTRL_MODE_SWITCH_REQ\n", tsm_pop_ctrl.state);
	sl_pop_fan_ctrl_t st_sl_pop_fan_ctrl;
	mem_cpy(&st_sl_pop_fan_ctrl, get_data_common_msg(msg), sizeof(sl_pop_fan_ctrl_t));

	if (st_sl_pop_fan_ctrl.mode == SL_POP_CTRL_MODE_AUTO) {
		if (tsm_pop_ctrl.state != POP_CTRL_AUTO_STATE) {
			tsm_tran(&tsm_pop_ctrl, POP_CTRL_AUTO_STATE);
		}
	}
	else if (st_sl_pop_fan_ctrl.mode == SL_POP_CTRL_MODE_MANUAL) {
		if (tsm_pop_ctrl.state != POP_CTRL_MANUAL_STATE) {
			tsm_tran(&tsm_pop_ctrl, POP_CTRL_MANUAL_STATE);
		}
	}

	{
		ak_msg_t* s_msg = get_common_msg();

		set_if_des_type(s_msg, IF_TYPE_CPU_SERIAL_MT);
		set_if_des_type(s_msg, IF_TYPE_CPU_SERIAL_SL);
		set_if_src_task_id(s_msg, SL_TASK_POP_CTRL_ID);
		set_if_des_task_id(s_msg, MT_TASK_SM_ID);
		set_if_sig(s_msg, MT_SM_SL_POP_CTRL_MODE_SWITCH_RES_OK);
		set_if_data_common_msg(s_msg, (uint8_t*)&st_sl_pop_fan_ctrl, sizeof(sl_pop_fan_ctrl_t));

		set_msg_sig(s_msg, SL_IF_COMMON_MSG_OUT);
		task_post(SL_TASK_IF_ID, s_msg);
	}
}

void sl_pop_ctrl_control_req(ak_msg_t* msg) {
	APP_DBG("[pop_ctrl_state: %d ] SL_POP_CTRL_CONTROL_REQ\n", tsm_pop_ctrl.state);
	sl_pop_fan_ctrl_t st_sl_pop_fan_ctrl;
	mem_cpy(&st_sl_pop_fan_ctrl, get_data_common_msg(msg), sizeof(sl_pop_fan_ctrl_t));

	if (st_sl_pop_fan_ctrl.mode == SL_POP_CTRL_MODE_MANUAL) {

		set_pop_ctrl_status(&st_sl_pop_fan_ctrl);

		/* response to server */
		{
			ak_msg_t* s_msg = get_common_msg();

			set_if_des_type(s_msg, IF_TYPE_CPU_SERIAL_MT);
			set_if_des_type(s_msg, IF_TYPE_CPU_SERIAL_SL);
			set_if_src_task_id(s_msg, SL_TASK_POP_CTRL_ID);
			set_if_des_task_id(s_msg, MT_TASK_SM_ID);
			set_if_sig(s_msg, MT_SM_SL_POP_CTRL_CONTROL_RES_OK);
			set_if_data_common_msg(s_msg, (uint8_t*)&st_sl_pop_fan_ctrl, sizeof(sl_pop_fan_ctrl_t));

			set_msg_sig(s_msg, SL_IF_COMMON_MSG_OUT);
			task_post(SL_TASK_IF_ID, s_msg);
		}
	}
	else {
		uint8_t error_code = APP_ERROR_CODE_STATE;

		ak_msg_t* s_msg = get_common_msg();

		set_if_des_type(s_msg, IF_TYPE_CPU_SERIAL_MT);
		set_if_des_type(s_msg, IF_TYPE_CPU_SERIAL_SL);
		set_if_src_task_id(s_msg, SL_TASK_POP_CTRL_ID);
		set_if_des_task_id(s_msg, MT_TASK_SM_ID);
		set_if_sig(s_msg, MT_SM_SL_POP_CTRL_CONTROL_RES_ERR);
		set_if_data_common_msg(s_msg, (uint8_t*)&error_code, sizeof(uint8_t));

		set_msg_sig(s_msg, SL_IF_COMMON_MSG_OUT);
		task_post(SL_TASK_IF_ID, s_msg);
	}
}

void sl_pop_ctrl_auto_control_req(ak_msg_t* msg) {
	APP_DBG("[pop_ctrl_state: %d ] SL_POP_CTRL_AUTO_CONTROL_REQ\n", tsm_pop_ctrl.state);

	static uint8_t temp_outside_stable_counter = 0;
	uint8_t inside_temp = get_inside_temperature();
	uint8_t outside_temp = get_outside_temperature();
	uint8_t inside_humidity = hs1101_read(&sensor_humidity_1);

	if (outside_temp < SL_OUTSIDE_TEMP_MILESTONE &&
			temp_outside_stable_counter++ > 10 &&
			inside_humidity < SL_OUTSIDE_HUMIDITY_MILESTONE) {

		set_pop_handle_state(POP_HANDLE_STATE_SAVE_ENERGY);

		temp_outside_stable_counter = 20;		/* prevent overflow counter */\

		/* turn ON all of FANS */
		fan_speed_control(&pop_fan_status[0], 100);
		fan_speed_control(&pop_fan_status[1], 100);
		fan_speed_control(&pop_fan_status[2], 100);
		fan_speed_control(&pop_fan_status[3], 100);
	}
	else if (inside_temp < SL_INSIDE_TEMP_CRITICAL_MILESTONE) {

		set_pop_handle_state(POP_HANDLE_STATE_NORMAL);

		/*
		 * NORMAL MODE:
		 * + reset outside_check stable counter
		 * + turn ON aircond
		 * + turn OFF POP FANS
		 */

		temp_outside_stable_counter = 0;

		set_switch_power_status(SL_POWER_STATUS_ON);

		if (pop_fan_status[0].speed_feedback) {
			fan_speed_control(&pop_fan_status[0], 0);
		}

		if (pop_fan_status[1].speed_feedback) {
			fan_speed_control(&pop_fan_status[1], 0);
		}

		if (pop_fan_status[2].speed_feedback) {
			fan_speed_control(&pop_fan_status[2], 0);
		}

		if (pop_fan_status[3].speed_feedback) {
			fan_speed_control(&pop_fan_status[3], 0);
		}
	}
	else {
		set_pop_handle_state(POP_HANDLE_STATE_CRITICAL);

		temp_outside_stable_counter = 0;

		set_switch_power_status(SL_POWER_STATUS_ON);

		fan_speed_control(&pop_fan_status[0], 0);
		fan_speed_control(&pop_fan_status[1], 0);
		fan_speed_control(&pop_fan_status[2], 0);
		fan_speed_control(&pop_fan_status[3], 0);
	}

	/* DEBUGS */
	{
		APP_DBG("\n~~ START DEBUGS INFO ~~\n");

		APP_DBG("[auto_control_req] inside temp: %d\n", inside_temp);
		APP_DBG("[auto_control_req] inside humi: %d\n", inside_humidity);
		APP_DBG("[auto_control_req] outside temp: %d\n", outside_temp);
		APP_DBG("[auto_control_req] temp_outside_stable_counter: %d\n", temp_outside_stable_counter);
		APP_DBG("[auto_control_req] aircond status: %d\n", get_switch_power_status());
		APP_DBG("[auto_control_req] aircond current: %d\n", power_current.calcIrms(SL_NUMBER_SAMPLE_CT_SENSOR));
		APP_DBG("[auto_control_req] fan_speed_1: %d\n", pop_fan_status[0].speed_feedback);
		APP_DBG("[auto_control_req] fan_speed_2: %d\n", pop_fan_status[1].speed_feedback);
		APP_DBG("[auto_control_req] fan_speed_3: %d\n", pop_fan_status[2].speed_feedback);
		APP_DBG("[auto_control_req] fan_speed_4: %d\n", pop_fan_status[3].speed_feedback);

		switch (get_pop_handle_state()) {
		case POP_HANDLE_STATE_SAVE_ENERGY: {
			APP_DBG("[auto_control_req]  --> POP_HANDLE_STATE_SAVE_ENERGY\n");
		}
			break;

		case POP_HANDLE_STATE_NORMAL: {
			APP_DBG("[auto_control_req]  --> POP_HANDLE_STATE_NORMAL\n");
		}
			break;

		case POP_HANDLE_STATE_CRITICAL: {
			APP_DBG("[auto_control_req]  --> POP_HANDLE_STATE_CRITICAL\n");
		}
			break;

		default:
			break;
		}

		APP_DBG("~~ END DEBUGS INFO ~~\n\n");
	}
}

uint8_t get_inside_temperature() {
	return (uint8_t)sensor_temperature_1.read_kalman();
}

uint8_t get_outside_temperature() {
	return (uint8_t)sensor_temperature_2.read_kalman();
}

uint8_t get_pop_handle_state() {
	return pop_handle_state;
}

void set_pop_handle_state(uint8_t state) {
	switch (state) {
	case POP_HANDLE_STATE_SAVE_ENERGY: {
		APP_DBG("set_pop_handle_state -> POP_HANDLE_STATE_SAVE_ENERGY\n");
	}
		break;

	case POP_HANDLE_STATE_NORMAL: {
		APP_DBG("set_pop_handle_state -> POP_HANDLE_STATE_NORMAL\n");
	}
		break;

	case POP_HANDLE_STATE_CRITICAL: {
		APP_DBG("set_pop_handle_state -> POP_HANDLE_STATE_CRITICAL\n");
	}
		break;

	default:
		break;
	}

	pop_handle_state = state;
}

void set_pop_ctrl_status(sl_pop_fan_ctrl_t* sl_pop_fan_ctrl) {
	if (sl_pop_fan_ctrl->mode == SL_POP_CTRL_MODE_MANUAL) {

		/* switch power control */
		if (sl_pop_fan_ctrl->power_status == SL_POWER_STATUS_ON) {
			set_switch_power_status(1);
		}
		else if (sl_pop_fan_ctrl->power_status == SL_POWER_STATUS_OFF) {
			set_switch_power_status(0);
		}

		for (uint32_t i = 0; i < SL_TOTAL_FAN_POP; i++) {
			if (sl_pop_fan_ctrl->fan_status[i] >= 0 && sl_pop_fan_ctrl->fan_status[i] <= 100) {
				fan_speed_control(&pop_fan_status[i], sl_pop_fan_ctrl->fan_status[i]);
			}
		}
	}
}

void get_pop_ctrl_status(sl_pop_fan_ctrl_t* sl_pop_fan_ctrl) {
	if (tsm_pop_ctrl.state == POP_CTRL_AUTO_STATE) {
		sl_pop_fan_ctrl->mode = SL_POP_CTRL_MODE_AUTO;
	}
	else if (tsm_pop_ctrl.state == POP_CTRL_MANUAL_STATE) {
		sl_pop_fan_ctrl->mode = SL_POP_CTRL_MODE_MANUAL;
	}

	sl_pop_fan_ctrl->power_status = (get_switch_power_status() == 1) ? SL_POWER_STATUS_ON : SL_POWER_STATUS_OFF;

	for (uint32_t i = 0; i < SL_TOTAL_FAN_POP; i++) {
		sl_pop_fan_ctrl->fan_status[i] = pop_fan_status[i].speed_feedback;
	}
}

void sl_pop_ctrl_control_info_req(ak_msg_t* msg) {
	sl_pop_fan_ctrl_t st_sl_pop_fan_ctrl;
	get_pop_ctrl_status(&st_sl_pop_fan_ctrl);

	uint8_t des_sig;
	mem_cpy(&des_sig, get_data_common_msg(msg), sizeof(uint8_t));

	ak_msg_t* s_msg = get_common_msg();

	set_if_des_task_id(s_msg, msg->if_src_task_id);
	set_if_src_task_id(s_msg, SL_TASK_POP_CTRL_ID);
	set_if_des_type(s_msg, msg->if_src_type);
	set_if_src_type(s_msg, IF_TYPE_CPU_SERIAL_SL);
	set_if_sig(s_msg, des_sig);
	set_if_data_common_msg(s_msg, (uint8_t*)&st_sl_pop_fan_ctrl, sizeof(sl_pop_fan_ctrl_t));

	set_msg_sig(s_msg, SL_IF_COMMON_MSG_OUT);
	task_post(SL_TASK_IF_ID, s_msg);
}
