#ifndef __TASK_LIST_H__
#define __TASK_LIST_H__

#include "../ak/ak.h"
#include "../ak/task.h"

extern task_t app_task_table[];

/*****************************************************************************/
/*  DECLARE: Internal Task ID
 *  Note: Task id MUST be increasing order.
 *					SLAVE
 */
/*****************************************************************************/
/**
  * SYSTEM TASKS
  **************/
#define TASK_TIMER_TICK_ID				0

/**
  * APP TASKS
  **************/
#define SL_TASK_SHELL_ID				1
#define SL_TASK_LIFE_ID					2
#define SL_TASK_SENSOR_ID				3
#define SL_TASK_POP_CTRL_ID				4
#define SL_TASK_DEV_CTRL_ID				5
#define SL_TASK_IO_CTRL_ID				6
#define SL_TASK_IF_ID					7
#define SL_TASK_CPU_SERIAL_IF_ID		8
#define SL_TASK_SM_ID					9

/**
  * EOT task ID
  **************/
#define AK_TASK_EOT_ID					10

/*****************************************************************************/
/*  DECLARE: Task entry point
 */
/*****************************************************************************/
extern void task_shell(ak_msg_t*);
extern void task_life(ak_msg_t*);
extern void task_sensor(ak_msg_t*);
extern void task_pop_ctrl(ak_msg_t*);
extern void task_dev_ctrl(ak_msg_t*);
extern void task_io_ctrl(ak_msg_t*);
extern void task_if(ak_msg_t*);
extern void task_cpu_serial_if(ak_msg_t*);
extern void task_sm(ak_msg_t*);

#endif //__TASK_LIST_H__
