CFLAGS		+= -I./sources/app
CPPFLAGS	+= -I./sources/app

VPATH += sources/app

# CPP source files
SOURCES_CPP += sources/app/app.cpp
SOURCES_CPP += sources/app/shell.cpp
SOURCES_CPP += sources/app/app_data.cpp
SOURCES_CPP += sources/app/app_flash.cpp

SOURCES_CPP += sources/app/task_list.cpp
SOURCES_CPP += sources/app/task_shell.cpp
SOURCES_CPP += sources/app/task_life.cpp
SOURCES_CPP += sources/app/task_sensor.cpp
SOURCES_CPP += sources/app/task_pop_ctrl.cpp
SOURCES_CPP += sources/app/task_dev_ctrl.cpp
SOURCES_CPP += sources/app/task_io_ctrl.cpp
SOURCES_CPP += sources/app/task_if.cpp
SOURCES_CPP += sources/app/task_cpu_serial_if.cpp
SOURCES_CPP += sources/app/task_sm.cpp
