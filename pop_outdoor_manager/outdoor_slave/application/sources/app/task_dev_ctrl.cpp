#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"

#include "../sys/sys_dbg.h"
#include "../sys/sys_ctrl.h"

#include "../common/utils.h"

#include "app.h"
#include "app_dbg.h"
#include "app_data.h"
#include "app_if.h"

#include "task_list.h"
#include "task_list_if.h"
#include "task_sensor.h"
#include "task_dev_ctrl.h"
#include "task_if.h"
#include "task_cpu_serial_if.h"

uint8_t device_fan_status;

void task_dev_ctrl(ak_msg_t* msg) {
	switch (msg->sig) {
	case SL_DEV_CTRL_FAN_SPEED_CONTROL_REQ: {
		APP_DBG("SL_DEV_CTRL_FAN_SPEED_CONTROL_REQ\n");
		uint8_t percent;
		mem_cpy(&percent, get_data_common_msg(msg), sizeof(uint8_t));
		APP_DBG("percent: %d\n", percent);
	}
		break;

	case SL_DEV_CTRL_FAN_AUTO_CONTROL_REQ:  {
		APP_DBG("SL_DEV_CTRL_FAN_AUTO_CONTROL_REQ\n");
	}
		break;

	default:
		break;
	}
}
