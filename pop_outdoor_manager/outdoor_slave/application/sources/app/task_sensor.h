#ifndef __TASK_SENSOR_H__
#define __TASK_SENSOR_H__

#include <stdint.h>

#include "../driver/thermistor/thermistor.h"
#include "../driver/hs1101/hs1101.h"
#include "../driver/EmonLib/EmonLib.h"

#include "app_data.h"

extern THERMISTOR sensor_temperature_1;
extern THERMISTOR sensor_temperature_2;
extern THERMISTOR sensor_temperature_3;
extern THERMISTOR sensor_temperature_4;

extern hs1101_t sensor_humidity_1;
extern hs1101_t sensor_humidity_2;

extern EnergyMonitor power_current;

#endif // __TASK_SENSOR_H__
