#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"

#include "../sys/sys_ctrl.h"

#include "app.h"
#include "app_dbg.h"

#include "task_list.h"
#include "task_list_if.h"
#include "task_sm.h"


void task_sm(ak_msg_t* msg) {
	switch (msg->sig) {
	case SL_SM_MT_SYNC_REQ: {
		APP_DBG("SL_SM_MT_SYNC_REQ\n");
		ak_msg_t* s_msg = get_common_msg();

		set_if_des_type(s_msg, IF_TYPE_CPU_SERIAL_MT);
		set_if_src_task_id(s_msg,SL_TASK_SM_ID);
		set_if_des_task_id(s_msg, MT_TASK_SM_ID);
		set_if_sig(s_msg, MT_SM_SL_SYNC_RES);
		set_if_data_common_msg(s_msg, (uint8_t*)get_data_common_msg(msg), sizeof(uint8_t));

		set_msg_sig(s_msg, SL_IF_COMMON_MSG_OUT);
		set_msg_src_task_id(s_msg, SL_TASK_SM_ID);
		task_post(SL_TASK_IF_ID, s_msg);
	}
		break;

	default:
		break;
	}
}
