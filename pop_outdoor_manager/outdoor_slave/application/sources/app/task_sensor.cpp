#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"

#include "../sys/sys_dbg.h"
#include "../sys/sys_ctrl.h"

#include "../common/utils.h"

#include "../driver/general_input/general_input.h"
#include "../driver/general_output/general_output.h"
#include "../driver/fan/fan.h"

#include "app.h"
#include "app_dbg.h"
#include "app_data.h"
#include "app_if.h"

#include "task_list.h"
#include "task_list_if.h"
#include "task_life.h"
#include "task_sensor.h"
#include "task_pop_ctrl.h"
#include "task_dev_ctrl.h"
#include "task_if.h"
#include "task_cpu_serial_if.h"

#define SENSOR_TYPE_TEMPERATURE					(1)
#define SENSOR_TYPE_HUMIDITY					(2)
#define SENSOR_TYPE_GENERAL_INPUT				(3)
#define SENSOR_TYPE_GENERAL_OUTPUT				(4)
#define SENSOR_TYPE_POP_FAN						(5)
#define SENSOR_TYPE_DEV_FAN						(6)
#define SENSOR_TYPE_POWER_STATUS				(7)
#define SENSOR_TYPE_POWER_CURRENT				(8)

THERMISTOR sensor_temperature_1(THER_0_ADC_CHANEL, 10000, 3590, 10000);
THERMISTOR sensor_temperature_2(THER_1_ADC_CHANEL, 10000, 3590, 10000);
THERMISTOR sensor_temperature_3(THER_2_ADC_CHANEL, 10000, 3590, 10000);
THERMISTOR sensor_temperature_4(THER_4_ADC_CHANEL, 10000, 3590, 10000);

hs1101_t sensor_humidity_1;
hs1101_t sensor_humidity_2;

EnergyMonitor power_current;

static void get_sensors_data(sl_sensors_t* sl_sensors, uint8_t sensor_type);

void task_sensor(ak_msg_t* msg) {
	switch (msg->sig) {
	case SL_SENSOR_REPORT_REQ: {
		APP_DBG("SL_SENSOR_REPORT_REQ\n");

		sl_sensors_t st_sensors;

		get_sensors_data(&st_sensors, SENSOR_TYPE_TEMPERATURE);
		get_sensors_data(&st_sensors, SENSOR_TYPE_HUMIDITY);
		get_sensors_data(&st_sensors, SENSOR_TYPE_GENERAL_INPUT);
		get_sensors_data(&st_sensors, SENSOR_TYPE_GENERAL_OUTPUT);
		get_sensors_data(&st_sensors, SENSOR_TYPE_POP_FAN);
		get_sensors_data(&st_sensors, SENSOR_TYPE_DEV_FAN);
		get_sensors_data(&st_sensors, SENSOR_TYPE_POWER_STATUS);
		get_sensors_data(&st_sensors, SENSOR_TYPE_POWER_CURRENT);

		APP_DBG("temperature_1: %d\n",			st_sensors.temperature[0]);
		APP_DBG("temperature_2: %d\n",			st_sensors.temperature[1]);
		APP_DBG("temperature_3: %d\n",			st_sensors.temperature[2]);
		APP_DBG("temperature_4: %d\n",			st_sensors.temperature[3]);
		APP_DBG("himidity_1: %d\n",				st_sensors.humidity[0]);
		APP_DBG("himidity_2: %d\n",				st_sensors.humidity[1]);
		APP_DBG("general_input_1: %d\n",		st_sensors.general_input[0]);
		APP_DBG("general_input_2: %d\n",		st_sensors.general_input[1]);
		APP_DBG("general_input_3: %d\n",		st_sensors.general_input[2]);
		APP_DBG("general_input_4: %d\n",		st_sensors.general_input[3]);
		APP_DBG("general_input_5: %d\n",		st_sensors.general_input[4]);
		APP_DBG("general_input_6: %d\n",		st_sensors.general_input[5]);
		APP_DBG("general_input_7: %d\n",		st_sensors.general_input[6]);
		APP_DBG("general_input_8: %d\n",		st_sensors.general_input[7]);
		APP_DBG("general_input_9: %d\n",		st_sensors.general_input[8]);
		APP_DBG("general_input_10: %d\n",		st_sensors.general_input[9]);
		APP_DBG("general_input_11: %d\n",		st_sensors.general_input[10]);
		APP_DBG("general_input_12: %d\n",		st_sensors.general_input[11]);
		APP_DBG("general_output_1: %d\n",		st_sensors.general_output[0]);
		APP_DBG("general_output_2: %d\n",		st_sensors.general_output[1]);
		APP_DBG("general_output_3: %d\n",		st_sensors.general_output[2]);
		APP_DBG("general_output_4: %d\n",		st_sensors.general_output[3]);
		APP_DBG("general_output_5: %d\n",		st_sensors.general_output[4]);
		APP_DBG("general_output_6: %d\n",		st_sensors.general_output[5]);
		APP_DBG("general_output_7: %d\n",		st_sensors.general_output[6]);
		APP_DBG("general_output_8: %d\n",		st_sensors.general_output[7]);
		APP_DBG("general_output_9: %d\n",		st_sensors.general_output[8]);
		APP_DBG("general_output_10: %d\n",		st_sensors.general_output[9]);
		APP_DBG("general_output_11: %d\n",		st_sensors.general_output[10]);
		APP_DBG("general_output_12: %d\n",		st_sensors.general_output[11]);
		APP_DBG("fan_pop_1: %d\n",				st_sensors.fan_pop[0]);
		APP_DBG("fan_pop_2: %d\n",				st_sensors.fan_pop[1]);
		APP_DBG("fan_pop_3: %d\n",				st_sensors.fan_pop[2]);
		APP_DBG("fan_pop_4: %d\n",				st_sensors.fan_pop[3]);
		APP_DBG("fan_dev: %d\n",				st_sensors.fan_dev);
		APP_DBG("power_output_status: %d\n",	st_sensors.power_output_status);
		APP_DBG("power_output_current: %d\n",	st_sensors.power_output_current);

		ak_msg_t* s_msg = get_common_msg();

		set_if_src_type(s_msg, IF_TYPE_CPU_SERIAL_SL);
		set_if_des_type(s_msg, IF_TYPE_CPU_SERIAL_MT);
		set_if_src_task_id(s_msg,SL_TASK_SENSOR_ID);
		set_if_des_task_id(s_msg, MT_TASK_SM_ID);
		set_if_sig(s_msg, MT_SM_SL_SENSOR_REPORT_RES);
		set_if_data_common_msg(s_msg, (uint8_t*)&st_sensors, sizeof(sl_sensors_t));

		set_msg_sig(s_msg, SL_IF_COMMON_MSG_OUT);
		set_msg_src_task_id(s_msg, SL_TASK_SENSOR_ID);
		task_post(SL_TASK_IF_ID, s_msg);
	}
		break;

	default:
		break;
	}
}

void get_sensors_data(sl_sensors_t* sl_sensors, uint8_t sensor_type) {
	switch (sensor_type) {
	case SENSOR_TYPE_TEMPERATURE: {
		sl_sensors->temperature[0] = sensor_temperature_1.read_kalman();
		sl_sensors->temperature[1] = sensor_temperature_2.read_kalman();
		sl_sensors->temperature[2] = sensor_temperature_3.read_kalman();
		sl_sensors->temperature[3] = sensor_temperature_4.read_kalman();
	}
		break;

	case SENSOR_TYPE_HUMIDITY: {
		sl_sensors->humidity[0] = hs1101_read(&sensor_humidity_1);
		sl_sensors->humidity[1] = hs1101_read(&sensor_humidity_2);
	}
		break;

	case SENSOR_TYPE_GENERAL_INPUT: {
		for (uint8_t pin = 0; pin < SL_TOTAL_GENERAL_INPUT; pin++) {
			sl_sensors->general_input[pin] = get_general_input_status(pin);
		}
	}
		break;

	case SENSOR_TYPE_GENERAL_OUTPUT: {
		for (uint8_t pin = 0; pin < SL_TOTAL_GENERAL_OUTPUT; pin++) {
			sl_sensors->general_output[pin] = get_general_output_status(pin);
		}
	}
		break;

	case SENSOR_TYPE_POP_FAN: {
		sl_sensors->fan_pop[0] = pop_fan_status[0].speed_feedback;
		sl_sensors->fan_pop[1] = pop_fan_status[1].speed_feedback;
		sl_sensors->fan_pop[2] = pop_fan_status[2].speed_feedback;
		sl_sensors->fan_pop[3] = pop_fan_status[3].speed_feedback;
	}
		break;

	case SENSOR_TYPE_DEV_FAN: {
		sl_sensors->fan_dev = device_fan_status;
	}
		break;

	case SENSOR_TYPE_POWER_STATUS: {
		sl_sensors->power_output_status = io_read_switch_power_status();
	}
		break;

	case SENSOR_TYPE_POWER_CURRENT: {
		sl_sensors->power_output_current = power_current.calcIrms(SL_NUMBER_SAMPLE_CT_SENSOR);;
	}
		break;

	default:
		break;
	}
}
