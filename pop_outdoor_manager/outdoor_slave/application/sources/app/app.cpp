/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   13/08/2016
 ******************************************************************************
**/

/* kernel include */
#include "../ak/ak.h"
#include "../ak/message.h"
#include "../ak/timer.h"
#include "../ak/fsm.h"
#include "../ak/tsm.h"

/* driver include */
#include "../driver/dev_cpu_serial_if/dev_cpu_serial_if.h"
#include "../driver/fan/fan.h"

/* common include */
#include "../common/utils.h"

/* app include */
#include "app.h"
#include "app_dbg.h"

#include "task_list.h"
#include "task_list_if.h"
#include "task_shell.h"
#include "task_life.h"
#include "task_sensor.h"
#include "task_pop_ctrl.h"
#include "task_dev_ctrl.h"
#include "task_if.h"
#include "task_cpu_serial_if.h"

/* sys include */
#include "../sys/sys_irq.h"
#include "../sys/sys_io.h"
#include "../sys/sys_ctrl.h"
#include "../sys/sys_dbg.h"
#include "../sys/sys_arduino.h"

static void app_start_timer();
static void app_init_state_machine();
static void app_task_init();

/*****************************************************************************/
/* app main function.
 */
/*****************************************************************************/
int main_app() {
	APP_PRINT("main_app() entry OK\n");

	/******************************************************************************
	* init active kernel
	*******************************************************************************/
	ENTRY_CRITICAL();
	task_init();
	task_create(app_task_table);
	EXIT_CRITICAL();

	/******************************************************************************
	* init applications
	*******************************************************************************/
	/* init watch dog timer */
	sys_ctrl_independent_watchdog_init();	/* 20s */
	sys_ctrl_soft_watchdog_init(100);		/* 10s */

	/*********************
	* hardware configure *
	**********************/
	/* uart peripheral configure for if_cpu */
	dev_serial_if_init();

	/* adc peripheral configure */
	io_adc_start_cfg();

	/* adc configure for ct sensor */
	io_adc_ct_cfg();

	/* adc configure for thermistor */
	io_adc_thermistor_cfg();

	/* pwm of timer 4 configure for cooling_fan */
	io_pwm_timer4_cfg();

	/* pwm of timer 3 configure for cooling_fan */
	io_pwm_timer3_cfg();

	/* port in & port out configure */
	io_port_in_cfg();
	io_port_out_cfg();

	/*********************
	* software configure *
	**********************/
	/* life led init */
	led_init(&led_life, led_life_init, led_life_on, led_life_off);

	/* hs1101 init */
	hs1101_init(&sensor_humidity_1);
	hs1101_init(&sensor_humidity_2);

	/* ct sensor init */
	power_current.current(CT_ADC_CHANEL, (double)2000/(double)220); /* 2000/R */

	/* cooling fan init */
	for (uint8_t i = 0; i < SL_TOTAL_FAN_POP; i++) {
		fan_init(&pop_fan_status[i], i, (uint8_t) 90);
	}

	/* start timer for application */
	app_init_state_machine();
	app_start_timer();

	/******************************************************************************
	* app task initial
	*******************************************************************************/
	app_task_init();

	/******************************************************************************
	* run applications
	*******************************************************************************/
	return task_run();
}

/*****************************************************************************/
/* app initial function.
 */
/*****************************************************************************/
/* start software timer for application
 * used for app tasks
 */
void app_start_timer() {
	/* start timer to toggle life led */
	timer_set(SL_TASK_LIFE_ID, SL_LIFE_SYSTEM_CHECK, SL_LIFE_TASK_TIMER_LED_LIFE_INTERVAL, TIMER_PERIODIC);

	/* start timer dev fan auto control */
	timer_set(SL_TASK_DEV_CTRL_ID, SL_DEV_CTRL_FAN_AUTO_CONTROL_REQ, SL_DEV_CTRL_FAN_AUTO_CONTROL_REQ_INTERVAL, TIMER_PERIODIC);

	/* start timer pop temperature auto control */
	timer_set(SL_TASK_POP_CTRL_ID, SL_POP_CTRL_AUTO_CONTROL_REQ, SL_POP_CTRL_AUTO_CONTROL_REQ_INTERVAL, TIMER_PERIODIC);
}

/* init state machine for tasks
 * used for app tasks
 */
void app_init_state_machine() {
	/* init table SL_POP_CTRL state-machine */
	tsm_pop_ctrl.on_state = pop_ctrl_on_state;
	tsm_init(&tsm_pop_ctrl, tsm_pop_ctrl_table, POP_CTRL_AUTO_STATE);
}

/* send first message to trigger start tasks
 * used for app tasks
 */
void app_task_init() {
}

/*****************************************************************************/
/* app common function
 */
/*****************************************************************************/
/* hardware timer interrupt 10ms
 * used for led, button polling
 */
void sys_irq_timer_10ms() {
}

/* hardware timer interrupt 100us
 * used for capture frequency hs1101
 */
void sys_irq_timer_hs1101() {
	hs1101_inc_timer_polling(&sensor_humidity_1);
	hs1101_inc_timer_polling(&sensor_humidity_2);
}

void sys_irq_rev_hs1101() {
	hs1101_capture_timer_polling(&sensor_humidity_1);
	hs1101_capture_timer_polling(&sensor_humidity_2);
}
