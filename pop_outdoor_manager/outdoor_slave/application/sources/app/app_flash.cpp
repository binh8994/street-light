#include "app.h"
#include "app_if.h"
#include "app_dbg.h"
#include "app_flash.h"

#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"

#include "../sys/sys_ctrl.h"
#include "../sys/sys_dbg.h"

#include "../common/utils.h"

#include "../driver/flash/flash.h"


#include "task_list.h"


void flash_read_setting_info() {
}

void flash_read_network_info() {
}

void flash_read_aircond_info() {
}


uint8_t flash_write_setting_info() {
	return FLASH_WRITE_OK;
}

uint8_t flash_write_network_info() {
	return FLASH_WRITE_OK;
}

uint8_t flash_write_aircond_info() {
	return FLASH_WRITE_OK;
}
