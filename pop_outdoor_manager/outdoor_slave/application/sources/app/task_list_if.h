#ifndef __TASK_LIST_IF_H__
#define __TASK_LIST_IF_H__

/* task list MUST BE increase order */

#define MT_TASK_IF_CONSOLE_ID				1
#define MT_TASK_CONSOLE_ID					2
#define MT_TASK_SNMP_ID						3
#define MT_TASK_CLOUD_ID					4
#define MT_TASK_IF_ID						5
#define MT_TASK_DEBUG_MSG_ID				6
#define MT_TASK_IF_APP_ID					7
#define MT_TASK_IF_CPU_SERIAL_ID			8
#define MT_TASK_SM_ID						9
#define MT_TASK_SENSOR_ID					10
#define MT_TASK_SYS_ID						11

/* size of task list table */
#define AK_TASK_LIST_LEN					11

#endif //__TASK_LIST_IF_H__
