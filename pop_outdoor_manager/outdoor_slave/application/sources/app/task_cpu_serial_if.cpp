#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"

#include "../common/cmd_line.h"
#include "../common/utils.h"
#include "../common/xprintf.h"

#include "../sys/sys_irq.h"
#include "../sys/sys_io.h"
#include "../sys/sys_ctrl.h"
#include "../sys/sys_dbg.h"
#include "../sys/sys_arduino.h"

#include "../driver/dev_cpu_serial_if/dev_cpu_serial_if.h"

#include "app.h"
#include "app_dbg.h"
#include "task_cpu_serial_if.h"
#include "task_list.h"

#define IFCPU_SOP_CHAR					(0xEF)
#define IFCPU_DATA_SIZE					(256)

#define SOP_STATE						(0x00)
#define LEN_STATE						(0x01)
#define DATA_STATE						(0x02)
#define FCS_STATE						(0x03)

#define RX_FRAME_PARSER_FAILED			(-1)
#define RX_FRAME_PARSER_SUCCESS			(0)
#define RX_FRAME_PARSER_rx_remain		(1)

struct if_cpu_serial_frame_t {
	uint8_t		frame_sop;
	uint32_t	len;
	uint8_t		data_index;
	uint8_t		data[IFCPU_DATA_SIZE];
	uint8_t		frame_fcs;
} if_cpu_serial_frame;

static uint8_t cpu_serial_rx_frame_state = SOP_STATE;
static uint8_t tx_buffer[IFCPU_DATA_SIZE];

static void rx_frame_parser(uint8_t data);
static void tx_frame_post(uint8_t*, uint8_t);
static uint8_t if_cpu_serial_calcfcs(uint8_t, uint8_t*);

void sys_irq_uart_cpu_serial_if() {
	rx_frame_parser(dev_cpu_serial_getc());
}

void task_cpu_serial_if(ak_msg_t* msg) {
	switch (msg->sig) {
	case SL_CPU_SERIAL_IF_PURE_MSG_OUT: {
		APP_DBG("[IF SL SERIAL][SEND] SL_CPU_SERIAL_IF_PURE_MSG_OUT\n");

		ak_msg_pure_if_t app_if_msg;
		memset(&app_if_msg, 0, sizeof(ak_msg_pure_if_t));

		/* assign if message */
		app_if_msg.header.type			= PURE_MSG_TYPE;
		app_if_msg.header.if_src_type	= msg->if_src_type;
		app_if_msg.header.if_des_type	= msg->if_des_type;
		app_if_msg.header.sig			= msg->if_sig;
		app_if_msg.header.src_task_id	= msg->if_src_task_id;
		app_if_msg.header.des_task_id	= msg->if_des_task_id;

		tx_frame_post((uint8_t*)&app_if_msg, sizeof(ak_msg_pure_if_t));
	}
		break;

	case SL_CPU_SERIAL_IF_COMMON_MSG_OUT: {
		APP_DBG("[IF SL SERIAL][SEND] SL_CPU_SERIAL_IF_COMMON_MSG_OUT\n");

		ak_msg_common_if_t app_if_msg;
		memset(&app_if_msg, 0, sizeof(ak_msg_pure_if_t));

		/* assign if message */
		app_if_msg.header.type			= COMMON_MSG_TYPE;
		app_if_msg.header.if_src_type	= msg->if_src_type;
		app_if_msg.header.if_des_type	= msg->if_des_type;
		app_if_msg.header.sig			= msg->if_sig;
		app_if_msg.header.src_task_id	= msg->if_src_task_id;
		app_if_msg.header.des_task_id	= msg->if_des_task_id;

		app_if_msg.len = get_data_len_common_msg(msg);
		mem_cpy(app_if_msg.data, get_data_common_msg(msg), sizeof(uint8_t) * app_if_msg.len);

		tx_frame_post((uint8_t*)&app_if_msg, sizeof(ak_msg_common_if_t));
	}
		break;

	default:
		break;
	}
}

void rx_frame_parser(uint8_t ch) {
	switch (cpu_serial_rx_frame_state) {
	case SOP_STATE: {
		if (IFCPU_SOP_CHAR == ch) {
			cpu_serial_rx_frame_state = LEN_STATE;
		}
	}
		break;

	case LEN_STATE: {
		if (ch > IFCPU_DATA_SIZE) {
			cpu_serial_rx_frame_state = SOP_STATE;
			return;
		}
		else {
			if_cpu_serial_frame.len = ch;
			if_cpu_serial_frame.data_index = 0;
			cpu_serial_rx_frame_state = DATA_STATE;
		}
	}
		break;

	case DATA_STATE: {
		if_cpu_serial_frame.data[if_cpu_serial_frame.data_index++] = ch;

		if (if_cpu_serial_frame.data_index == if_cpu_serial_frame.len) {
			cpu_serial_rx_frame_state = FCS_STATE;
		}
	}
		break;

	case FCS_STATE: {
		cpu_serial_rx_frame_state = SOP_STATE;

		if_cpu_serial_frame.frame_fcs = ch;

		if (if_cpu_serial_frame.frame_fcs \
				== if_cpu_serial_calcfcs(if_cpu_serial_frame.len, if_cpu_serial_frame.data)) {

			ak_msg_if_header_t* if_msg_header = (ak_msg_if_header_t*)if_cpu_serial_frame.data;

			switch (if_msg_header->type) {
			case PURE_MSG_TYPE: {
				APP_DBG("[IF CPU SERIAL][REV] PURE_MSG_TYPE\n");
				ak_msg_t* s_msg = get_pure_msg();

				set_if_src_task_id(s_msg, if_msg_header->src_task_id);
				set_if_des_task_id(s_msg, if_msg_header->des_task_id);
				set_if_src_type(s_msg, IF_TYPE_CPU_SERIAL_SL);
				set_if_des_type(s_msg, if_msg_header->if_des_type);
				set_if_sig(s_msg, if_msg_header->sig);

				set_msg_sig(s_msg, SL_IF_PURE_MSG_IN);
				task_post(SL_TASK_IF_ID, s_msg);
			}
				break;

			case COMMON_MSG_TYPE: {
				APP_DBG("[IF CPU SERIAL][REV] COMMON_MSG_TYPE\n");
				ak_msg_t* s_msg = get_common_msg();

				set_if_src_task_id(s_msg, if_msg_header->src_task_id);
				set_if_des_task_id(s_msg, if_msg_header->des_task_id);
				set_if_src_type(s_msg, IF_TYPE_CPU_SERIAL_SL);
				set_if_des_type(s_msg, if_msg_header->if_des_type);
				set_if_sig(s_msg, if_msg_header->sig);
				set_if_data_common_msg(s_msg, ((ak_msg_common_if_t*)if_msg_header)->data, ((ak_msg_common_if_t*)if_msg_header)->len);

				set_msg_sig(s_msg, SL_IF_COMMON_MSG_IN);
				task_post(SL_TASK_IF_ID, s_msg);
			}
				break;

			default:
				break;
			}
		}
		else {
			/* TODO: handle checksum incorrect */
		}
	}
		break;

	default:
		break;
	}
}

void tx_frame_post(uint8_t* data, uint8_t len) {
	tx_buffer[0] = IFCPU_SOP_CHAR;
	tx_buffer[1] = len;
	mem_cpy(&tx_buffer[2], data, len);
	tx_buffer[2 + len] = if_cpu_serial_calcfcs(len, data);

	for (uint8_t i = 0; i < len + 3; i++) {
		dev_cpu_serial_if_putc(tx_buffer[i]);
	}
}

uint8_t if_cpu_serial_calcfcs(uint8_t len, uint8_t *data_ptr) {
	uint8_t xor_result = len;
	for (int i = 0; i < len; i++, data_ptr++) {
		xor_result = xor_result ^ *data_ptr;
	}
	return xor_result;
}
