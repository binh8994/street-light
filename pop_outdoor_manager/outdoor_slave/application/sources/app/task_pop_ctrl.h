#ifndef __TASK_POP_CTRL_H__
#define __TASK_POP_CTRL_H__

#include "../ak/ak.h"
#include "../ak/tsm.h"

#include "app_data.h"

#include "../driver/fan/fan.h"

#define POP_CTRL_AUTO_STATE			0
#define POP_CTRL_MANUAL_STATE		1

extern void pop_ctrl_on_state(tsm_state_t);

extern tsm_t* tsm_pop_ctrl_table[];
extern tsm_tbl_t tsm_pop_ctrl;

extern fan_t pop_fan_status[SL_TOTAL_FAN_POP];

#endif // __TASK_POP_CTRL_H__
