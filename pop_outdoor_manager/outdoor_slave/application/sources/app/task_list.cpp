#include "task_list.h"
#include "../ak/timer.h"

task_t app_task_table[] = {
	/*************************************************************************/
	/* SYSTEM TASK */
	/*************************************************************************/
	{TASK_TIMER_TICK_ID,	TASK_PRI_LEVEL_7,		task_timer_tick			},

	/*************************************************************************/
	/* APP TASK */
	/*************************************************************************/
	{SL_TASK_SHELL_ID			,	TASK_PRI_LEVEL_2	,	task_shell			},
	{SL_TASK_LIFE_ID			,	TASK_PRI_LEVEL_6	,	task_life			},
	{SL_TASK_SENSOR_ID			,	TASK_PRI_LEVEL_2	,	task_sensor			},
	{SL_TASK_POP_CTRL_ID		,	TASK_PRI_LEVEL_2	,	task_pop_ctrl		},
	{SL_TASK_DEV_CTRL_ID		,	TASK_PRI_LEVEL_2	,	task_dev_ctrl		},
	{SL_TASK_IO_CTRL_ID			,	TASK_PRI_LEVEL_2	,	task_io_ctrl		},
	{SL_TASK_IF_ID				,	TASK_PRI_LEVEL_4	,	task_if				},
	{SL_TASK_CPU_SERIAL_IF_ID	,	TASK_PRI_LEVEL_2	,	task_cpu_serial_if	},
	{SL_TASK_SM_ID				,	TASK_PRI_LEVEL_2	,	task_sm				},

	/*************************************************************************/
	/* END OF TABLE */
	/*************************************************************************/
	{AK_TASK_EOT_ID,			TASK_PRI_LEVEL_0,		(pf_task)0			}
};
