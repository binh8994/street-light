#ifndef __APP_DATA_H__
#define __APP_DATA_H__
#include <stdint.h>
#include "app.h"

/** CPU SERIAL interface, communication via uart serial interface
 *
 */
#define IF_TYPE_CPU_SERIAL_MT				(120)
#define IF_TYPE_CPU_SERIAL_SL				(121)

/******************************************************************************
* Common define
*******************************************************************************/
#define APP_ERROR_CODE_TIMEOUT		0x01
#define APP_ERROR_CODE_BUSY			0x02
#define APP_ERROR_CODE_STATE		0x03

/******************************************************************************
* Commom data structure for transceiver data
*******************************************************************************/
#define FIRMWARE_PSK		0x1A2B3C4D
#define FIRMWARE_LOK		0x1234ABCD

typedef struct {
	uint32_t psk;
	uint32_t bin_len;
	uint16_t checksum;
} firmware_header_t;

#define SENSOR_STATUS_UNKOWN				0x00
#define SENSOR_STATUS_NORMAL				0x01
#define SENSOR_STATUS_WARNING				0x02
#define SENSOR_STATUS_DISCONNECTED			0x03

#define SENSOR_STATUS_CLOSED				0x01
#define SENSOR_STATUS_OPENED				0x02

#define SENSOR_STATUS_HIGH					0x01
#define SENSOR_STATUS_LOW					0x02

#define SL_POWER_STATUS_ON					0x01
#define SL_POWER_STATUS_OFF					0x02

#define SL_POP_CTRL_MODE_AUTO				0x01
#define SL_POP_CTRL_MODE_MANUAL				0x02

#define SL_TOTAL_SENSOR_TEMPERATURE			(4)
#define SL_TOTAL_SENSOR_HUMIDITY			(2)
#define SL_TOTAL_GENERAL_INPUT				(12)
#define SL_TOTAL_GENERAL_OUTPUT				(12)
#define SL_TOTAL_FAN_POP					(4)

#define SL_AIRCOND_ON_MILESTONE				(100)
#define SL_OUTSIDE_TEMP_MILESTONE			(24)
#define SL_OUTSIDE_HUMIDITY_MILESTONE		(90)
#define SL_INSIDE_TEMP_CRITICAL_MILESTONE	(35)

typedef struct {
	uint8_t temperature[SL_TOTAL_SENSOR_TEMPERATURE];	/* *C */
	uint8_t humidity[SL_TOTAL_SENSOR_HUMIDITY];			/* RH% */
	uint8_t general_input[SL_TOTAL_GENERAL_INPUT];		/* HIGH/LOW */
	uint8_t general_output[SL_TOTAL_GENERAL_OUTPUT];	/* HIGH/LOW */
	uint8_t fan_pop[SL_TOTAL_FAN_POP];					/* (0 -> 100)% */
	uint8_t fan_dev;									/* (0 -> 100)% */
	uint8_t power_output_status;						/* ON/OFF */
	uint32_t power_output_current;						/* mA */
} sl_sensors_t;

typedef struct {
	uint8_t mode;
	uint8_t power_status;
	uint8_t fan_status[SL_TOTAL_FAN_POP];
} sl_pop_fan_ctrl_t;

typedef struct {
	uint8_t general_output[SL_TOTAL_GENERAL_OUTPUT];
} sl_io_ctrl_t;

#endif //__APP_DATA_H__
