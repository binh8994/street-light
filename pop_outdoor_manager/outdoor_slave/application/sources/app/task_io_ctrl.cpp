#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"

#include "../sys/sys_dbg.h"
#include "../sys/sys_ctrl.h"

#include "../common/utils.h"

#include "../driver/general_output/general_output.h"

#include "app.h"
#include "app_if.h"
#include "app_dbg.h"
#include "app_data.h"

#include "task_list.h"
#include "task_life.h"
#include "task_sensor.h"
#include "task_io_ctrl.h"
#include "task_if.h"

static void set_io_ctrl_status(sl_io_ctrl_t* sl_io_ctrl);
static void get_io_ctrl_status(sl_io_ctrl_t* sl_io_ctrl);

void task_io_ctrl(ak_msg_t* msg) {
	switch (msg->sig) {
	case SL_IO_CTRL_CONTROL_REQ: {
		APP_DBG("SL_IO_CTRL_CONTROL_REQ\n");
		sl_io_ctrl_t st_sl_io_ctrl;
		mem_cpy(&st_sl_io_ctrl, get_data_common_msg(msg), sizeof(sl_io_ctrl_t));
		set_io_ctrl_status(&st_sl_io_ctrl);
	}
		break;

	case SL_IO_CTRL_CONTROL_INFO_REQ: {
		APP_DBG("SL_IO_CTRL_CONTROL_INFO_REQ\n");
		uint8_t des_sig;
		mem_cpy(&des_sig, get_data_common_msg(msg), sizeof(uint8_t));

		sl_io_ctrl_t st_sl_io_ctrl;
		get_io_ctrl_status(&st_sl_io_ctrl);

		ak_msg_t* s_msg = get_common_msg();

		set_if_des_task_id(s_msg, msg->if_src_task_id);
		set_if_src_task_id(s_msg, SL_TASK_IO_CTRL_ID);
		set_if_des_type(s_msg, msg->if_src_type);
		set_if_src_type(s_msg, IF_TYPE_CPU_SERIAL_SL);
		set_if_sig(s_msg, des_sig);
		set_if_data_common_msg(s_msg, (uint8_t*)&st_sl_io_ctrl, sizeof(sl_io_ctrl_t));

		set_msg_sig(s_msg, SL_IF_COMMON_MSG_OUT);
		task_post(SL_TASK_IF_ID, s_msg);
	}
		break;

	default:
		break;
	}
}

void set_io_ctrl_status(sl_io_ctrl_t* sl_io_ctrl) {
	for (uint32_t i = 0; i < SL_TOTAL_GENERAL_OUTPUT; i++) {
		set_general_output_status(i, sl_io_ctrl->general_output[i]);
	}
}

void get_io_ctrl_status(sl_io_ctrl_t* sl_io_ctrl) {
	for (uint32_t i = 0; i < SL_TOTAL_GENERAL_OUTPUT; i++) {
		sl_io_ctrl->general_output[i] = get_general_output_status(i);
	}
}
