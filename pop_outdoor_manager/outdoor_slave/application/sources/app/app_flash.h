#ifndef __APP_FLASH_H__
#define __APP_FLASH_H__

#include <stdint.h>
#include <stdbool.h>

#include "app_data.h"

#define APP_FLASH_SETTING_INFO_SECTOR			(0x0000)
#define APP_FLASH_AIRCOND_INFO_SECTOR			(0x1000)
#define APP_FLASH_NETWORK_INFO_SECTOR			(0x2000)

#define APP_FLASH_LOG_SECTOR_1					(0x3000)
#define APP_FLASH_LOG_SECTOR_2					(0x4000)
#define APP_FLASH_LOG_SECTOR_3					(0x5000)

#define APP_FLASH_DBG_SECTOR_1					(0x6000)

#define APP_FLASH_FIRMWARE_INFO_SECTOR_1		(0x7000)

#define APP_FLASH_FATAL_LOG_SECTOR				(0x8000)

#define APP_FLASH_FIRMWARE_START_ADDR			(0x80000)
#define APP_FLASH_FIRMWARE_BLOCK_64K_SIZE		(2)

/**
  *****************************************************************************
  * default setting value.
  *
  *****************************************************************************
  */

#define APP_SETTING_DEFAUL_TOTAL_AIR_COND				(2)
#define APP_SETTING_DEFAUL_TOTAL_AIR_COND_ALTERNATE		(1)
#define APP_SETTING_DEFAUL_TOTAL_AIR_COND_MODE_AUTO		(1) /* 1: AUTO, 2: MANUAL */

#define APP_SETTING_DEFAUL_MILESTONE_TEMP_COOL			(20)
#define APP_SETTING_DEFAUL_MILESTONE_TEMP_NORMAL		(25)
#define APP_SETTING_DEFAUL_MILESTONE_TEMP_HOT			(30)

#define APP_SETTING_DEFAUL_MILESTONE_AIR_COND_ON		(100)

#define APP_SETTING_DEFAUL_ERASE_LOG_MEM_EN				(1)

#define APP_SETTING_DEFAUL_TIME_AIR_COUNTER				(0)
#define APP_SETTING_NUMBER_TIME_AIR_RANGE				(8)

#define APP_SETTING_DEFAUL_TEMP_CALIBRATION				(0)
#define APP_SETTING_DEFAUL_HUM_CALIBRATION				(20)

#define APP_SETTING_DEFAUL_OPTS_TEMP_CALIBRATION		(0)
#define APP_SETTING_DEFAUL_OPTS_HUM_CALIBRATION			(0)

#define APP_SETTING_DEFAUL_NODE_CHANEL					(90)
#define APP_SETTING_DEFAUL_NODE_ADDR					(1)
#define APP_SETTING_DEFAUL_NODE_SERVER_ADDR				(0)

#define FLASH_WRITE_NG									(0)
#define FLASH_WRITE_OK									(1)

extern void flash_read_setting_info();
extern void flash_read_network_info();
extern void flash_read_aircond_info();

extern uint8_t flash_write_setting_info();
extern uint8_t flash_write_network_info();
extern uint8_t flash_write_aircond_info();

#endif //__APP_FLASH_H__
