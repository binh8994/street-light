#include <stdbool.h>
#include <stdint.h>

#include "app.h"
#include "app_if.h"
#include "app_dbg.h"
#include "app_data.h"

#include "task_if.h"
#include "task_list.h"
#include "task_list_if.h"

#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"
#include "../ak/timer.h"

#include "../common/utils.h"

#include "../sys/sys_dbg.h"
#include "../sys/sys_irq.h"
#include "../sys/sys_io.h"

static void cpu_serial_if_forward_msg(ak_msg_t* msg);

void task_if(ak_msg_t* msg) {
	if (msg->if_des_type == IF_TYPE_CPU_SERIAL_MT ||
			msg->if_des_type == IF_TYPE_CPU_SERIAL_SL) {
		cpu_serial_if_forward_msg(msg);
	}
}

void cpu_serial_if_forward_msg(ak_msg_t* msg) {
	switch (msg->sig) {
	case SL_IF_PURE_MSG_IN:	{
		APP_DBG("SL_IF_PURE_MSG_IN\n");
		msg_inc_ref_count(msg);

		set_msg_sig(msg, msg->if_sig);
		set_msg_src_task_id(msg, msg->if_src_task_id);
		task_post(msg->if_des_task_id, msg);
	}
		break;

	case SL_IF_COMMON_MSG_IN: {
		APP_DBG("SL_IF_COMMON_MSG_IN\n");
		msg_inc_ref_count(msg);

		set_msg_sig(msg, msg->if_sig);
		set_msg_src_task_id(msg, msg->if_src_task_id);
		task_post(msg->if_des_task_id, msg);
	}
		break;

	case SL_IF_PURE_MSG_OUT: {
		APP_DBG("SL_IF_PURE_MSG_OUT\n");
		msg_inc_ref_count(msg);
		set_msg_sig(msg, SL_CPU_SERIAL_IF_PURE_MSG_OUT);
		task_post(SL_TASK_CPU_SERIAL_IF_ID, msg);
	}
		break;

	case SL_IF_COMMON_MSG_OUT: {
		APP_DBG("SL_IF_COMMON_MSG_OUT\n");
		msg_inc_ref_count(msg);
		set_msg_sig(msg, SL_CPU_SERIAL_IF_COMMON_MSG_OUT);
		task_post(SL_TASK_CPU_SERIAL_IF_ID, msg);
	}
		break;

	default:
		break;
	}
}
