/**
 ******************************************************************************
 * @author: ThanNT
 * @date:   30/1/2017
 * @brief:  app external signal define
 ******************************************************************************
**/
#ifndef __APP_IF_H__
#define __APP_IF_H__
#include <stdint.h>

/*****************************************************************************/
/* task MT_SYS define.
 */
/*****************************************************************************/
/* define timer */
/* define signal */
#define MT_SYS_WATCH_DOG_REPORT_REQ					(1)

/*****************************************************************************/
/*  task MT_RF24 define.
 */
/*****************************************************************************/
/* define timer */
#define MT_RF24_IF_TIMER_PACKET_DELAY_INTERVAL		(100)

/* define signal */
#define MT_RF24_IF_PURE_MSG_OUT						(1)
#define MT_RF24_IF_COMMON_MSG_OUT					(2)
#define MT_RF24_IF_TIMER_PACKET_DELAY				(3)

/*****************************************************************************/
/*  task MT_CONSOLE define
 */
/*****************************************************************************/
/* define timer */

/* define signal */
#define MT_CONSOLE_INTERNAL_LOGIN_CMD				(1)

/*****************************************************************************/
/* task MT_IF define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
#define MT_IF_PURE_MSG_IN							(1)
#define MT_IF_PURE_MSG_OUT							(2)
#define MT_IF_COMMON_MSG_IN							(3)
#define MT_IF_COMMON_MSG_OUT						(4)

/*****************************************************************************/
/* task MT_CLOUD define.
 */
/*****************************************************************************/
/* define timer */
/* define signal */
#define MT_CLOUD_SL_SYNC_OK							(1)
#define MT_CLOUD_SL_SYNC_ERR						(2)
#define MT_CLOUD_SL_SENSOR_REPORT_REP				(3)
#define MT_CLOUD_MQTT_AIRCOND_CONTROL_REQ			(4)
#define MT_CLOUD_MQTT_SET_SL_SETTINGS_REQ			(5)
#define MT_CLOUD_SYNC_LS_SETTINGS_REP				(6)
#define MT_CLOUD_POP_CTRL_MODE_SWITCH_RES_OK		(7)
#define MT_CLOUD_POP_CTRL_MODE_SWITCH_RES_ERR		(8)
#define MT_CLOUD_POP_CTRL_CONTROL_RES_OK			(9)
#define MT_CLOUD_POP_CTRL_CONTROL_RES_ERR			(10)
#define MT_CLOUD_POP_CTRL_GET_INFO_REQ				(11)
#define MT_CLOUD_POP_CTRL_GET_INFO_RES				(12)
#define MT_CLOUD_IO_CTRL_GET_INFO_REQ				(13)
#define MT_CLOUD_IO_CTRL_GET_INFO_RES				(14)
#define MT_CLOUD_IO_CTRL_CONTROL_REQ				(15)

/*****************************************************************************/
/* task MT_SNMP define.
 */
/*****************************************************************************/
/* define timer */
/* define signal */
#define MT_SNMP_SL_SYNC_OK							(1)
#define MT_SNMP_SL_SYNC_ERR							(2)
#define MT_SNMP_SL_SENSOR_REPORT_REP				(3)
#define MT_SNMP_SYNC_LS_SETTINGS_REP				(4)

/*****************************************************************************/
/* task MT_SM define
 */
/*****************************************************************************/
/* define timer */
#define MT_SM_SL_SYNC_REQ_INTERVAL					(3000)
#define MT_SM_SL_SYNC_REQ_TO_INTERVAL				(2000)

/* define signal */
#define MT_SM_SL_SYNC_REQ							(1)
#define MT_SM_SL_SYNC_REQ_TO						(2)
#define MT_SM_SL_SYNC_RES							(3)
#define MT_SM_SENSOR_REPORT_REQ						(4)
#define MT_SM_SL_SENSOR_REPORT_RES					(5)
#define MT_SM_SL_INIT_SETTING_REQ					(6)
#define MT_SM_SL_INIT_SETTING_RES					(7)
#define MT_SM_SL_SET_SETTINGS_REQ					(8)
#define MT_SM_SL_SET_SETTINGS_RES					(9)
#define MT_SM_SL_GET_SETTINGS_REQ					(10)
#define MT_SM_SL_GET_SETTINGS_RES					(11)
#define MT_SM_SL_POP_CTRL_MODE_SWITCH_REQ			(12)
#define MT_SM_SL_POP_CTRL_MODE_SWITCH_RES_OK		(13)
#define MT_SM_SL_POP_CTRL_MODE_SWITCH_RES_ERR		(14)
#define MT_SM_SL_POP_CTRL_CONTROL_REQ				(15)
#define MT_SM_SL_POP_CTRL_CONTROL_RES_OK			(16)
#define MT_SM_SL_POP_CTRL_CONTROL_RES_ERR			(17)
#define MT_SM_SL_IO_CONTROL_REQ						(18)
#define MT_SM_SL_IO_CONTROL_RES						(19)

/*****************************************************************************/
/* task MT_SENSOR define.
 */
/*****************************************************************************/
/* define timer */
#define MT_SENSOR_SL_SENSOR_REPORT_REQ_INTERVAL		(10000)
#define MT_SENSOR_SL_SENSOR_REPORT_REQ_TO_INTERVAL	(2000)

/* define signal */
#define MT_SENSOR_SL_SENSOR_REPORT_REQ				(1)
#define MT_SENSOR_SL_SENSOR_REPORT_REQ_TO			(2)
#define MT_SENSOR_SL_SENSOR_REPORT_RES				(3)

#endif //__APP_IF_H__
