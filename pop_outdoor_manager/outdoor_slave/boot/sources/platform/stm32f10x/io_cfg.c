/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   05/09/2016
 * @Update:
 * @AnhHH: Add io function for sth11 sensor.
 ******************************************************************************
**/
#include <stdint.h>
#include <stdbool.h>

#include "io_cfg.h"
#include "stm32.h"
#include "arduino/Arduino.h"

#include "../sys/sys_dbg.h"

#include "../common/utils.h"
#include "../app/app_dbg.h"

/******************************************************************************
* led status function
*******************************************************************************/
void led_life_init() {
	GPIO_InitTypeDef        GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(LED_LIFE_IO_CLOCK, ENABLE);

	GPIO_InitStructure.GPIO_Pin = LED_LIFE_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
}

void led_life_on() {
	GPIO_SetBits(LED_LIFE_IO_PORT, LED_LIFE_IO_PIN);
}

void led_life_off() {
	GPIO_ResetBits(LED_LIFE_IO_PORT, LED_LIFE_IO_PIN);
}
/******************************************************************************
* led door function
*******************************************************************************/
void led_door_init() {
	GPIO_InitTypeDef        GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(LED_DOOR_IO_CLOCK, ENABLE);

	GPIO_InitStructure.GPIO_Pin = LED_DOOR_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(LED_DOOR_IO_PORT, &GPIO_InitStructure);
}

void led_door_on() {
	GPIO_ResetBits(LED_DOOR_IO_PORT, LED_DOOR_IO_PIN);
}

void led_door_off() {
	GPIO_SetBits(LED_DOOR_IO_PORT, LED_DOOR_IO_PIN);
}
/******************************************************************************
* led fire function
*******************************************************************************/
void led_fire_init() {
	GPIO_InitTypeDef        GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(LED_FIRE_IO_CLOCK, ENABLE);

	GPIO_InitStructure.GPIO_Pin = LED_FIRE_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(LED_FIRE_IO_PORT, &GPIO_InitStructure);
}

void led_fire_on() {
	GPIO_ResetBits(LED_FIRE_IO_PORT, LED_FIRE_IO_PIN);
}

void led_fire_off() {
	GPIO_SetBits(LED_FIRE_IO_PORT, LED_FIRE_IO_PIN);
}
/******************************************************************************
* led smoke function
*******************************************************************************/
void led_smoke_init() {
	GPIO_InitTypeDef        GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(LED_SMOKE_IO_CLOCK, ENABLE);

	GPIO_InitStructure.GPIO_Pin = LED_SMOKE_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(LED_SMOKE_IO_PORT, &GPIO_InitStructure);
}

void led_smoke_on() {
	GPIO_ResetBits(LED_SMOKE_IO_PORT, LED_SMOKE_IO_PIN);
}

void led_smoke_off() {
	GPIO_SetBits(LED_SMOKE_IO_PORT, LED_SMOKE_IO_PIN);
}
/******************************************************************************
* led water function
*******************************************************************************/
void led_water_init() {
	GPIO_InitTypeDef        GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(LED_WATER_IO_CLOCK, ENABLE);

	GPIO_InitStructure.GPIO_Pin = LED_WATER_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(LED_WATER_IO_PORT, &GPIO_InitStructure);
}

void led_water_on() {
	GPIO_ResetBits(LED_WATER_IO_PORT, LED_WATER_IO_PIN);
}

void led_water_off() {
	GPIO_SetBits(LED_WATER_IO_PORT, LED_WATER_IO_PIN);
}
/******************************************************************************
* nfr24l01 IO function
*******************************************************************************/
void nrf24l01_io_ctrl_init() {
	/* CE / CSN / IRQ */
	GPIO_InitTypeDef        GPIO_InitStructure;
	EXTI_InitTypeDef        EXTI_InitStruct;
	NVIC_InitTypeDef        NVIC_InitStruct;

	RCC_APB2PeriphClockCmd(NRF_CE_IO_CLOCK, ENABLE);
	RCC_APB2PeriphClockCmd(NRF_CSN_IO_CLOCK, ENABLE);

	/*CE -> PA8*/
	GPIO_InitStructure.GPIO_Pin = NRF_CE_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(NRF_CE_IO_PORT, &GPIO_InitStructure);

	/*CNS -> PB9*/
	GPIO_InitStructure.GPIO_Pin = NRF_CSN_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(NRF_CSN_IO_PORT, &GPIO_InitStructure);

	/* IRQ -> PB1 */
	GPIO_InitStructure.GPIO_Pin = NRF_IRQ_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(NRF_IRQ_IO_PORT, &GPIO_InitStructure);

	GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource1);

	EXTI_InitStruct.EXTI_Line = EXTI_Line1;
	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_Init(&EXTI_InitStruct);

	NVIC_InitStruct.NVIC_IRQChannel = EXTI1_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);
}

void nrf24l01_ce_low() {
	GPIO_ResetBits(NRF_CE_IO_PORT, NRF_CE_IO_PIN);
}

void nrf24l01_ce_high() {
	GPIO_SetBits(NRF_CE_IO_PORT, NRF_CE_IO_PIN);
}

void nrf24l01_csn_low() {
	GPIO_ResetBits(NRF_CSN_IO_PORT, NRF_CSN_IO_PIN);
}

void nrf24l01_csn_high() {
	GPIO_SetBits(NRF_CSN_IO_PORT, NRF_CSN_IO_PIN);
}

/******************************************************************************
* flash IO config
*******************************************************************************/
void flash_io_ctrl_init() {
	GPIO_InitTypeDef        GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(FLASH_CE_IO_CLOCK, ENABLE);

	GPIO_InitStructure.GPIO_Pin = FLASH_CE_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(FLASH_CE_IO_PORT, &GPIO_InitStructure);
}

void flash_cs_low() {
	GPIO_ResetBits(FLASH_CE_IO_PORT, FLASH_CE_IO_PIN);
}

void flash_cs_high() {
	GPIO_SetBits(FLASH_CE_IO_PORT, FLASH_CE_IO_PIN);
}

uint8_t flash_transfer(uint8_t data) {
	unsigned long rxtxData = data;

	/* waiting send idle then send data */
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET);
	SPI_I2S_SendData(SPI1, (uint8_t)rxtxData);

	/* waiting conplete rev data */
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET);
	rxtxData = (uint8_t)SPI_I2S_ReceiveData(SPI1);

	return (uint8_t)rxtxData;
}

/******************************************************************************
* ir IO function
*******************************************************************************/
void timer_50us_init() {
}

void timer_50us_enable() {
}

void timer_50us_disable() {
}

void ir_rev_io_init() {
}

void ir_rev_io_irq_disable() {
}

void ir_rev_io_irq_enable() {
}

void ir_carrier_freq_init() {
}

void ir_carrier_freq_on() {
}

void ir_carrier_freq_off() {
}

/******************************************************************************
* direction IR IO function
*******************************************************************************/
void ir_dir_io_config(){
}

void ir_select_direction(uint8_t ir_number){
	(void)ir_number;
}

/******************************************************************************
* sht11 IO function
*******************************************************************************/
void sht1x_clk_input_mode(){
}

void sht1x_clk_output_mode(){
}

void sht1x_clk_digital_write_low() {
}

void sht1x_clk_digital_write_high(){
}

void sht1x_data_input_mode(){
}

void sht1x_data_output_mode(){
}

void sht1x_data_digital_write_low(){
}

void sht1x_data_digital_write_high(){
}

int sht1x_data_digital_read(){
	return 0;
}

/******************************************************************************
* ds1302 IO function
*******************************************************************************/
/* rst pin config*/
void ds1302_ce_input_mode() {
}

void ds1302_ce_output_mode() {
}

void ds1302_ce_digital_write_low() {
}

void ds1302_ce_digital_write_high(){
}

/* scl pin config*/
void ds1302_clk_input_mode() {
}

void ds1302_clk_output_mode() {
}

void ds1302_clk_digital_write_low() {
}

void ds1302_clk_digital_write_high(){
}

/* sda pin config*/
void ds1302_data_input_mode() {
}

void ds1302_data_output_mode() {
}

void ds1302_data_digital_write_low() {
}

void ds1302_data_digital_write_high() {
}

uint8_t ds1302_data_digital_read(){
	return 0;
}

/******************************************************************************
* hs1101 IO function
* config DAC, COMP, PWM for read hs1101 function
*******************************************************************************/
void io_cfg_dac_hs1101() {
}

void io_cfg_comp_hs1101() {
}

void io_cfg_timer3_hs1101() {
}

void io_cfg_timer4_hs1101() {
}

uint32_t io_timer4_get_capture() {
	return 0;
}

void io_reset_timer4_capture() {
}

void io_start_timer4_capture() {
}

void io_hs1101_read_enable() {
}

void io_hs1101_read_disable() {
}

/******************************************************************************
* adc function
* + CT sensor
* + themistor sensor
* Note: MUST be enable internal clock for adc module.
*******************************************************************************/
void io_cfg_adc1(void) {
	ADC_InitTypeDef ADC_InitStructure;
	RCC_HSICmd(ENABLE);
	while(RCC_GetFlagStatus(RCC_FLAG_HSIRDY) == RESET);

	/* Enable ADC1 clock */
	RCC_ADCCLKConfig(RCC_PCLK2_Div8);
	RCC_APB2PeriphClockCmd(CT_ADC_CLOCK , ENABLE);
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	ADC_InitStructure.ADC_ContinuousConvMode =DISABLE;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfChannel = 4;
	ADC_Init(ADC1, &ADC_InitStructure);
	ADC_Cmd(ADC1, ENABLE);
	ADC_ResetCalibration(ADC1); // Reset previous calibration
	while(ADC_GetResetCalibrationStatus(ADC1));
	ADC_StartCalibration(ADC1); // Start new calibration (ADC must be off at that time)
	while(ADC_GetCalibrationStatus(ADC1));
}

void adc_ct_io_cfg() {
	GPIO_InitTypeDef    GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(CT_ADC_IO_CLOCK, ENABLE);

	GPIO_InitStructure.GPIO_Pin =  CT2_ADC_PIN | CT4_ADC_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(CT_ADC_PORT, &GPIO_InitStructure);
}

void adc_thermistor_io_cfg() {
	GPIO_InitTypeDef    GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(THER_ADC_IO_CLOCK, ENABLE);

	GPIO_InitStructure.GPIO_Pin = THER_ADC_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(THER_ADC_PORT, &GPIO_InitStructure);
}

uint16_t adc_ct_io_read(uint8_t chanel) {
	ADC_RegularChannelConfig(ADC1, chanel, 1, ADC_SampleTime_7Cycles5);

	ADC_SoftwareStartConvCmd(ADC1, ENABLE);
	while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);
	return ADC_GetConversionValue(ADC1);
}

uint16_t adc_thermistor_io_read(uint8_t chanel) {
	ADC_RegularChannelConfig(ADC1, chanel, 1, ADC_SampleTime_7Cycles5);

	ADC_SoftwareStartConvCmd(ADC1, ENABLE);
	while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);
	return ADC_GetConversionValue(ADC1);
}

void adc_wr_sensor_io_cfg() {
	GPIO_InitTypeDef    GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(ADC_WR_SENSOR_IO_CLOCK, ENABLE);

	GPIO_InitStructure.GPIO_Pin = WATER_SENSOR_ADC_PIN | DOOR_SENSOR_ADC_PIN | SMOKE_SENSOR_ADC_PIN | FIRE_SENSOR_ADC_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init(ADC_WR_SENSOR_IO_PORT, &GPIO_InitStructure);

}

uint16_t adc_wr_sensor_water_read() {
	ADC_RegularChannelConfig(ADC1, WATER_SENSOR_ADC_PIN, 1, ADC_SampleTime_239Cycles5);

	ADC_SoftwareStartConvCmd(ADC1, ENABLE);
	while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);

	return ADC_GetConversionValue(ADC1);

}

uint16_t adc_wr_sensor_door_read() {
	ADC_RegularChannelConfig(ADC1, DOOR_SENSOR_ADC_PIN, 1, ADC_SampleTime_239Cycles5);

	ADC_SoftwareStartConvCmd(ADC1, ENABLE);
	while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);
	return ADC_GetConversionValue(ADC1);
}

uint16_t adc_wr_sensor_smoke_read() {
	ADC_RegularChannelConfig(ADC1, SMOKE_SENSOR_ADC_PIN, 1, ADC_SampleTime_239Cycles5);

	ADC_SoftwareStartConvCmd(ADC1, ENABLE);
	while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);
	return ADC_GetConversionValue(ADC1);

}

uint16_t adc_wr_sensor_fire_read() {
	ADC_RegularChannelConfig(ADC1, FIRE_SENSOR_ADC_PIN, 1, ADC_SampleTime_239Cycles5);

	ADC_SoftwareStartConvCmd(ADC1, ENABLE);
	while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);
	return ADC_GetConversionValue(ADC1);
}



/******************************************************************************
* ssd1306 oled IO function
*******************************************************************************/
void ssd1306_clk_input_mode() {
}

void ssd1306_clk_output_mode() {
}

void ssd1306_clk_digital_write_low() {
}

void ssd1306_clk_digital_write_high() {
}

void ssd1306_data_input_mode() {
}

void ssd1306_data_output_mode() {
}

void ssd1306_data_digital_write_low() {
}

void ssd1306_data_digital_write_high() {
}

int ssd1306_data_digital_read() {
	return 0;
}

