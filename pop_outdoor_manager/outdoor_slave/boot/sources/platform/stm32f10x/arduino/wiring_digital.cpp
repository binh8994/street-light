#include "Arduino.h"
#include "../io_cfg.h"
#include "../../../sys/sys_dbg.h"

void pinMode(uint8_t pin, uint8_t mode) {
	(void)pin;
	(void)mode;
}

void digitalWrite(uint8_t pin, uint8_t val) {
	(void)pin;
	(void)val;
}

int digitalRead(uint8_t pin) {
	(void)pin;
	return 0;
}
