#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "app.h"
#include "app_if.h"
#include "app_data.h"
#include "app_dbg.h"

#include "task_list.h"
#include "task_list_if.h"
#include "task_firmware.h"

q_msg_t mt_task_firmware_mailbox;

void* mt_task_firmware_entry(void*) {
	task_mask_started();
	wait_all_tasks_started();

	APP_DBG("[STARTED] mt_task_firmware_entry\n");

	while (1) {
		while (msg_available(MT_TASK_FIRMWARE_ID)) {
			/* get messge */
			ak_msg_t* msg = rev_msg(MT_TASK_FIRMWARE_ID);

			switch (msg->header->sig) {
			case MT_FIRMWARE_SL_FW_UPDATE_REQ: {
				APP_DBG("MT_FIRMWARE_SL_FW_UPDATE_REQ\n");
			}
				break;

			case MT_FIRMWARE_SL_FW_INFO_RES: {
				APP_DBG("MT_FIRMWARE_SL_FW_INFO_RES\n");
			}
				break;

			case MT_FIRMWARE_SL_FW_TRANF_REQ: {
				APP_DBG("MT_FIRMWARE_SL_FW_TRANF_REQ\n");
			}
				break;

			case MT_FIRMWARE_SL_FW_TRANF_RES: {
				APP_DBG("MT_FIRMWARE_SL_FW_TRANF_RES\n");
			}
				break;

			case MT_FIRMWARE_SL_FW_TRANF_DATA_RES: {
				APP_DBG("MT_FIRMWARE_SL_FW_TRANF_DATA_RES\n");
			}
				break;

			case MT_FIRMWARE_SL_FW_CHECKSUM_CORRECT_RES: {
				APP_DBG("MT_FIRMWARE_SL_FW_CHECKSUM_CORRECT_RES\n");
			}
				break;

			case MT_FIRMWARE_SL_FW_UPDATE_COMPLETED_REP: {
				APP_DBG("MT_FIRMWARE_SL_FW_UPDATE_COMPLETED_REP\n");
			}
				break;

			case MT_FIRMWARE_SL_FW_CHECKSUM_INCORRECT_RES: {
				APP_DBG("MT_FIRMWARE_SL_FW_CHECKSUM_INCORRECT_RES\n");
			}
				break;

			case MT_FIRMWARE_SL_FW_REQ_TIMEOUT: {
				APP_DBG("MT_FIRMWARE_SL_FW_REQ_TIMEOUT\n");
			}
				break;

			default:
				break;
			}

			/* free message */
			free_msg(msg);
		}

		usleep(1000);
	}

	return (void*)0;
}
