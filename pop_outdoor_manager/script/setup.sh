echo "[UPDATE]"
apt-get update

echo "[INSTALL LIB PACKET]"
apt-get install -y debhelper libc-ares-dev libssl-dev libwrap0-dev python-all python3-all uthash-dev uuid-dev libuuid1 xsltproc docbook-xsl cmake automake snmp snmpd snmp-mibs-downloader

echo "[INSTALL MQTT]"
cd ../outdoor_master_gateway/resources/mosquitto/
make -j4
make install

echo "[INSTALL CURL]"
cd ../curl/
cmake .
make -j4
make install

echo "[DEPLOY SNMP]"
cd ../../snmp/
./snmp_deploy

echo "[INSTALL MASTER GATEWAY]"
cd ../gateway/
make clean
make -j4
make install


echo "[INSTALL GATEWAY MANAGER]"
cd ../../gateway_manager/
make -j4
make install

