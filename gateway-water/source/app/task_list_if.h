#ifndef __TASK_LIST_IF_H__
#define __TASK_LIST_IF_H__

#define SL_TASK_SHELL_ID				1
#define SL_TASK_LIFE_ID					2
#define SL_TASK_SENSOR_ID				3
#define SL_TASK_POP_CTRL_ID				4
#define SL_TASK_DEV_CTRL_ID				5
#define SL_TASK_IO_CTRL_ID				6
#define SL_TASK_IF_ID					7
#define SL_TASK_CPU_SERIAL_IF_ID		8
#define SL_TASK_SM_ID					9

#endif //__TASK_LIST_IF_H__
