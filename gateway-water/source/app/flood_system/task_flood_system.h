#ifndef __TASK_FLOOD_SYSTEM_H__
#define __TASK_FLOOD_SYSTEM_H__

#include "../ak/message.h"

extern q_msg_t mt_task_flood_system_mailbox;
extern void* mt_task_flood_system_entry(void*);

#endif //__TASK_FLOOD_SYSTEM_H__
