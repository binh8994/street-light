#ifndef __TASK_CONSOLE_H__
#define __TASK_CONSOLE_H__

#include "../ak/message.h"

extern q_msg_t mt_task_console_mailbox;
extern void* mt_task_console_entry(void*);

#endif //__TASK_CONSOLE_H__
