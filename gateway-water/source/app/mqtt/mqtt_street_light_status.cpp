#include "mqtt_street_light_status.h"

#include "app_dbg.h"
#include "app.h"

mqtt_street_light_status::mqtt_street_light_status(const char *id, const char *host, int port) : mosquittopp(id) {
	/* init private data */
	m_connect_ok_flag = -1;
	m_mid = 1;

	/* init mqtt */
	mosqpp::lib_init();

	/* connect */
	username_pw_set(gateway_configure_parameter.street_light_gateway.user_name_view, gateway_configure_parameter.street_light_gateway.user_psk_view);
	connect_async(host, port, 60);
	loop_start();
}

mqtt_street_light_status::~mqtt_street_light_status() {
	loop_stop();
	mosqpp::lib_cleanup();
}

void mqtt_street_light_status::set_topic(const char* topic) {
	m_topic = static_cast<string>(topic);
}

void mqtt_street_light_status::on_connect(int rc) {
	if (rc == 0) {
		m_connect_ok_flag = 0;
		APP_DBG("[mqtt_street_light_status] on_connect OK\n");
	}
	else {
		APP_DBG("[mqtt_street_light_status] on_connect ERROR\n");
	}
}

void mqtt_street_light_status::status_public(uint8_t* msg, uint32_t len) {
	APP_DBG("[mqtt_street_light_status][status_public] msg:%s len:%d\n", msg, len);
	publish(&m_mid, m_topic.data(), len, msg);
}

void mqtt_street_light_status::on_publish(int mid) {
	APP_DBG("[mqtt_street_light_status][on_publish] mid: %d\n", mid);
}
