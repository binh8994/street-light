#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "app.h"
#include "app_if.h"
#include "app_data.h"
#include "app_dbg.h"

#include "task_list.h"
#include "task_list_if.h"
#include "if_rf24.h"

#include "task_debug_msg.h"

q_msg_t mt_task_debug_msg_mailbox;

void* mt_task_debug_msg_entry(void*) {
	task_mask_started();
	wait_all_tasks_started();

	APP_DBG("[STARTED] mt_task_debug_msg_entry\n");

	while (1) {
		while (msg_available(MT_TASK_DEBUG_MSG_ID)) {
			/* get messge */
			ak_msg_t* msg = rev_msg(MT_TASK_DEBUG_MSG_ID);

			switch (msg->header->sig) {

			default:
				break;
			}

			/* free message */
			free_msg(msg);
		}

		usleep(10000);
	}

	return (void*)0;
}
