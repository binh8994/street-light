#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string>

#include "app.h"
#include "app_data.h"
#include "app_dbg.h"

#include "task_list.h"
#include "task_if.h"
#include "task_snmp.h"

using namespace std;

q_msg_t mt_task_snmp_mailbox;


static string snmp_folder_path;

static string sl_sensors_file_path;
static string connection_file_path;

static FILE* sl_sensors_file_obj;
static FILE* connection_file_obj;

static void update_sl_sensors_file(sl_sensors_t* sl_sensors);
static void update_connection_file(gw_connection_t* gw_connection);

void* mt_task_snmp_entry(void*) {
	task_mask_started();
	wait_all_tasks_started();
	APP_DBG("[STARTED] mt_task_snmp_entry\n");

	/* create file paths */
	snmp_folder_path		= string(APP_ROOT_PATH_RAM) + string("/snmp");
	sl_sensors_file_path	= snmp_folder_path + string("/sl_sensors.txt");
	connection_file_path	= snmp_folder_path + string("/gw_connection.txt");

	/* create snmp folder path */
	struct stat st = {0};
	if (stat(snmp_folder_path.data(), &st) == -1) {
		mkdir(snmp_folder_path.data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	}

	while (1) {

		while (msg_available(MT_TASK_SNMP_ID)) {
			/* get messge */
			ak_msg_t* msg = rev_msg(MT_TASK_SNMP_ID);

			/* handler message */
			switch (msg->header->sig) {
			case MT_SNMP_SL_SYNC_OK: {
				APP_DBG("MT_SNMP_SL_SYNC_OK\n");
				gw_connection_t gw_connection;
				app_data_get_gw_connection(&gw_connection);
				gw_connection.sl_conn = GW_CONNECTION_STATUS_CONNECTED;
				update_connection_file(&gw_connection);
			}
				break;

			case MT_SNMP_SL_SYNC_ERR: {
				APP_DBG("MT_SNMP_SL_SYNC_ERR\n");
				gw_connection_t gw_connection;
				app_data_get_gw_connection(&gw_connection);
				gw_connection.sl_conn = GW_CONNECTION_STATUS_DISCONNECTED;
				update_connection_file(&gw_connection);
			}
				break;

			case MT_SNMP_SL_SENSOR_REPORT_REP: {
				APP_DBG("MT_SNMP_SL_SENSOR_REPORT_REP\n");
				sl_sensors_t st_sl_sensors;
				get_data_common_msg(msg, (uint8_t*)&st_sl_sensors, sizeof(sl_sensors_t));
				update_sl_sensors_file(&st_sl_sensors);
			}
				break;

			default:
				break;
			}

			/* free message */
			free_msg(msg);
		}

		usleep(1000);
	}

	return (void*)0;
}

void update_sl_sensors_file(sl_sensors_t* sl_sensors) {
	sl_sensors_file_obj = fopen(connection_file_path.data(), "w+r");

	if (sl_sensors_file_obj == NULL) {
		APP_DBG("can not open %s file\n", connection_file_path.data());
	}
	else {
		APP_DBG("opened %s file\n", connection_file_path.data());

		APP_DBG("[sl_sensors_file] -> temperature_1: %d\n",			sl_sensors->temperature[0]);
		APP_DBG("[sl_sensors_file] -> temperature_2: %d\n",			sl_sensors->temperature[1]);
		APP_DBG("[sl_sensors_file] -> temperature_3: %d\n",			sl_sensors->temperature[2]);
		APP_DBG("[sl_sensors_file] -> temperature_4: %d\n",			sl_sensors->temperature[3]);
		APP_DBG("[sl_sensors_file] -> himidity_1: %d\n",			sl_sensors->humidity[0]);
		APP_DBG("[sl_sensors_file] -> himidity_2: %d\n",			sl_sensors->humidity[1]);
		APP_DBG("[sl_sensors_file] -> general_input_1: %d\n",		sl_sensors->general_input[0]);
		APP_DBG("[sl_sensors_file] -> general_input_2: %d\n",		sl_sensors->general_input[1]);
		APP_DBG("[sl_sensors_file] -> general_input_3: %d\n",		sl_sensors->general_input[2]);
		APP_DBG("[sl_sensors_file] -> general_input_4: %d\n",		sl_sensors->general_input[3]);
		APP_DBG("[sl_sensors_file] -> general_input_5: %d\n",		sl_sensors->general_input[4]);
		APP_DBG("[sl_sensors_file] -> general_input_6: %d\n",		sl_sensors->general_input[5]);
		APP_DBG("[sl_sensors_file] -> general_input_7: %d\n",		sl_sensors->general_input[6]);
		APP_DBG("[sl_sensors_file] -> general_input_8: %d\n",		sl_sensors->general_input[7]);
		APP_DBG("[sl_sensors_file] -> general_input_9: %d\n",		sl_sensors->general_input[8]);
		APP_DBG("[sl_sensors_file] -> general_input_10: %d\n",		sl_sensors->general_input[9]);
		APP_DBG("[sl_sensors_file] -> general_input_11: %d\n",		sl_sensors->general_input[10]);
		APP_DBG("[sl_sensors_file] -> general_input_12: %d\n",		sl_sensors->general_input[11]);
		APP_DBG("[sl_sensors_file] -> general_output_1: %d\n",		sl_sensors->general_output[0]);
		APP_DBG("[sl_sensors_file] -> general_output_2: %d\n",		sl_sensors->general_output[1]);
		APP_DBG("[sl_sensors_file] -> general_output_3: %d\n",		sl_sensors->general_output[2]);
		APP_DBG("[sl_sensors_file] -> general_output_4: %d\n",		sl_sensors->general_output[3]);
		APP_DBG("[sl_sensors_file] -> general_output_5: %d\n",		sl_sensors->general_output[4]);
		APP_DBG("[sl_sensors_file] -> general_output_6: %d\n",		sl_sensors->general_output[5]);
		APP_DBG("[sl_sensors_file] -> general_output_7: %d\n",		sl_sensors->general_output[6]);
		APP_DBG("[sl_sensors_file] -> general_output_8: %d\n",		sl_sensors->general_output[7]);
		APP_DBG("[sl_sensors_file] -> general_output_9: %d\n",		sl_sensors->general_output[8]);
		APP_DBG("[sl_sensors_file] -> general_output_10: %d\n",		sl_sensors->general_output[9]);
		APP_DBG("[sl_sensors_file] -> general_output_11: %d\n",		sl_sensors->general_output[10]);
		APP_DBG("[sl_sensors_file] -> general_output_12: %d\n",		sl_sensors->general_output[11]);
		APP_DBG("[sl_sensors_file] -> fan_pop_1: %d\n",				sl_sensors->fan_pop[0]);
		APP_DBG("[sl_sensors_file] -> fan_pop_2: %d\n",				sl_sensors->fan_pop[1]);
		APP_DBG("[sl_sensors_file] -> fan_pop_3: %d\n",				sl_sensors->fan_pop[2]);
		APP_DBG("[sl_sensors_file] -> fan_pop_4: %d\n",				sl_sensors->fan_pop[3]);
		APP_DBG("[sl_sensors_file] -> fan_dev: %d\n",				sl_sensors->fan_dev);
		APP_DBG("[sl_sensors_file] -> power_output_status: %d\n",	sl_sensors->power_output_status);
		APP_DBG("[sl_sensors_file] -> power_output_current: %d\n",	sl_sensors->power_output_current);

		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->temperature[0]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->temperature[1]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->temperature[2]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->temperature[3]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->humidity[0]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->humidity[1]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->general_input[0]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->general_input[1]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->general_input[2]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->general_input[3]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->general_input[4]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->general_input[5]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->general_input[6]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->general_input[7]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->general_input[8]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->general_input[9]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->general_input[10]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->general_input[11]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->general_output[0]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->general_output[1]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->general_output[2]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->general_output[3]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->general_output[4]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->general_output[5]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->general_output[6]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->general_output[7]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->general_output[8]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->general_output[9]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->general_output[10]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->general_output[11]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->fan_pop[0]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->fan_pop[1]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->fan_pop[2]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->fan_pop[3]);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->fan_dev);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->power_output_status);
		fprintf(sl_sensors_file_obj, "%d\n",	sl_sensors->power_output_current);

		fclose(sl_sensors_file_obj);
	}
}

void update_connection_file(gw_connection_t* gw_connection) {
	connection_file_obj = fopen(connection_file_path.data(), "w+r");

	if (connection_file_obj == NULL) {
		APP_DBG("can not open %s file\n", connection_file_path.data());
	}
	else {
		APP_DBG("opened %s file\n", connection_file_path.data());

		APP_DBG("sl_conn: %d\n"		, gw_connection->sl_conn);

		fprintf(connection_file_obj, "%d\n", gw_connection->sl_conn);

		fclose(connection_file_obj);
	}
}
