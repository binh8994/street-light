#ifndef __APP_H__
#define __APP_H__

#include <stdint.h>
#include "serial.h"

#define SERIAL_PRINT_EN

#define RX_BUFFER_SIZE						(9)
#define LORA_MESSAGE_DATA_SIZE					(4)
#define TYPE_CFG						(0)
#define TYPE_REPORT						(1)

#define IPCPU_SOP_CHAR						0xEF
#define SOP_STATE		0x00
#define DATA_STATE		0x01
#define FCS_STATE		0x02

typedef struct {
	uint32_t	scr_addr;
	uint32_t	des_addr;
	uint8_t		type;
} __attribute__((__packed__)) lora_header_t;

typedef struct {
	lora_header_t header;
	uint32_t data;
}  __attribute__((__packed__)) lora_message_t;

typedef struct {
	uint8_t data_index;
	uint8_t frame_sop;
	uint8_t data[RX_BUFFER_SIZE];
	uint8_t frame_fcs;
} __attribute__((__packed__)) lora_usb_stick_frame_t;


void lora_message_set_data(lora_message_t* msg, uint8_t* data, uint16_t len);
extern void lora_post_message(lora_message_t* msg);
extern uint8_t usb_stick_frame_calcfcs(uint8_t *data_ptr);

#endif //__APP_H__
