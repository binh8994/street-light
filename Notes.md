
Class A
Năng lượng thấp nhất, endd chỉ lắng nghe nhân dư liệu khi endd muốn, bằng cách gửi uplink tới gateway
Có 2 cử sổ nhận sau uplink, có thể lap trinh dc bằng phần mềm.
Cửa sổ 1 cùng kênh và datarate với uplink
Cửa sổ 2 có thể cấu hình đc kênh và datarate khác. ( cấu hình kênh khác để tránh đụng độ kênh, cấu hình datarate khác để tối ưu tốc độ downlink- từ việc phân tích dữ liệu từ uplink)
Khi  nhân đc dự liệu trong thời gian của sổ 1 thì sẽ không mở cử sổ 2

Class B
Endd đăt lịch trươc với gateway lịch trình mở của sổ lắng nghe


Class C
Endd luôn luôn lắng nghe


MAC MESSAGES FORMAT

Radio PHY layer : Preamble | PHDR | PHDR_CRC | PHYPayload | CRC*
PHYPayload: MHDR | MACPayload | MIC
MACPayload: FHDR | FPort | FRMPayload
FHDR : DevAddr | FCtrl |FCnt | FOpts
DevAddr: NwkID | NwkAddr

LORA Preamble=0x34 , Preamble len:8 symbols
GFSK Preamble=0xC194C1, Preamble len:5 bytes

FRMPayload: Chua du lieu cua application

Bit FCtrl:7 enable/disable Adaptive data rate
ADR cho phep toi uu toc do truyen du lieu cua thiet bi dau cuoi tinh

FPort 0: dung NkwSKey giai ma, MACPayload=FRMPayload
Khi Port=0 :FRMPayload=MACCommand.
Khi port !=0: FOpts=MACCommand.
FPort 1-255: dung AppSKey giai ma


msg = MHDR | FHDR | FPort | FRMPayload
cmac = aes128_cmac(NwkSKey, B0 | msg)
MIC = cmac[0..3]

**Over-the-Air Activation
#Join-request message 
	AppEUI 		|  	DevEUI 		| 	DevNonce
	(8 bytes)	  	(8 bytes)		(2 bytes Random)
	
#Join-accept message
	AppNonce			|	NetID	|	DevAddr	|	DLsetting	|	RxDelay	|	CFList
	(3 bytes Random)	

NwkSKey = aes128_encrypt(AppKey, 0x01 | AppNonce | NetID | DevNonce | pad16)
AppSKey = aes128_encrypt(AppKey, 0x02 | AppNonce | NetID | DevNonce | pad16)

**Activation by Personalization
DevAddr, NwkSKey, AppSKey are directly stored into the end-device

AppEUI: Dinh danh ung dung
NwkSKey: dung de giai ma lop MAC
AppSKey: dung de giai ma lop MAC


