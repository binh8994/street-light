#include <string.h>

#include "mqtt_street_light_ctr.h"

#include "../ak/ak.h"

#include "app.h"
#include "app_dbg.h"
#include "app_data.h"

#include "task_list.h"

mqtt_street_light_ctr::mqtt_street_light_ctr(const char *id, const char *host, int port) : mosquittopp(id, true) {
	/* init private data */
	m_connect_ok_flag = -1;
	m_mid = 1;

	/* init mqtt */
	mosqpp::lib_init();

	/* connect */
	username_pw_set(gateway_configure_parameter.street_light_gateway.user_name_view, gateway_configure_parameter.street_light_gateway.user_psk_view);
	connect_async(host, port, 60);
	loop_start();
}

mqtt_street_light_ctr::~mqtt_street_light_ctr() {
	loop_stop();
	mosqpp::lib_cleanup();
}

void mqtt_street_light_ctr::set_topic(const char* topic) {
	m_topic = static_cast<string>(topic);
}

void mqtt_street_light_ctr::on_connect(int rc) {
	if (rc == 0) {
		m_connect_ok_flag = 0;
		APP_DBG("[mqtt_street_light_ctr] on_connect OK\n");
		subscribe(NULL, m_topic.data());
	}
	else {
		APP_DBG("[mqtt_street_light_ctr] on_connect ERROR\n");
	}
}

void mqtt_street_light_ctr::on_disconnect(int rc) {
	APP_DBG("[mqtt_street_light_ctr] on_disconnect: %d \n", rc);
}

void mqtt_street_light_ctr::control_public(uint8_t* msg, uint32_t len) {
	APP_DBG("[mqtt_street_light_ctr][control_public] msg:%s len:%d\n", msg, len);
	publish(&m_mid, m_topic.data(), len, msg, 0, true);
}

void mqtt_street_light_ctr::on_publish(int mid) {
	APP_DBG("[mqtt_street_light_ctr][on_publish] mid: %d\n", mid);
}

void mqtt_street_light_ctr::on_subscribe(int mid, int qos_count, const int *granted_qos) {
	(void)granted_qos;
	APP_DBG("[mqtt_street_light_ctr][on_subscribe] mid:%d\tqos_count:%d\n", mid, qos_count);
}

void mqtt_street_light_ctr::on_message(const struct mosquitto_message *message) {
	APP_DBG("[mqtt_street_light_ctr][on_message] topic:%s\tpayloadlen:%d\n", message->topic, message->payloadlen);

	if (!strcmp(message->topic, m_topic.data())) {
		if (message->payloadlen > 0) {
			int payload_len = message->payloadlen + 1;
			char* payload = (char*)malloc(payload_len);
			memset(payload, 0 , payload_len);

			memcpy(payload, message->payload, message->payloadlen);

			//APP_DBG("[mqtt_street_light_ctr][on_message] message->payload:%s\n", payload);

			json j = json::parse(payload);
			street_light_control_t* control_payload = (street_light_control_t*)malloc(sizeof(street_light_control_t));
			street_light_control_json_to_struct(control_payload, j);

			ak_msg_t* s_msg = get_common_msg();
			set_msg_sig(s_msg, MT_STREET_LIGHT_SYSTEM_LORA_MSG_OUT);
			set_data_common_msg(s_msg, (uint8_t*)control_payload, sizeof(street_light_control_t));
			task_post(MT_TASK_STREET_LIGHT_SYSTEM_ID, s_msg);

			free(control_payload);

			/* free temp payload */
			free(payload);
		}
	}
}
