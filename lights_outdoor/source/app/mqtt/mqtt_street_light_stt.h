#ifndef __MQTT_STREET_LIGHT_STATUS_H__
#define __MQTT_STREET_LIGHT_STATUS_H__

#include <stdint.h>
#include <string>

#include <mosquittopp.h>
#include "task_cloud.h"

using namespace std;

class mqtt_street_light_stt : public mosqpp::mosquittopp {
public:
	mqtt_street_light_stt(const char *id, const char *host, int port);
	~mqtt_street_light_stt();

	void set_topic(const char* topic);

	void status_public(uint8_t* msg, uint32_t len);

	/* call back functions */
	void on_connect(int rc);
	void on_publish(int mid);

private:
	string m_topic;
	char m_connect_ok_flag;
	int m_mid;
};

#endif // __MQTT_STREET_LIGHT_STATUS_H__
