#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../ak/ak.h"
#include "../ak/timer.h"

#include "app.h"
#include "app_if.h"
#include "app_dbg.h"
#include "app_data.h"

#include "task_list.h"
#include "task_sensor.h"

q_msg_t mt_task_sensor_mailbox;

#define SL_SENSOR_REQ_RETRY_MAX							5
static uint32_t sensor_sl_sensor_req_failed_counter	= 0;

void* mt_task_sensor_entry(void*) {
	task_mask_started();
	wait_all_tasks_started();

	APP_DBG("[STARTED] mt_task_sensor_entry\n");

	//timer_set(MT_TASK_SENSOR_ID, MT_SENSOR_SL_SENSOR_REPORT_REQ, MT_SENSOR_SL_SENSOR_REPORT_REQ_INTERVAL, TIMER_ONE_SHOT);

	while (1) {
		while (msg_available(MT_TASK_SENSOR_ID)) {
			/* get messge */
			ak_msg_t* msg = rev_msg(MT_TASK_SENSOR_ID);

			switch (msg->header->sig) {
			case MT_SENSOR_SL_SENSOR_REPORT_REQ: {
				APP_DBG("MT_SENSOR_SL_SENSOR_REPORT_REQ\n");

				ak_msg_t* s_msg = get_pure_msg();
				set_msg_sig(s_msg, MT_SM_SENSOR_REPORT_REQ);
				set_msg_src_task_id(s_msg, MT_TASK_SENSOR_ID);
				task_post(MT_TASK_SM_ID, s_msg);

				timer_set(MT_TASK_SENSOR_ID, MT_SENSOR_SL_SENSOR_REPORT_REQ_TO, MT_SENSOR_SL_SENSOR_REPORT_REQ_TO_INTERVAL, TIMER_ONE_SHOT);
			}
				break;

			case MT_SENSOR_SL_SENSOR_REPORT_REQ_TO: {
				APP_DBG("MT_SENSOR_SL_SENSOR_REPORT_REQ_TO\n");
				if (sensor_sl_sensor_req_failed_counter++ < SL_SENSOR_REQ_RETRY_MAX) {
					timer_set(MT_TASK_SENSOR_ID, MT_SENSOR_SL_SENSOR_REPORT_REQ, MT_SENSOR_SL_SENSOR_REPORT_REQ_INTERVAL, TIMER_ONE_SHOT);
				}
				else {
					sensor_sl_sensor_req_failed_counter = 0;

					uint8_t error_code = APP_ERROR_CODE_TIMEOUT;

					{
						ak_msg_t* s_msg = get_common_msg();
						set_msg_sig(s_msg, MT_CLOUD_SL_SYNC_ERR);
						set_data_common_msg(s_msg, (uint8_t*)&error_code, sizeof(uint8_t));
						set_msg_src_task_id(s_msg, MT_TASK_SM_ID);
						task_post(MT_TASK_CLOUD_ID, s_msg);
					}

					{
						ak_msg_t* s_msg = get_common_msg();
						set_msg_sig(s_msg, MT_SNMP_SL_SYNC_ERR);
						set_data_common_msg(s_msg, (uint8_t*)&error_code, sizeof(uint8_t));
						set_msg_src_task_id(s_msg, MT_TASK_SM_ID);
						task_post(MT_TASK_SNMP_ID, s_msg);
					}

					{
						ak_msg_t* s_msg = get_pure_msg();
						set_msg_sig(s_msg, MT_SM_SL_SYNC_REQ);
						set_msg_src_task_id(s_msg, MT_TASK_SENSOR_ID);
						task_post(MT_TASK_SM_ID, s_msg);
					}
				}
			}
				break;

			case MT_SENSOR_SL_SENSOR_REPORT_RES: {
				APP_DBG("MT_SENSOR_SL_SENSOR_REPORT_RES\n");
				timer_remove_attr(MT_TASK_SENSOR_ID, MT_SENSOR_SL_SENSOR_REPORT_REQ_TO);

				{
					ak_msg_t* s_msg = ak_memcpy_msg(msg);
					set_msg_sig(s_msg, MT_CLOUD_SL_SENSOR_REPORT_REP);
					set_msg_src_task_id(s_msg, MT_TASK_SENSOR_ID);
					task_post(MT_TASK_CLOUD_ID, s_msg);
				}

				{
					ak_msg_t* s_msg = ak_memcpy_msg(msg);
					set_msg_sig(s_msg, MT_SNMP_SL_SENSOR_REPORT_REP);
					set_msg_src_task_id(s_msg, MT_TASK_SENSOR_ID);
					task_post(MT_TASK_SNMP_ID, s_msg);
				}

				timer_set(MT_TASK_SENSOR_ID, MT_SENSOR_SL_SENSOR_REPORT_REQ, MT_SENSOR_SL_SENSOR_REPORT_REQ_INTERVAL, TIMER_ONE_SHOT);
			}
				break;

			default:
				break;
			}

			/* free message */
			free_msg(msg);
		}

		usleep(100);
	}

	return (void*)0;
}
