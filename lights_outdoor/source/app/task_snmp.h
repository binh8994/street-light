#ifndef __TASK_SNMP_H__
#define __TASK_SNMP_H__

#include "../ak/message.h"

extern q_msg_t mt_task_snmp_mailbox;
extern void* mt_task_snmp_entry(void*);

#endif //__TASK_SNMP_H__
