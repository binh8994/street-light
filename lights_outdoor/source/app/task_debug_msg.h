#ifndef __TASK_DEBUG_MSG_H__
#define __TASK_DEBUG_MSG_H__

#include "../ak/message.h"

extern q_msg_t mt_task_debug_msg_mailbox;
extern void* mt_task_debug_msg_entry(void*);

#endif //__TASK_DEBUG_MSG_H__
