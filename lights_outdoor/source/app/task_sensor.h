#ifndef __TASK_SENSOR_H__
#define __TASK_SENSOR_H__

#include "../ak/message.h"

extern q_msg_t mt_task_sensor_mailbox;
extern void* mt_task_sensor_entry(void*);

#endif //__TASK_SENSOR_H__
