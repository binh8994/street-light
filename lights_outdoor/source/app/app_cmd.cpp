#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/socket.h>
#include <sys/stat.h>

#include <time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>

#include "../ak/ak.h"
#include "../ak/timer.h"

#include "../common/cmd_line.h"

#include "app.h"
#include "app_if.h"
#include "app_dbg.h"
#include "app_config.h"
#include "app_cmd.h"

#include "task_list.h"
#include "task_list_if.h"

static int32_t i_shell_ver(uint8_t* argv);
static int32_t i_shell_help(uint8_t* argv);
static int32_t i_shell_cfg(uint8_t* argv);
static int32_t i_shell_dbg(uint8_t* argv);
static int32_t i_shell_pop(uint8_t* argv);
static int32_t i_shell_out(uint8_t* argv);
static int32_t i_shell_flood(uint8_t* argv);
static int32_t i_shell_lora(uint8_t* argv);

cmd_line_t lgn_cmd_table[] = {
	{(const int8_t*)"ver",		i_shell_ver,			(const int8_t*)"get kernel version"},
	{(const int8_t*)"help",		i_shell_help,			(const int8_t*)"help command info"},
	{(const int8_t*)"cfg",		i_shell_cfg,			(const int8_t*)"config"},
	{(const int8_t*)"dbg",		i_shell_dbg,			(const int8_t*)"debug"},
	{(const int8_t*)"pop",		i_shell_pop,			(const int8_t*)"pop temperature control"},
	{(const int8_t*)"out",		i_shell_out,			(const int8_t*)"general output control"},
	{(const int8_t*)"flood",	i_shell_flood,			(const int8_t*)"flood sensor"},
	{(const int8_t*)"lora",		i_shell_lora,			(const int8_t*)"lora_usb_stick"},

	/* End Of Table */
	{(const int8_t*)0,(pf_cmd_func)0,(const int8_t*)0}
};

int32_t i_shell_ver(uint8_t* argv) {
	(void)argv;
	APP_PRINT("version: %s\n", AK_VERSION);
	return 0;
}

int32_t i_shell_help(uint8_t* argv) {
	uint32_t idx = 0;
	switch (*(argv + 4)) {
	default:
		APP_PRINT("\nCOMMANDS INFORMATION:\n\n");
		while(lgn_cmd_table[idx].cmd != (const int8_t*)0) {
			APP_PRINT("%s\n\t%s\n\n", lgn_cmd_table[idx].cmd, lgn_cmd_table[idx].info);
			idx++;
		}
		break;
	}
	return 0;
}

int32_t i_shell_cfg(uint8_t* argv) {
	switch (*(argv + 4)) {
	case '0': {
		app_config_parameter_t config;


		/* lora gateway */
		strcpy(config.lora_gateway.lora_host,			"1.1.1.1");
		strcpy(config.lora_gateway.mqtt_host,			"118.69.135.199");
		config.lora_gateway.mqtt_port =					1883;
		strcpy(config.lora_gateway.mqtt_user_name,		"y55fYL");
		strcpy(config.lora_gateway.mqtt_psk,			"eJwKMNV2BQwC69PC");

		/* mqtt server */
		strcpy(config.pop_gateway.gateway_id_prefix,	"iot-");
		strcpy(config.pop_gateway.gateway_id,			"pop-dev");
		strcpy(config.pop_gateway.host,					"118.69.135.199");
		config.pop_gateway.port =						1883;
		strcpy(config.pop_gateway.user_name_view,		"fiot");
		strcpy(config.pop_gateway.user_psk_view	,		"ZmlvdEA5MTFmaW90");
		strcpy(config.pop_gateway.user_name_control,	"fciot");
		strcpy(config.pop_gateway.user_psk_control,		"ZmNpb3RAOTExOTExZmNpb3Q=");

		/* street light mqtt server */
		strcpy(config.street_light_gateway.gateway_id_prefix,	"iot-");
		strcpy(config.street_light_gateway.gateway_id,			"street_light-dev");
		strcpy(config.street_light_gateway.host,				"118.69.135.199");
		config.street_light_gateway.port =						1883;
		strcpy(config.street_light_gateway.user_name_view,		"Z4BvdC");
		strcpy(config.street_light_gateway.user_psk_view	,	"A3zw8zVTEpR6hZfH");
		strcpy(config.street_light_gateway.user_name_control,	"QET9pd");
		strcpy(config.street_light_gateway.user_psk_control,	"9zMRZyrPZtTEyCp3");

		gateway_configure.write_config_data(&config);
		gateway_configure.parser_config_file(&config);

		APP_DBG("lora_gateway.lora_host:%s\n"			, config.lora_gateway.lora_host);
		APP_DBG("lora_gateway.mqtt_host:%s\n"			, config.lora_gateway.mqtt_host);
		APP_DBG("lora_gateway.mqtt_port:%d\n"			, config.lora_gateway.mqtt_port);
		APP_DBG("lora_gateway.mqtt_user_name:%s\n"		, config.lora_gateway.mqtt_user_name);
		APP_DBG("lora_gateway.mqtt_psk:%s\n"			, config.lora_gateway.mqtt_psk);

		APP_DBG("mqtt_server.gateway_id_prefix:%s\n"	, config.pop_gateway.gateway_id_prefix);
		APP_DBG("mqtt_server.gateway_id:%s\n"			, config.pop_gateway.gateway_id);
		APP_DBG("mqtt_server.host:%s\n"					, config.pop_gateway.host);
		APP_DBG("mqtt_server.port:%d\n"					, config.pop_gateway.port);
		APP_DBG("mqtt_server.user_name_view:%s\n"		, config.pop_gateway.user_name_view);
		APP_DBG("mqtt_server.user_psk_view:%s\n"		, config.pop_gateway.user_psk_view);
		APP_DBG("mqtt_server.user_name_control:%s\n"	, config.pop_gateway.user_name_control);
		APP_DBG("mqtt_server.user_psk_control:%s\n"		, config.pop_gateway.user_psk_control);

		APP_DBG("mqtt_street_light_server.gateway_id_prefix:%s\n"	, config.street_light_gateway.gateway_id_prefix);
		APP_DBG("mqtt_street_light_server.gateway_id:%s\n"			, config.street_light_gateway.gateway_id);
		APP_DBG("mqtt_street_light_server.host:%s\n"				, config.street_light_gateway.host);
		APP_DBG("mqtt_street_light_server.port:%d\n"				, config.street_light_gateway.port);
		APP_DBG("mqtt_street_light_server.user_name_view:%s\n"		, config.street_light_gateway.user_name_view);
		APP_DBG("mqtt_street_light_server.user_psk_view:%s\n"		, config.street_light_gateway.user_psk_view);
		APP_DBG("mqtt_street_light_server.user_name_control:%s\n"	, config.street_light_gateway.user_name_control);
		APP_DBG("mqtt_street_light_server.user_psk_control:%s\n"	, config.street_light_gateway.user_psk_control);
	}
		break;

	default:
		break;
	}

	return 0;
}

int32_t i_shell_dbg(uint8_t* argv) {
	switch (*(argv + 4)) {

	default:
		break;
	}
	return 0;
}

int32_t i_shell_pop(uint8_t* argv) {
	switch (*(argv + 4)) {
	case 'i': {
		APP_PRINT("POP INFO request !\n");
		ak_msg_t* s_msg = get_pure_msg();
		set_msg_sig(s_msg, MT_CLOUD_POP_CTRL_GET_INFO_REQ);
		set_msg_src_task_id(s_msg, MT_TASK_CONSOLE_ID);
		task_post(MT_TASK_CLOUD_ID, s_msg);
	}
		break;

	case 'a': {
		APP_PRINT("POP AUTO mode request !\n");
		sl_pop_fan_ctrl_t st_sl_pop_fan_ctrl;
		memset(&st_sl_pop_fan_ctrl, 0, sizeof(sl_pop_fan_ctrl_t));
		st_sl_pop_fan_ctrl.mode = SL_POP_CTRL_MODE_AUTO;
		ak_msg_t* s_msg = get_common_msg();

		set_if_des_type(s_msg, IF_TYPE_CPU_SERIAL_SL);
		set_if_src_type(s_msg, IF_TYPE_CPU_SERIAL_MT);
		set_if_des_task_id(s_msg, SL_TASK_POP_CTRL_ID);
		set_if_src_task_id(s_msg, MT_TASK_CONSOLE_ID);
		set_if_sig(s_msg, SL_POP_CTRL_MODE_SWITCH_REQ);
		set_if_data_common_msg(s_msg, (uint8_t*)&st_sl_pop_fan_ctrl, sizeof(sl_pop_fan_ctrl_t));

		set_msg_sig(s_msg, MT_IF_COMMON_MSG_OUT);
		set_msg_src_task_id(s_msg, MT_TASK_CONSOLE_ID);
		task_post(MT_TASK_IF_ID, s_msg);
	}
		break;

	case 'm': {
		APP_PRINT("POP MANUAL mode request !\n");
		sl_pop_fan_ctrl_t st_sl_pop_fan_ctrl;
		memset(&st_sl_pop_fan_ctrl, 0, sizeof(sl_pop_fan_ctrl_t));
		st_sl_pop_fan_ctrl.mode = SL_POP_CTRL_MODE_MANUAL;
		ak_msg_t* s_msg = get_common_msg();

		set_if_des_type(s_msg, IF_TYPE_CPU_SERIAL_SL);
		set_if_src_type(s_msg, IF_TYPE_CPU_SERIAL_MT);
		set_if_des_task_id(s_msg, SL_TASK_POP_CTRL_ID);
		set_if_src_task_id(s_msg, MT_TASK_CONSOLE_ID);
		set_if_sig(s_msg, SL_POP_CTRL_MODE_SWITCH_REQ);
		set_if_data_common_msg(s_msg, (uint8_t*)&st_sl_pop_fan_ctrl, sizeof(sl_pop_fan_ctrl_t));

		set_msg_sig(s_msg, MT_IF_COMMON_MSG_OUT);
		set_msg_src_task_id(s_msg, MT_TASK_CONSOLE_ID);
		task_post(MT_TASK_IF_ID, s_msg);
	}
		break;

	case 'F': {
		APP_PRINT("POP FANS run request !\n");
		sl_pop_fan_ctrl_t st_sl_pop_fan_ctrl;
		memset(&st_sl_pop_fan_ctrl, 0, sizeof(sl_pop_fan_ctrl_t));
		st_sl_pop_fan_ctrl.mode = SL_POP_CTRL_MODE_MANUAL;
		st_sl_pop_fan_ctrl.power_status = SL_POWER_STATUS_OFF;
		st_sl_pop_fan_ctrl.fan_status[0] = 100;
		st_sl_pop_fan_ctrl.fan_status[1] = 100;
		st_sl_pop_fan_ctrl.fan_status[2] = 100;
		st_sl_pop_fan_ctrl.fan_status[3] = 100;

		ak_msg_t* s_msg = get_common_msg();

		set_if_des_type(s_msg, IF_TYPE_CPU_SERIAL_SL);
		set_if_src_type(s_msg, IF_TYPE_CPU_SERIAL_MT);
		set_if_des_task_id(s_msg, SL_TASK_POP_CTRL_ID);
		set_if_src_task_id(s_msg, MT_TASK_CONSOLE_ID);
		set_if_sig(s_msg, SL_POP_CTRL_CONTROL_REQ);
		set_if_data_common_msg(s_msg, (uint8_t*)&st_sl_pop_fan_ctrl, sizeof(sl_pop_fan_ctrl_t));

		set_msg_sig(s_msg, MT_IF_COMMON_MSG_OUT);
		set_msg_src_task_id(s_msg, MT_TASK_CONSOLE_ID);
		task_post(MT_TASK_IF_ID, s_msg);
	}
		break;

	case 'A': {
		APP_PRINT("POP AIRCOND run request !\n");
		sl_pop_fan_ctrl_t st_sl_pop_fan_ctrl;
		memset(&st_sl_pop_fan_ctrl, 0, sizeof(sl_pop_fan_ctrl_t));
		st_sl_pop_fan_ctrl.mode = SL_POP_CTRL_MODE_MANUAL;
		st_sl_pop_fan_ctrl.power_status = SL_POWER_STATUS_ON;
		st_sl_pop_fan_ctrl.fan_status[0] = 0;
		st_sl_pop_fan_ctrl.fan_status[1] = 0;
		st_sl_pop_fan_ctrl.fan_status[2] = 0;
		st_sl_pop_fan_ctrl.fan_status[3] = 0;

		ak_msg_t* s_msg = get_common_msg();

		set_if_des_type(s_msg, IF_TYPE_CPU_SERIAL_SL);
		set_if_src_type(s_msg, IF_TYPE_CPU_SERIAL_MT);
		set_if_des_task_id(s_msg, SL_TASK_POP_CTRL_ID);
		set_if_src_task_id(s_msg, MT_TASK_CONSOLE_ID);
		set_if_sig(s_msg, SL_POP_CTRL_CONTROL_REQ);
		set_if_data_common_msg(s_msg, (uint8_t*)&st_sl_pop_fan_ctrl, sizeof(sl_pop_fan_ctrl_t));

		set_msg_sig(s_msg, MT_IF_COMMON_MSG_OUT);
		set_msg_src_task_id(s_msg, MT_TASK_CONSOLE_ID);
		task_post(MT_TASK_IF_ID, s_msg);
	}
		break;

	default: {
		APP_PRINT("\"pop a\" -> switch to AUTO mode\n");
		APP_PRINT("\"pop m\" -> switch to MANUAL mode\n");
		APP_PRINT("\"pop F\" -> on FANS and off AIRCOND\n");
		APP_PRINT("\"pop A\" -> off FANS and on AIRCOND\n");
	}
		break;
	}

	return 0;
}

int32_t i_shell_out(uint8_t* argv) {
	switch (*(argv + 4)) {
	case 'i': {
		ak_msg_t* s_msg = get_pure_msg();
		set_msg_sig(s_msg, MT_CLOUD_IO_CTRL_GET_INFO_REQ);
		set_msg_src_task_id(s_msg, MT_TASK_CONSOLE_ID);
		task_post(MT_TASK_CLOUD_ID, s_msg);
	}
		break;

	case 'o': {
		sl_io_ctrl_t st_sl_io_ctrl;
		memset(&st_sl_io_ctrl, 1, sizeof(sl_io_ctrl_t));

		ak_msg_t* s_msg = get_common_msg();
		set_msg_sig(s_msg, MT_CLOUD_IO_CTRL_CONTROL_REQ);
		set_data_common_msg(s_msg, (uint8_t*)&st_sl_io_ctrl, sizeof(sl_io_ctrl_t));
		set_msg_src_task_id(s_msg, MT_TASK_CONSOLE_ID);
		task_post(MT_TASK_CLOUD_ID, s_msg);
	}
		break;

	default:
		break;
	}

	return 0;
}

int32_t i_shell_flood(uint8_t* argv) {
	switch (*(argv + 6)) {
	case '1': {
		lora_message_t flood_data_sensor;
		flood_data_sensor.header.des_addr	=	(uint32_t)inet_addr(gateway_configure_parameter.lora_gateway.lora_host);
		flood_data_sensor.header.scr_addr	=	(uint32_t)inet_addr("1.1.1.12");
		flood_data_sensor.header.type		=	LORA_NODE_REPORT;
		flood_data_sensor.data				=	FLOOD_SENSOR_ACTIVE;

		ak_msg_t* s_msg = get_dymanic_msg();

		set_msg_sig(s_msg, MT_CLOUD_MQTT_FLOOD_SENSOR_REP);
		set_data_dynamic_msg(s_msg, (uint8_t*)&flood_data_sensor, sizeof(lora_message_t));

		set_msg_src_task_id(s_msg, MT_TASK_FLOOD_SYSTEM_ID);
		task_post(MT_TASK_CLOUD_ID, s_msg);
	}
		break;

	case '2': {
		lora_message_t flood_data_sensor;
		flood_data_sensor.header.des_addr	=	(uint32_t)inet_addr(gateway_configure_parameter.lora_gateway.lora_host);
		flood_data_sensor.header.scr_addr	=	(uint32_t)inet_addr("1.1.1.12");
		flood_data_sensor.header.type		=	LORA_NODE_REPORT;
		flood_data_sensor.data				=	FLOOD_SENSOR_INACTIVE;

		ak_msg_t* s_msg = get_dymanic_msg();

		set_msg_sig(s_msg, MT_CLOUD_MQTT_FLOOD_SENSOR_REP);
		set_data_dynamic_msg(s_msg, (uint8_t*)&flood_data_sensor, sizeof(lora_message_t));

		set_msg_src_task_id(s_msg, MT_TASK_FLOOD_SYSTEM_ID);
		task_post(MT_TASK_CLOUD_ID, s_msg);
	}
		break;

	case '3': {
		lora_message_t flood_data_bat;
		flood_data_bat.header.des_addr	=	(uint32_t)inet_addr(gateway_configure_parameter.lora_gateway.lora_host);
		flood_data_bat.header.scr_addr	=	(uint32_t)inet_addr("1.1.1.12");
		flood_data_bat.header.type		=	LORA_NODE_KEEP_ALIVE;
		flood_data_bat.data				=	56;

		ak_msg_t* s_msg = get_dymanic_msg();

		set_msg_sig(s_msg, MT_CLOUD_MQTT_FLOOD_BAT_REP);
		set_data_dynamic_msg(s_msg, (uint8_t*)&flood_data_bat, sizeof(lora_message_t));

		set_msg_src_task_id(s_msg, MT_TASK_FLOOD_SYSTEM_ID);
		task_post(MT_TASK_CLOUD_ID, s_msg);
	}
		break;

	case 'c': {
		ak_msg_t* s_msg = get_dymanic_msg();

		set_msg_sig(s_msg, MT_FLOOD_SYSTEM_CALIBRATE);
		task_post(MT_TASK_FLOOD_SYSTEM_ID, s_msg);
	}

	default:
		break;
	}

	return 0;
}

static int32_t i_shell_lora(uint8_t* argv){
	switch (*(argv + 5)) {
	case 's': {
		APP_PRINT("[i_shell_lora]\n");
		lora_pkg_t test_pkg;
		test_pkg.hdr.app_id = STREET_LIGHT_APP_ID;
		test_pkg.hdr.src_addr = 0x00000002;
		test_pkg.hdr.des_addr = CONCENTRATOR_ADDR;
		test_pkg.hdr.type = SENSOR_TYPE;
		test_pkg.data.sen_data.ntc_sen = 0;
		test_pkg.data.sen_data.curr_sen = 120;

		ak_msg_t* s_msg = get_common_msg();
		set_msg_sig(s_msg, MT_IF_LORA_USB_STICK_MSG_IN);
		set_data_common_msg(s_msg, (uint8_t*)&test_pkg, sizeof(lora_pkg_t));
		task_post(MT_TASK_IF_LORA_USB_STICK_ID, s_msg);

	}
		break;
	}
}
