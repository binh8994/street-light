#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/signal.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include <semaphore.h>
#include <list>

#include "../ak/ak.h"

#include "../sys/sys_dbg.h"

#include "app.h"
#include "app_if.h"
#include "app_dbg.h"
#include "app_data.h"

#include "task_list.h"
#include "task_list_if.h"
#include "if_lora_usb_stick.h"

#define IPCPU_SOP_CHAR												(uint8_t)0xEF
#define IFCPU_DEVPATH												"/dev/ttyUSB0"
#define IFCPU_DATA_SIZE												256

#define RX_BUFFER_SIZE												256

typedef struct {
	uint8_t frame_sop;
	uint8_t len;
	lora_pkg_t lora_pkg;
	uint8_t frame_fcs;
} __attribute__((__packed__)) lora_usb_stick_msg_frame_t;

static int lora_usb_fd;
static uint8_t data_index = 0;

static register_lora_usb_stick_t* register_msg_list_usb_stick_head;
static void register_msg_list_usb_stick_init(register_lora_usb_stick_t* node);
static void register_msg_list_usb_stick_insert(register_lora_usb_stick_t* node);
static void register_msg_list_usb_stick_remove(register_lora_usb_stick_t* node);
static void send_msg_to_task_registered(lora_pkg_t* pkg);

static int lora_usb_opentty(const char* devpath);
static pthread_t lora_usb_rx_thread;
static void* lora_usb_rx_thread_handler(void*);
static uint8_t lora_usb_calcfcs(uint8_t len, uint8_t *data_ptr);

static uint8_t system_packaged_serial_msg(ak_msg_t* ak_msg);
static void rx_frame_parser(uint8_t* data, uint8_t len);

#define SOP_STATE		0x00
#define LEN_STATE		0x01
#define DATA_STATE		0x02
#define FCS_STATE		0x03
static uint8_t rx_frame_state = SOP_STATE;

static lora_usb_stick_msg_frame_t rev_usb_stick_msg_frame_mng;

q_msg_t mt_task_if_lora_usb_stick_mailbox;

void* mt_task_if_lora_usb_stick_entry(void*) {
	task_mask_started();
	wait_all_tasks_started();

	APP_DBG("[STARTED] mt_task_if_lora_usb_stick_entry\n");

	if (lora_usb_opentty(IFCPU_DEVPATH) < 0) {
		APP_DBG("Cannot open %s !\n", IFCPU_DEVPATH);
	}
	else {
		APP_DBG("Opened %s success !\n", IFCPU_DEVPATH);	\
		pthread_create(&lora_usb_rx_thread, NULL, lora_usb_rx_thread_handler, NULL);
	}

	register_msg_list_usb_stick_init(register_msg_list_usb_stick_head);

	while (1) {
		while (msg_available(MT_TASK_IF_LORA_USB_STICK_ID)) {
			ak_msg_t* msg = rev_msg(MT_TASK_IF_LORA_USB_STICK_ID);

			switch (msg->header->sig) {
			case MT_IF_LORA_USB_STICK_MSG_OUT :{
				APP_DBG("MT_IF_LORA_USB_STICK_MSG_OUT\n");

				//				lora_pkg_t r_pkg;
				//				get_data_common_msg(msg,(uint8_t*)&r_pkg,sizeof(lora_pkg_t));

				//				APP_DBG("[hdr.app_id] 0x%X\n", r_pkg.hdr.app_id);
				//				APP_DBG("[hdr.src_addr] 0x%X\n", r_pkg.hdr.src_addr);
				//				APP_DBG("[hdr.des_addr] 0x%X\n", r_pkg.hdr.des_addr);
				//				APP_DBG("[hdr.type] %d\n", r_pkg.hdr.type);
				//				APP_DBG("[data.ctr_data.mod] %d\n", r_pkg.data.ctr_data.mod);
				//				if(r_pkg.data.ctr_data.mod == AUTO_MOD){
				//					APP_DBG("[data.ctr_data.mod_data.time_set.start_hour] %d\n", r_pkg.data.ctr_data.mod_data.time_set.start_hour);
				//					APP_DBG("[data.ctr_data.mod_data.time_set.start_min] %d\n", r_pkg.data.ctr_data.mod_data.time_set.start_min);
				//					APP_DBG("[data.ctr_data.mod_data.time_set.end_hour] %d\n", r_pkg.data.ctr_data.mod_data.time_set.end_hour);
				//					APP_DBG("[data.ctr_data.mod_data.time_set.end_min] %d\n", r_pkg.data.ctr_data.mod_data.time_set.end_min);
				//				}
				//				else{
				//					APP_DBG("[data.ctr_data.mod_data.power] %d\n", r_pkg.data.ctr_data.mod_data.power);
				//				}

				system_packaged_serial_msg(msg);
			}
				break;
			case MT_IF_LORA_USB_STICK_MSG_IN :{
				APP_DBG("MT_IF_LORA_USB_STICK_MSG_IN\n");
				lora_pkg_t r_pkg;
				get_data_common_msg(msg,(uint8_t*)&r_pkg,sizeof(lora_pkg_t));

				send_msg_to_task_registered(&r_pkg);
			}
				break;

			case MT_IF_LORA_USB_STICK_REGIS:{
				APP_DBG("MT_IF_LORA_USB_STICK_REGIS\n");

				register_lora_usb_stick_t regis;
				get_data_common_msg(msg,(uint8_t*)&regis,sizeof(register_lora_usb_stick_t));
				register_msg_list_usb_stick_insert(&regis);
			}
				break;

			case MT_IF_LORA_USB_STICK_UNREGIS:{
				APP_DBG("MT_IF_LORA_USB_STICK_UNREGIS\n");

				register_lora_usb_stick_t regis;
				get_data_common_msg(msg,(uint8_t*)&regis,sizeof(register_lora_usb_stick_t));
				register_msg_list_usb_stick_remove(&regis);
			}
				break;

			default:
				break;
			}

			/* free message */
			free_msg(msg);
		}

		usleep(1000);
	}

	return (void*)0;
}

void* lora_usb_rx_thread_handler(void*) {
	APP_DBG("lora_usb_rx_thread_handler entry successed!\n");

	uint8_t rx_buffer[RX_BUFFER_SIZE];
	uint8_t rx_read_len;

	while(1) {
		rx_read_len = read(lora_usb_fd, rx_buffer, RX_BUFFER_SIZE);
		if (rx_read_len > 0) {
			rx_frame_parser(rx_buffer, rx_read_len);
		}
		usleep(100);
	}

	return (void*)0;
}

int lora_usb_opentty(const char* devpath) {
	struct termios options;
	APP_DBG("[MT_TASK_IF_LORA_USB_STICK_ID][lora_usb_opentty] devpath: %s\n", devpath);

	lora_usb_fd = open(devpath, O_RDWR | O_NOCTTY | O_NDELAY);
	if (lora_usb_fd < 0) {
		return lora_usb_fd;
	}
	else {
		fcntl(lora_usb_fd, F_SETFL, 0);

		/* get current status */
		tcgetattr(lora_usb_fd, &options);

		cfsetispeed(&options, B115200);
		cfsetospeed(&options, B115200);

		/* No parity (8N1) */
		options.c_cflag &= ~PARENB;
		options.c_cflag &= ~CSTOPB;
		options.c_cflag &= ~CSIZE;
		options.c_cflag |= CS8;

		options.c_cflag |= (CLOCAL | CREAD);
		options.c_cflag     &=  ~CRTSCTS;

		cfmakeraw(&options);

		tcflush(lora_usb_fd, TCIFLUSH);
		if (tcsetattr (lora_usb_fd, TCSANOW, &options) != 0) {
			SYS_DBG("error in tcsetattr()\n");
		}
	}
	return 0;
}

/* Calculate lora_usb frame FCS */
uint8_t lora_usb_calcfcs(uint8_t len, uint8_t *data_ptr) {
	uint8_t xor_result;
	xor_result = 0;

	for (int i = 0; i < len; i++, data_ptr++) {
		xor_result = xor_result ^ *data_ptr;
	}

	return xor_result;
}

void rx_frame_parser(uint8_t* data, uint8_t len) {
	uint8_t ch;
	int rx_remain;

	while(len) {

		ch = *data++;
		len--;

		switch (rx_frame_state) {
		case SOP_STATE: {
			if (IPCPU_SOP_CHAR == ch) {
				rx_frame_state = LEN_STATE;
			}
		}
			break;

		case LEN_STATE: {
			if (ch > IFCPU_DATA_SIZE) {
				rx_frame_state = SOP_STATE;
				return;
			}
			else {
				rev_usb_stick_msg_frame_mng.len = ch;
				data_index = 0;
				rx_frame_state = DATA_STATE;
			}
		}
			break;

		case DATA_STATE: {
			*( (uint8_t*)&rev_usb_stick_msg_frame_mng.lora_pkg + data_index++ ) = ch;

//			rx_remain = rev_usb_stick_msg_frame_mng.len - data_index;

//			if (len >= rx_remain) {
//				memcpy(((uint8_t*)&rev_usb_stick_msg_frame_mng.lora_pkg + data_index), data, rx_remain);
//				data_index += rx_remain;
//				len -= rx_remain;
//				data += rx_remain;
//			}
//			else {
//				memcpy(((uint8_t*)&rev_usb_stick_msg_frame_mng.lora_pkg + data_index), data, len);
//				data_index += len;
//				len = 0;
//				data += len;
//			}

			if (data_index == rev_usb_stick_msg_frame_mng.len) {
				rx_frame_state = FCS_STATE;
			}
		}
			break;

		case FCS_STATE: {
			rx_frame_state = SOP_STATE;

//			APP_DBG("len: %d\n", rev_usb_stick_msg_frame_mng.len);
//			for(int i=0; i< rev_usb_stick_msg_frame_mng.len; i++){
//				APP_DBG("0x%X\n", *((uint8_t*)&rev_usb_stick_msg_frame_mng.lora_pkg + i));
//			}


			rev_usb_stick_msg_frame_mng.frame_fcs = ch;

			if (rev_usb_stick_msg_frame_mng.frame_fcs \
					== lora_usb_calcfcs(rev_usb_stick_msg_frame_mng.len,\
										(uint8_t*)&rev_usb_stick_msg_frame_mng.lora_pkg)) {

//				APP_DBG("app id: 0x%X\n", rev_usb_stick_msg_frame_mng.lora_pkg.hdr.app_id);
//				APP_DBG("src addr: 0x%X\n", rev_usb_stick_msg_frame_mng.lora_pkg.hdr.src_addr);
//				APP_DBG("des addr: 0x%X\n", rev_usb_stick_msg_frame_mng.lora_pkg.hdr.des_addr);
//				APP_DBG("msg type: 0x%X\n", rev_usb_stick_msg_frame_mng.lora_pkg.hdr.type);
				//APP_DBG("current: %d\n", rev_usb_stick_msg_frame_mng.lora_pkg.data.sen_data.curr_sen);
				//APP_DBG("ntc: %d\n", rev_usb_stick_msg_frame_mng.lora_pkg.data.sen_data.ntc_sen);

				ak_msg_t* s_msg = get_common_msg();
				set_msg_sig(s_msg, MT_IF_LORA_USB_STICK_MSG_IN);
				set_data_common_msg(s_msg, (uint8_t*)&rev_usb_stick_msg_frame_mng.lora_pkg, sizeof(lora_pkg_t));
				task_post(MT_TASK_IF_LORA_USB_STICK_ID, s_msg);

			}
			else {
				/* TODO: handle checksum incorrect */
			}
		}
			break;

		default:
			break;
		}
	}

}


uint8_t system_packaged_serial_msg(ak_msg_t* ak_msg) {
	lora_usb_stick_msg_frame_t send_usb_stick_msg_frame_mng;
	uint8_t len = get_data_len_common_msg(ak_msg);

	send_usb_stick_msg_frame_mng.frame_sop = IPCPU_SOP_CHAR;
	send_usb_stick_msg_frame_mng.len = len;
	get_data_common_msg(ak_msg, (uint8_t*)&send_usb_stick_msg_frame_mng.lora_pkg, sizeof(lora_pkg_t));
	send_usb_stick_msg_frame_mng.frame_fcs = lora_usb_calcfcs(len, (uint8_t*)&send_usb_stick_msg_frame_mng.lora_pkg);

//	for(uint8_t i=0; i<sizeof(lora_usb_stick_msg_frame_t); i++){
//		APP_DBG("0x%X\n", (uint8_t)*((uint8_t*)&send_usb_stick_msg_frame_mng + i));
//	}

	write(lora_usb_fd,(uint8_t*)&send_usb_stick_msg_frame_mng, sizeof(lora_usb_stick_msg_frame_t));

	return 0;
}

void register_msg_list_usb_stick_init(register_lora_usb_stick_t* node){
	node = NULL;
}

void register_msg_list_usb_stick_insert(register_lora_usb_stick_t* node){
	register_lora_usb_stick_t* temp = register_msg_list_usb_stick_head;
	register_lora_usb_stick_t* regis_new;

	/* check regis exist */
	while(temp != NULL){
		if(temp->register_msg.task_id == node->register_msg.task_id && \
				temp->register_msg.sig == node->register_msg.sig){
			//APP_DBG("[MT_TASK_IF_LORA_USB_STICK_ID][register_msg_insert] register exist\n");
			return ;
		}
		temp = temp->next;
	}

	regis_new = (register_lora_usb_stick_t*)malloc(sizeof(register_lora_usb_stick_t));
	regis_new->prev = NULL;
	regis_new->next = NULL;

	if(regis_new == NULL){
		FATAL("REGIS", 0x02);
	}

	memcpy(regis_new, node, sizeof(register_lora_usb_stick_t));

	if(register_msg_list_usb_stick_head == NULL){
		register_msg_list_usb_stick_head = regis_new;
		//APP_DBG("[MT_TASK_IF_LORA_USB_STICK_ID][register_msg_insert] OK\n");
		return;
	}

	register_msg_list_usb_stick_head->prev = regis_new;
	regis_new->next = register_msg_list_usb_stick_head;
	register_msg_list_usb_stick_head = regis_new;
	//APP_DBG("[MT_TASK_IF_LORA_USB_STICK_ID][register_msg_insert] OK\n");
}


void register_msg_list_usb_stick_remove(register_lora_usb_stick_t* node){
	register_lora_usb_stick_t* temp = register_msg_list_usb_stick_head;

	while(temp != NULL){
		if(temp->register_msg.task_id == node->register_msg.task_id && \
				temp->register_msg.sig == node->register_msg.sig){
			register_lora_usb_stick_t* before = temp->prev;

			before->next = temp->next;
			temp->next->prev = before;

			free(temp);
			//APP_DBG("[MT_TASK_IF_LORA_USB_STICK_ID][register_msg_remove] OK\n");
			return;
		}
		temp = temp->next;
	}
}

void send_msg_to_task_registered(lora_pkg_t* pkg){
	//APP_DBG("[MT_TASK_IF_LORA_USB_STICK_ID][send_msg_to_task_registered]\n");
	register_lora_usb_stick_t* temp = register_msg_list_usb_stick_head;
	while(temp != NULL){

		if(temp->register_msg.app_id == pkg->hdr.app_id){
			APP_DBG("SEND TO TASK ID: 0x%X\n", temp->register_msg.task_id);
			if (temp->register_msg.type_if == IF_TYPE_APP_MT) {
				ak_msg_t* s_msg = get_common_msg();
				set_msg_sig(s_msg, temp->register_msg.sig);
				set_data_common_msg(s_msg, (uint8_t*)pkg, sizeof(lora_pkg_t));
				task_post(temp->register_msg.task_id, s_msg);
			}
			else {
				ak_msg_t* s_msg = get_common_msg();

				set_if_src_task_id(s_msg, MT_TASK_IF_LORA_USB_STICK_ID);
				set_if_src_task_id(s_msg, temp->register_msg.task_id);
				set_if_des_type(s_msg, temp->register_msg.type_if);
				set_if_src_type(s_msg, IF_TYPE_APP_MT);
				set_if_sig(s_msg, temp->register_msg.sig);
				set_if_data_common_msg(s_msg, (uint8_t*)pkg, sizeof(lora_pkg_t));

				set_msg_sig(s_msg, MT_IF_COMMON_MSG_OUT);
				set_msg_src_task_id(s_msg, MT_TASK_IF_LORA_USB_STICK_ID);
				task_post(MT_TASK_IF_ID, s_msg);
			}
		}
			temp = temp->next;
	}
}
