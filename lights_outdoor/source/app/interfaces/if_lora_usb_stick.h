#ifndef __IF_LORA_USB_STICK_H__
#define __IF_LORA_USB_STICK_H__

#include <stdint.h>

#include "../ak/message.h"

typedef struct {
	uint32_t app_id;
	uint32_t type_if;
	uint32_t task_id;
	uint32_t sig;
} register_msg_t;

struct register_lora_usb_stick_t {
	register_msg_t register_msg;

	register_lora_usb_stick_t* next;
	register_lora_usb_stick_t* prev;
};

extern q_msg_t mt_task_if_lora_usb_stick_mailbox;
extern void* mt_task_if_lora_usb_stick_entry(void*);

#endif //__IF_LORA_USB_STICK_H__
