#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <ctime>
#include <string>
#include <curl/curl.h>

#include "../ak/ak.h"
#include "../ak/timer.h"

#include "../common/json.hpp"

#include "app.h"
#include "app_if.h"
#include "app_dbg.h"
#include "app_data.h"

#include "mqtt_sl_sensor.h"
#include "mqtt_flood_sensor.h"
#include "mqtt_temperature_cont.h"
#include "mqtt_street_light_ctr.h"
#include "mqtt_street_light_stt.h"

#include "task_list.h"
#include "task_list_if.h"
#include "task_cloud.h"

using namespace std;
using json = nlohmann::json;

q_msg_t mt_task_cloud_mailbox;

class curl_c {
public:
	curl_c() : curl(curl_easy_init()), http_code(0) {}

	~curl_c() {
		if (curl) {
			curl_easy_cleanup(curl);
		}
	}

	string get(const string& url) {
		CURLcode res;
		curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, this);

		ss.str("");
		http_code = 0;

		res = curl_easy_perform(curl);
		if (res != CURLE_OK) {
			throw runtime_error(curl_easy_strerror(res));
		}

		curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &http_code);

		return ss.str();
	}

	long get_http_code() {
		return http_code;
	}

private:
	static size_t write_data(void *buffer, size_t size, size_t nmemb, void *userp) {
		return static_cast<curl_c*>(userp)->Write(buffer,size,nmemb);
	}

	size_t Write(void *buffer, size_t size, size_t nmemb) {
		ss.write((const char*)buffer,size*nmemb);
		return size*nmemb;
	}

	CURL* curl;
	stringstream ss;
	long http_code;
};


static void cloud_sensors_packet_report();

void* mt_task_cloud_entry(void*) {
	task_mask_started();
	wait_all_tasks_started();

	APP_DBG("[STARTED] mt_task_cloud_entry\n");

	/*
	 * init mqtt topic for FPT POP OUTDOOR
	 */
	/* start timer to publish sensor data to server  */
	//	string sl_sensors_topic = string("$SYS/gateways/") + string(gateway_configure_parameter.pop_gateway.gateway_id) + string("/sensors/");
	//	string flood_sensors_topic = string("$SYS/fwaters/") + string(gateway_configure_parameter.pop_gateway.gateway_id) + string("/nodes/");
	//	string temperature_control_topic = string("$SYS/gateways/") + string(gateway_configure_parameter.pop_gateway.gateway_id) + string("/temperature_control/");

	//	APP_DBG("sl_sensors_topic:%s\n", sl_sensors_topic.data());
	//	APP_DBG("flood_sensors_topic:%s\n", flood_sensors_topic.data());
	//	APP_DBG("temperature_control_topic:%s\n", temperature_control_topic.data());

	//	string clien_id_sl_sensor = string(gateway_configure_parameter.pop_gateway.gateway_id_prefix) + string(gateway_configure_parameter.pop_gateway.gateway_id) + string("-sl_sensor");
	//	APP_DBG("clien_id_sl_sensor:%s\n", clien_id_sl_sensor.data());

	//	string clien_id_flood_sensor = string(gateway_configure_parameter.pop_gateway.gateway_id_prefix) + string(gateway_configure_parameter.pop_gateway.gateway_id) + string("-flood_sensor");
	//	APP_DBG("clien_id_flood_sensor:%s\n", clien_id_flood_sensor.data());

	//	string clien_id_control = string(gateway_configure_parameter.pop_gateway.gateway_id_prefix) + string(gateway_configure_parameter.pop_gateway.gateway_id) + string("-temperature_control");
	//	APP_DBG("clien_id_control:%s\n", clien_id_control.data());


	//	/* config mqtt */
	//	mqtt_sl_sensor*	mqtt_slsensor	= new mqtt_sl_sensor(clien_id_sl_sensor.data(), gateway_configure_parameter.pop_gateway.host, gateway_configure_parameter.pop_gateway.port);
	//	mqtt_slsensor->set_topic(sl_sensors_topic.data());

	//	mqtt_flood_sensor*	mqtt_floodsensor	= new mqtt_flood_sensor(clien_id_flood_sensor.data(), gateway_configure_parameter.lora_gateway.mqtt_host, gateway_configure_parameter.lora_gateway.mqtt_port);
	//	mqtt_floodsensor->set_topic(flood_sensors_topic.data());

	//	mqtt_temperature_cont*	mqtt_temperature_control	= new mqtt_temperature_cont("iot-thannt", gateway_configure_parameter.pop_gateway.host, gateway_configure_parameter.pop_gateway.port);
	//	mqtt_temperature_control->set_topic(temperature_control_topic.data());

	/*
	 * init mqtt topic for STREET LIGHT
	 */
	string street_light_status_topic = string("iot/flights/data/demo/");
	string street_light_control_topic = string("iot/flights/ctrl/demo/");

	APP_DBG("street_light_status_topic:%s\n", street_light_status_topic.data());
	APP_DBG("street_light_control_topic:%s\n", street_light_control_topic.data());

	string clien_id_street_light_status = string(gateway_configure_parameter.street_light_gateway.gateway_id_prefix) + string(gateway_configure_parameter.street_light_gateway.gateway_id) + string("-status");
	APP_DBG("clien_id_street_light_status:%s\n", clien_id_street_light_status.data());

	string clien_id_street_light_control = string(gateway_configure_parameter.street_light_gateway.gateway_id_prefix) + string(gateway_configure_parameter.street_light_gateway.gateway_id) + string("-control");
	APP_DBG("clien_id_street_light_control:%s\n", clien_id_street_light_control.data());


	/* config mqtt */
	mqtt_street_light_stt*	mqtt_street_light_status	= new mqtt_street_light_stt(clien_id_street_light_status.data(), gateway_configure_parameter.street_light_gateway.host, gateway_configure_parameter.street_light_gateway.port);
	mqtt_street_light_status->set_topic(street_light_status_topic.data());

	mqtt_street_light_ctr*	mqtt_street_light_control	= new mqtt_street_light_ctr(clien_id_street_light_control.data(), gateway_configure_parameter.street_light_gateway.host, gateway_configure_parameter.street_light_gateway.port);
	mqtt_street_light_control->set_topic(street_light_control_topic.data());

	timer_set(MT_TASK_CLOUD_ID, MT_CLOUD_MQTT_SENT_TEST, 3000, TIMER_PERIODIC);

	while (1) {
		while (msg_available(MT_TASK_CLOUD_ID)) {
			/* get messge */
			ak_msg_t* msg = rev_msg(MT_TASK_CLOUD_ID);

			switch (msg->header->sig) {
			case MT_CLOUD_SL_SYNC_OK: {
				APP_DBG("MT_CLOUD_SL_SYNC_OK\n");
			}
				break;

			case MT_CLOUD_SL_SYNC_ERR: {
				APP_DBG("MT_CLOUD_SL_SYNC_ERR\n");
			}
				break;
#if 0
			case MT_CLOUD_SL_SENSOR_REPORT_REP: {
				APP_DBG("MT_CLOUD_SL_SENSOR_REPORT_REP\n");
				sl_sensors_t st_sl_sensors;
				get_data_common_msg(msg, (uint8_t*)&st_sl_sensors, sizeof(sl_sensors_t));

				json js_sl_sensor = sl_sensors_struct_to_json(&st_sl_sensors);
				string str_sl_sensor = js_sl_sensor.dump();
				mqtt_slsensor->sensor_public((uint8_t*)str_sl_sensor.c_str(), str_sl_sensor.length());
			}
				break;


			case MT_CLOUD_POP_CTRL_MODE_SWITCH_RES_OK: {
				APP_DBG("MT_CLOUD_POP_CTRL_MODE_SWITCH_RES_OK\n");
				cloud_sensors_packet_report();
			}
				break;

			case MT_CLOUD_POP_CTRL_MODE_SWITCH_RES_ERR: {
				APP_DBG("MT_CLOUD_POP_CTRL_MODE_SWITCH_RES_ERR\n");
				uint8_t error_code;
				get_data_common_msg(msg, &error_code, sizeof(uint8_t));
				APP_DBG("error_code: %d\n", error_code);
				cloud_sensors_packet_report();
			}
				break;

			case MT_CLOUD_POP_CTRL_CONTROL_RES_OK: {
				APP_DBG("MT_CLOUD_POP_CTRL_CONTROL_RES_OK\n");
				cloud_sensors_packet_report();
			}
				break;

			case MT_CLOUD_POP_CTRL_CONTROL_RES_ERR: {
				APP_DBG("MT_CLOUD_POP_CTRL_CONTROL_RES_ERR\n");
				uint8_t error_code;
				get_data_common_msg(msg, &error_code, sizeof(uint8_t));
				APP_DBG("error_code: %d\n", error_code);
				cloud_sensors_packet_report();
			}
				break;

			case MT_CLOUD_POP_CTRL_GET_INFO_REQ: {
				APP_DBG("MT_CLOUD_POP_CTRL_GET_INFO_REQ\n");
				uint8_t ret_sig = MT_CLOUD_POP_CTRL_GET_INFO_RES;
				ak_msg_t* s_msg = get_common_msg();

				set_if_des_type(s_msg, IF_TYPE_CPU_SERIAL_SL);
				set_if_src_type(s_msg, IF_TYPE_CPU_SERIAL_MT);
				set_if_des_task_id(s_msg, SL_TASK_POP_CTRL_ID);
				set_if_src_task_id(s_msg, MT_TASK_CLOUD_ID);
				set_if_sig(s_msg, SL_POP_CTRL_CONTROL_INFO_REQ);
				set_if_data_common_msg(s_msg, (uint8_t*)&ret_sig, sizeof(uint8_t));

				set_msg_sig(s_msg, MT_IF_COMMON_MSG_OUT);
				set_msg_src_task_id(s_msg, MT_TASK_CLOUD_ID);
				task_post(MT_TASK_IF_ID, s_msg);
			}
				break;

			case MT_CLOUD_POP_CTRL_GET_INFO_RES: {
				APP_DBG("MT_CLOUD_POP_CTRL_GET_INFO_RES\n");
				sl_pop_fan_ctrl_t st_sl_pop_fan_ctrl;
				get_data_common_msg(msg, (uint8_t*)&st_sl_pop_fan_ctrl, sizeof(sl_pop_fan_ctrl_t));
				APP_DBG("mode :%d\n", st_sl_pop_fan_ctrl.mode);
				APP_DBG("power_status :%d\n", st_sl_pop_fan_ctrl.power_status);
				APP_DBG("fan_status[0] :%d\n", st_sl_pop_fan_ctrl.fan_status[0]);
				APP_DBG("fan_status[1] :%d\n", st_sl_pop_fan_ctrl.fan_status[1]);
				APP_DBG("fan_status[2] :%d\n", st_sl_pop_fan_ctrl.fan_status[2]);
				APP_DBG("fan_status[3] :%d\n", st_sl_pop_fan_ctrl.fan_status[3]);
			}
				break;

			case MT_CLOUD_IO_CTRL_GET_INFO_REQ: {
				APP_DBG("MT_CLOUD_IO_CTRL_GET_INFO_REQ\n");
				uint8_t ret_sig = MT_CLOUD_IO_CTRL_GET_INFO_RES;
				ak_msg_t* s_msg = get_common_msg();

				set_if_des_type(s_msg, IF_TYPE_CPU_SERIAL_SL);
				set_if_src_type(s_msg, IF_TYPE_CPU_SERIAL_MT);
				set_if_des_task_id(s_msg, SL_TASK_IO_CTRL_ID);
				set_if_src_task_id(s_msg, MT_TASK_CLOUD_ID);
				set_if_sig(s_msg, SL_IO_CTRL_CONTROL_INFO_REQ);
				set_if_data_common_msg(s_msg, (uint8_t*)&ret_sig, sizeof(uint8_t));

				set_msg_sig(s_msg, MT_IF_COMMON_MSG_OUT);
				set_msg_src_task_id(s_msg, MT_TASK_CLOUD_ID);
				task_post(MT_TASK_IF_ID, s_msg);
			}
				break;

			case MT_CLOUD_IO_CTRL_GET_INFO_RES: {
				APP_DBG("MT_CLOUD_IO_CTRL_GET_INFO_RES\n");
				sl_io_ctrl_t st_sl_io_ctrl;
				get_data_common_msg(msg, (uint8_t*)&st_sl_io_ctrl, sizeof(sl_io_ctrl_t));
				for (uint32_t i = 0; i < SL_TOTAL_GENERAL_OUTPUT; i++) {
					APP_DBG("general_output[%d]: %d\n", i, st_sl_io_ctrl.general_output[i]);
				}
			}
				break;

			case MT_CLOUD_IO_CTRL_CONTROL_REQ: {
				APP_DBG("MT_CLOUD_IO_CTRL_CONTROL_REQ\n");
				sl_io_ctrl_t st_sl_io_ctrl;
				get_data_common_msg(msg, (uint8_t*)&st_sl_io_ctrl, sizeof(sl_io_ctrl_t));

				ak_msg_t* s_msg = get_common_msg();

				set_if_des_type(s_msg, IF_TYPE_CPU_SERIAL_SL);
				set_if_src_type(s_msg, IF_TYPE_CPU_SERIAL_MT);
				set_if_des_task_id(s_msg, SL_TASK_IO_CTRL_ID);
				set_if_src_task_id(s_msg, MT_TASK_CLOUD_ID);
				set_if_sig(s_msg, SL_IO_CTRL_CONTROL_REQ);
				set_if_data_common_msg(s_msg, (uint8_t*)&st_sl_io_ctrl, sizeof(sl_io_ctrl_t));

				set_msg_sig(s_msg, MT_IF_COMMON_MSG_OUT);
				set_msg_src_task_id(s_msg, MT_TASK_CLOUD_ID);
				task_post(MT_TASK_IF_ID, s_msg);
			}
				break;

			case MT_CLOUD_MQTT_FLOOD_SENSOR_REP: {
				APP_DBG("MT_CLOUD_MQTT_FLOOD_SENSOR_REP\n");
				lora_message_t st_lora_message;
				get_data_dynamic_msg(msg, (uint8_t*)&st_lora_message, sizeof(lora_message_t));

				json js_flood_sensor = flood_sensors_struct_to_json(&st_lora_message);
				string str_flood_sensor = js_flood_sensor.dump();
				mqtt_floodsensor->sensor_public((uint8_t*)str_flood_sensor.c_str(), str_flood_sensor.length());
			}
				break;

			case MT_CLOUD_MQTT_FLOOD_BAT_REP: {
				APP_DBG("MT_CLOUD_MQTT_FLOOD_BAT_REP\n");
				lora_message_t st_lora_message;
				get_data_dynamic_msg(msg, (uint8_t*)&st_lora_message, sizeof(lora_message_t));

				json js_flood_sensor = flood_sensors_struct_to_json(&st_lora_message);
				string str_flood_sensor = js_flood_sensor.dump();
				mqtt_floodsensor->sensor_public((uint8_t*)str_flood_sensor.c_str(), str_flood_sensor.length());
			}
				break;
#endif
			case MT_CLOUD_MQTT_STREET_LIGHT_UP: {
				APP_DBG("MT_CLOUD_MQTT_STREET_LIGHT_UP\n");
				street_light_status_t stt;
				get_data_dynamic_msg(msg, (uint8_t*)&stt, sizeof(street_light_status_t));

				json js_status = street_light_status_struct_to_json(&stt);
				string str_status = js_status.dump();
				mqtt_street_light_status->status_public((uint8_t*)str_status.c_str(), str_status.length());
			}
				break;

			default:
				break;
			}

			/* free message */
			free_msg(msg);
		}

		usleep(100);
	}

	return (void*)0;
}

void cloud_sensors_packet_report() {
	ak_msg_t* s_msg = get_pure_msg();
	set_msg_sig(s_msg, MT_CLOUD_POP_CTRL_GET_INFO_REQ);
	set_msg_src_task_id(s_msg, MT_TASK_CLOUD_ID);
	task_post(MT_TASK_CLOUD_ID, s_msg);
}
