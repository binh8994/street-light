#ifndef __TASK_STREET_LIGHT_SYSTEM_H__
#define __TASK_STREET_LIGHT_SYSTEM_H__

#include "../ak/message.h"

typedef struct {
	lora_pkg_t storage_pkg;
	uint32_t count;
} ctr_storage_pkg_t;

extern q_msg_t mt_task_street_light_system_mailbox;
extern void* mt_task_street_light_system_entry(void*);

#endif //__TASK_STREET_LIGHT_SYSTEM_H__
