#include <sys/socket.h>
#include <sys/stat.h>

#include <time.h>
#include <ctime>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>

#include <vector>
#include <iostream>
#include <fstream>
#include <iostream>

#include "../ak/ak.h"

#include "../sys/sys_dbg.h"

#include "app.h"
#include "app_if.h"
#include "app_dbg.h"
#include "app_data.h"

#include "task_list.h"
#include "task_list_if.h"
#include "task_street_light_system.h"

static uint32_t timestamp;

static string street_light_system_folder_path;
static string street_light_system_devs_file_path;
static int street_light_system_file_obj;
static ctr_storage_pkg_t ctr_storage_pkg;

static vector<uint8_t> time_setup_vt;
static void get_time_setup(vector<uint8_t>&);
static void set_time_setup(vector<uint8_t>&);
static void show_time_setup(vector<uint8_t>&);

static void regis_lora_usb_stick(void);

q_msg_t mt_task_street_light_system_mailbox;

void *mt_task_street_light_system_entry(void *) {

	street_light_system_folder_path		= string(APP_ROOT_PATH_DISK) + string("/street_light_system");
	street_light_system_devs_file_path		= street_light_system_folder_path + string("/street_light_setup_time.json");

	/* create folder path */
	struct stat st = {0};
	if (stat(street_light_system_folder_path.data(), &st) == -1) {
		mkdir(street_light_system_folder_path.data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	}


	//get_time_setup(time_setup_vt);
	//show_time_setup(time_setup_vt);

	task_mask_started();
	wait_all_tasks_started();
	APP_DBG("[STARTED] mt_task_street_light_system_entry\n");

	/* regis recv msg lora usb stick	*/
	regis_lora_usb_stick();

	/* timeout sync gateway and switch board */
	timer_set(MT_TASK_STREET_LIGHT_SYSTEM_ID, MT_STREET_LIGHT_SYSTEM_SYNC_TIME_TIMEOUT, MT_STREET_LIGHT_SYSTEM_TIMEOUT_SYNC_INTERVAL, TIMER_PERIODIC);

	while(1) {
		while (msg_available(MT_TASK_STREET_LIGHT_SYSTEM_ID)) {
			ak_msg_t* msg = rev_msg(MT_TASK_STREET_LIGHT_SYSTEM_ID);

			switch (msg->header->sig) {
			case MT_STREET_LIGHT_SYSTEM_SYNC_TIME_TIMEOUT:{
				APP_DBG("MT_STREET_LIGHT_SYSTEM_SYNC_TIME_TIMEOUT\n");
				APP_DBG("[MT_STREET_LIGHT_SYSTEM_SYNC_TIME_TIMEOUT][SWITCH DIE]\n");
			}
				break;

			case MT_STREET_LIGHT_SYSTEM_CONTROL_REQ_TIMEOUT:{
				APP_DBG("MT_STREET_LIGHT_SYSTEM_CONTROL_REQ_TIMEOUT\n");

				if(ctr_storage_pkg.count > 3){
					timer_remove_attr(MT_TASK_STREET_LIGHT_SYSTEM_ID, MT_STREET_LIGHT_SYSTEM_CONTROL_REQ_TIMEOUT);
					APP_DBG("[MT_STREET_LIGHT_SYSTEM_CONTROL_REQ_TIMEOUT][SWITCH DIE]\n");
				}
				else {
					APP_DBG("[MT_STREET_LIGHT_SYSTEM_CONTROL_REQ_TIMEOUT][SEND AGAIN]\n");
					ak_msg_t* s_msg = get_common_msg();
					set_msg_sig(s_msg, MT_IF_LORA_USB_STICK_MSG_OUT);
					set_data_common_msg(s_msg, (uint8_t*)&ctr_storage_pkg.storage_pkg, sizeof(lora_pkg_t));
					task_post(MT_TASK_IF_LORA_USB_STICK_ID, s_msg);

					ctr_storage_pkg.count++;
				}
			}
				break;

			case MT_STREET_LIGHT_SYSTEM_LORA_MSG_IN: {
				APP_DBG("MT_STREET_LIGHT_SYSTEM_LORA_MSG_IN\n");

				lora_pkg_t r_pkg;
				get_data_common_msg(msg,(uint8_t*)&r_pkg,sizeof(lora_pkg_t));

				APP_DBG("[hdr.app_id] 0x%X\n", r_pkg.hdr.app_id);
				APP_DBG("[hdr.src_addr] 0x%X\n", r_pkg.hdr.src_addr);
				APP_DBG("[hdr.des_addr] 0x%X\n", r_pkg.hdr.des_addr);
				APP_DBG("[hdr.type] %d\n", r_pkg.hdr.type);

				if(r_pkg.hdr.app_id == STREET_LIGHT_APP_ID){
					if(r_pkg.hdr.des_addr == CONCENTRATOR_ADDR) {

						if(r_pkg.hdr.type == SENSOR_TYPE){
							APP_DBG("[MT_STREET_LIGHT_SYSTEM_LORA_MSG_IN][SENSOR_TYPE]\n");

							street_light_status_t stt_msg;
							stt_msg.id = r_pkg.hdr.src_addr;
							stt_msg.current = r_pkg.data.sen_data.curr_sen;
							if( r_pkg.data.sen_data.curr_sen > 100 ){
								stt_msg.status = 1;
							}
							else {
								stt_msg.status = 0;
							}

							ak_msg_t* s_msg = get_dymanic_msg();
							set_msg_sig(s_msg, MT_CLOUD_MQTT_STREET_LIGHT_UP);
							set_data_dynamic_msg(s_msg, (uint8_t*)&stt_msg, sizeof(street_light_status_t));
							set_msg_src_task_id(s_msg, MT_TASK_STREET_LIGHT_SYSTEM_ID);
							task_post(MT_TASK_CLOUD_ID, s_msg);
						}
						else if(r_pkg.hdr.type == SYNC_TYPE){
							/*reset timer*/
							timer_set(MT_TASK_STREET_LIGHT_SYSTEM_ID, MT_STREET_LIGHT_SYSTEM_SYNC_TIME_TIMEOUT, MT_STREET_LIGHT_SYSTEM_TIMEOUT_SYNC_INTERVAL, TIMER_PERIODIC);

							APP_DBG("[MT_STREET_LIGHT_SYSTEM_LORA_MSG_IN][SYNC_TYPE]\n");

							// current date/time based on current system
							time_t now = time(0);
							tm *ltm = localtime(&now);

							uint8_t h_now = ltm->tm_hour;
							uint8_t m_now = ltm->tm_min;

							if(r_pkg.data.sync_data.hour != h_now || \
									(abs(m_now - r_pkg.data.sync_data.min)) > OFFSET_SYNC){

								APP_DBG("[MT_STREET_LIGHT_SYSTEM_LORA_MSG_IN][SETTING_SYNC]\n");

								lora_pkg_t s_pkg;
								s_pkg.hdr.app_id = STREET_LIGHT_APP_ID;
								s_pkg.hdr.src_addr = CONCENTRATOR_ADDR;
								s_pkg.hdr.des_addr = SWITCH_ADDR;
								s_pkg.hdr.type = SYNC_TYPE;
								s_pkg.data.sync_data.hour = h_now;
								s_pkg.data.sync_data.min = m_now;

								ak_msg_t* s_msg = get_common_msg();
								set_msg_sig(s_msg, MT_IF_LORA_USB_STICK_MSG_OUT);
								set_data_common_msg(s_msg, (uint8_t*)&s_pkg, sizeof(lora_pkg_t));
								task_post(MT_TASK_IF_LORA_USB_STICK_ID, s_msg);
							}

						}
						else if(r_pkg.hdr.type = CONTROL_TYPE){
							APP_DBG("[MT_STREET_LIGHT_SYSTEM_LORA_MSG_IN][CONTROL_TYPE]\n");
							timer_remove_attr(MT_TASK_STREET_LIGHT_SYSTEM_ID, MT_STREET_LIGHT_SYSTEM_CONTROL_REQ_TIMEOUT);
							/* recv response from switch*/
						}
					}
				}

			}
				break;

			case MT_STREET_LIGHT_SYSTEM_LORA_MSG_OUT: {
				APP_DBG("MT_STREET_LIGHT_SYSTEM_LORA_MSG_OUT\n");

				street_light_control_t ctr;
				memset(&ctr, 0, sizeof (street_light_control_t));

				get_data_common_msg(msg, (uint8_t*)&ctr, sizeof(street_light_control_t));

				APP_DBG("[ctr.mode] %d\n", ctr.mode);
				APP_DBG("[ctr.power] %d\n", ctr.power);
				APP_DBG("[ctr.start_hour] %d\n", ctr.start_hour);
				APP_DBG("[ctr.start_min] %d\n", ctr.start_min);
				APP_DBG("[ctr.end_hour] %d\n", ctr.end_hour);
				APP_DBG("[ctr.end_min] %d\n", ctr.end_min);
				APP_DBG("[ctr.request] %d\n", ctr.request);

				if(timestamp == ctr.request) break;

				timestamp = ctr.request;

				lora_pkg_t s_pkg;
				s_pkg.hdr.app_id = STREET_LIGHT_APP_ID;
				s_pkg.hdr.src_addr = CONCENTRATOR_ADDR;
				s_pkg.hdr.des_addr = SWITCH_ADDR;
				s_pkg.hdr.type = CONTROL_TYPE;
				s_pkg.data.ctr_data.mod = ctr.mode;
				if( ctr.mode == AUTO_MOD){
					s_pkg.data.ctr_data.mod_data.time_set.start_hour = ctr.start_hour;
					s_pkg.data.ctr_data.mod_data.time_set.start_min = ctr.start_min;
					s_pkg.data.ctr_data.mod_data.time_set.end_hour = ctr.end_hour;
					s_pkg.data.ctr_data.mod_data.time_set.end_min = ctr.end_min;

					time_setup_vt.clear();
					time_setup_vt.push_back(ctr.start_hour);
					time_setup_vt.push_back(ctr.start_min);
					time_setup_vt.push_back(ctr.end_hour);
					time_setup_vt.push_back(ctr.start_min);
					/* write to file */
					//set_time_setup(time_setup_vt);
				}
				else {
					s_pkg.data.ctr_data.mod_data.power = ctr.power;
				}

				ak_msg_t* s_msg = get_common_msg();
				set_msg_sig(s_msg, MT_IF_LORA_USB_STICK_MSG_OUT);
				set_data_common_msg(s_msg, (uint8_t*)&s_pkg, sizeof(lora_pkg_t));
				task_post(MT_TASK_IF_LORA_USB_STICK_ID, s_msg);

				/*store to reser*/
				memcpy(&ctr_storage_pkg.storage_pkg, &s_pkg, sizeof(lora_pkg_t));
				ctr_storage_pkg.count = 1;
				timer_set(MT_TASK_STREET_LIGHT_SYSTEM_ID, MT_STREET_LIGHT_SYSTEM_CONTROL_REQ_TIMEOUT, MT_STREET_LIGHT_SYSTEM_CONTROL_REQ_INTERVAL, TIMER_PERIODIC);
			}
				break;

			default:
				break;
			}
			/* free message */
			free_msg(msg);
		}

	}

	usleep(1000);

	return (void*)0;
}


void set_time_setup(vector<uint8_t>& time_setup) {
	street_light_system_file_obj = open(street_light_system_devs_file_path.c_str(), O_WRONLY | O_CREAT, 0666);

	if (street_light_system_file_obj < 0) {
		APP_DBG("can't open %s file\n", street_light_system_devs_file_path.c_str());
	}
	else {
		APP_DBG("opened %s file\n", street_light_system_devs_file_path.c_str());
		json j_vector(time_setup);
		string str_j_vector = j_vector.dump();
		pwrite(street_light_system_file_obj, str_j_vector.c_str(), str_j_vector.length() + 1, 0);
		close(street_light_system_file_obj);
	}
}

void get_time_setup(vector<uint8_t>& time_setup) {
	struct stat file_info;
	street_light_system_file_obj =  open(street_light_system_devs_file_path.c_str(), O_RDONLY | O_CREAT, 0666);

	if (street_light_system_file_obj < 0) {
		APP_DBG("can not open %s file\n", street_light_system_devs_file_path.c_str());
	}
	else {
		APP_DBG("opened %s file\n", street_light_system_devs_file_path.c_str());
		fstat(street_light_system_file_obj, &file_info);

		uint32_t file_buffer_len = file_info.st_size + 1;
		char* file_buffer = (char*)malloc(file_buffer_len);

		if (file_buffer == NULL) {
			return;
		}

		memset(file_buffer, 0, file_buffer_len);

		pread(street_light_system_file_obj, file_buffer, file_info.st_size, 0);

		close(street_light_system_file_obj);

		if (file_info.st_size) {
			json j = json::parse(file_buffer);

			if (j.size()) {
				/* clear list */
				time_setup.clear();

				/* update new element */
				for (json::iterator it = j.begin(); it != j.end(); ++it) {
					time_setup.push_back(*it);
				}
			}
		}

		free(file_buffer);
	}
}

void show_time_setup(vector<uint8_t>& time_setup) {
	//	uint8_t start_hr = (time_setup[0]);
	//	uint8_t start_min = (time_setup[1]);
	//	uint8_t end_hr = (time_setup[2]);
	//	uint8_t end_min = (time_setup[3]);

	//	cout << "TIME SETUP" << endl;
	//	if(time_setup.size()){
	//		cout << "START: " << start_hr << ":" << start_min << endl;
	//		cout << "END: " << end_hr << ":" << end_min << endl;
	//	}
	//	else{
	//		cout << "NOT YET" << endl;
	//	}
}

void regis_lora_usb_stick(void){
	/* regis recv msg lora usb stick	*/
	register_lora_usb_stick_t regis_msg;
	regis_msg.register_msg.app_id = STREET_LIGHT_APP_ID;
	regis_msg.register_msg.type_if = IF_TYPE_APP_MT;
	regis_msg.register_msg.task_id = MT_TASK_STREET_LIGHT_SYSTEM_ID;
	regis_msg.register_msg.sig = MT_STREET_LIGHT_SYSTEM_LORA_MSG_IN;

	ak_msg_t* s_msg = get_common_msg();
	set_msg_sig(s_msg, MT_IF_LORA_USB_STICK_REGIS);
	set_data_common_msg(s_msg, (uint8_t*)&regis_msg, sizeof(register_lora_usb_stick_t));
	task_post(MT_TASK_IF_LORA_USB_STICK_ID, s_msg);
}

