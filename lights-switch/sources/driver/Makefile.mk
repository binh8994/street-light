CFLAGS		+= -I./sources/driver/led
CFLAGS		+= -I./sources/driver/rtc
CFLAGS		+= -I./sources/driver/button
CFLAGS		+= -I./sources/driver/flash
CFLAGS		+= -I./sources/driver/onewire
CFLAGS		+= -I./sources/driver/kalman


CPPFLAGS	+= -I./sources/driver/thermistor
CPPFLAGS	+= -I./sources/driver/EmonLib
CPPFLAGS	+= -I./sources/driver/epprom
CPPFLAGS	+= -I./sources/driver/exor
CPPFLAGS	+= -I./sources/driver/ds1302

VPATH += sources/driver/led
VPATH += sources/driver/button
VPATH += sources/driver/rtc
VPATH += sources/driver/thermistor
VPATH += sources/driver/EmonLib
VPATH += sources/driver/eeprom
VPATH += sources/driver/flash
VPATH += sources/driver/exor
VPATH += sources/driver/onewire
VPATH += sources/driver/kalman
VPATH += sources/driver/ds1302


SOURCES += sources/driver/led/led.c
#SOURCES += sources/driver/rtc/rtc.c
SOURCES += sources/driver/button/button.c
#SOURCES += sources/driver/flash/flash.c
#SOURCES += sources/driver/onewire/onewire.c
#SOURCES += sources/driver/kalman/kalman.c


#SOURCES_CPP += sources/driver/thermistor/thermistor.cpp
#SOURCES_CPP += sources/driver/EmonLib/EmonLib.cpp
SOURCES_CPP += sources/driver/eeprom/eeprom.cpp
#SOURCES_CPP += sources/driver/exor/exor.cpp
SOURCES_CPP += sources/driver/ds1302/DS1302.cpp
