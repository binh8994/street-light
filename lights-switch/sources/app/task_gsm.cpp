#include "stdio.h"

#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"
#include "../ak/timer.h"
#include "../common/utils.h"
#include "../common/xprintf.h"
#include "../common/fifo.h"

#include "../app/app.h"
#include "../app/app_dbg.h"
#include "../app/task_list.h"

#include "../sys/sys_ctrl.h"
#include "../sys/sys_irq.h"
#include "../sys/sys_dbg.h"

#include "../platform/stm32l/io_cfg.h"
#include "../platform/stm32l/arduino/Arduino.h"

#include "task_gsm.h"

#define UART_BUFFER_LENGTH			255
#define TOPIC_BUFFER_LENGTH			50    //Maximum length allowed Topic
#define MESSAGE_BUFFER_LENGTH		250  //Maximum length allowed data

// ######################################################################################################################
#define RESERVED					0
#define CONNECT						1   //Client request to connect to Server                Client          Server
#define CONNACK						2   //Connect Acknowledgment                             Server/Client   Server/Client
#define PUBLISH						3   //Publish message                                    Server/Client   Server/Client
#define PUBACK						4   //Publish Acknowledgment                             Server/Client   Server/Client
#define PUBREC						5   //Publish Received (assured delivery part 1)         Server/Client   Server/Client
#define PUBREL						6   //Publish Release (assured delivery part 2)          Server/Client   Server/Client
#define PUBCOMP						7   //Publish Complete (assured delivery part 3)         Server/Client   Server/Client
#define SUBSCRIBE					8   //Client Subscribe request                           Client          Server
#define SUBACK						9   //Subscribe Acknowledgment                           Server          Client
#define UNSUBSCRIBE					10  //Client Unsubscribe request                         Client          Server
#define UNSUBACK					11  //Unsubscribe Acknowledgment                         Server          Client
#define PINGREQ						12  //PING Request                                       Client          Server
#define PINGRESP					13  //PING Response                                      Server          Client
#define DISCONNECT					14  //Client is Disconnecting                            Client          Server

// QoS value bit 2 bit 1 Description
//   0       0       0   At most once    Fire and Forget         <=1
//   1       0       1   At least once   Acknowledged delivery   >=1
//   2       1       0   Exactly once    Assured delivery        =1
//   3       1       1   Reserved
#define DUP_Mask								4   // Duplicate delivery   Only for QoS>0
#define QoS_Mask								6   // Quality of Service
#define QoS_Scale								2   // (()&QoS)/QoS_Scale
#define RETAIN_Mask								1   // RETAIN flag

#define User_Name_Flag_Mask						128
#define Password_Flag_Mask						64
#define Will_Retain_Mask						32
#define Will_QoS_Mask							24
#define Will_QoS_Scale							8
#define Will_Flag_Mask							4
#define Clean_Session_Mask						2

#define DISCONNECTED							0
#define CONNECTED								1
#define NO_ACKNOWLEDGEMENT						255

#define MQTT_PARSER_DATA_POOL_SIZE				2
#define MQTT_DATA_NULL							((mqtt_parser_t*)0)

#define IDLE_PROCESS							0
#define INIT_GSM_PROCESS						1
#define TCP_STATE_PROCESS						2
#define MQTT_PROCESS							3

#define FIFO_BUF_SIZE							(2047)

// ########## UART #######################
typedef struct {
	uint8_t		process;
	uint8_t		response_string[UART_BUFFER_LENGTH];
	uint8_t		string_index;
	uint8_t		expect_string[3][20];
	uint8_t		expect_num;
	uint32_t	timeout;
	uint8_t		next_sig;
	uint8_t		next_sig_if_timeout;
} uart_response_mng_t;

static uart_response_mng_t uart_response_mng;

// ######### MQTT #######################
typedef struct mqtt_parser_t {
	struct mqtt_parser_t* next;

	uint8_t		parser_state;
	uint8_t		parser_index;
	uint32_t	parser_multiplier;
	uint32_t	parser_remain_byte;

	uint8_t		msg_type;
	uint8_t		QoS;
	uint8_t		DUP;
	uint8_t		RETAIN;
	uint32_t	control_pack_length;
	uint8_t		recv_payload[512];

	uint8_t		recv_message[MESSAGE_BUFFER_LENGTH];
	uint32_t	recv_message_length;
	uint8_t		recv_topic[TOPIC_BUFFER_LENGTH];
	uint8_t		recv_topic_length;
} mqtt_parser_t;

static mqtt_parser_t* current_mqtt_parser;

typedef struct {
	uint8_t		is_connected;
	uint32_t	last_msg_id;
	uint8_t		in_process;
} mqtt_t;

static mqtt_t mqtt;

static const char MQTT_HOST[] = "118.69.135.199";
//static const char MQTT_HOST[] = "test.mosquitto.org";
static const char MQTT_PORT[] = "1883";
static const char APN[] = "v-internet";
static const char APN_USR[] = "";
static const char APN_PWD[] = "";

static uint8_t username[] = "QET9pd";
static uint8_t password[] = "9zMRZyrPZtTEyCp3";
static uint8_t clientid[] = "iot-lights-gsm";
static uint32_t keepalive = 0;//30s
static uint8_t sub_topic[] = "iot/flights/ctrl/demo/";
static uint8_t pub_topic[] = "iot/flights/data/demo/";
static uint8_t publish_buf[100];

static mqtt_parser_t mqtt_parser_data_pool[MQTT_PARSER_DATA_POOL_SIZE];
static mqtt_parser_t* free_list_mqtt_parser_data_pool;

static void mqtt_parser_data_pool_init();
static mqtt_parser_t* malloc_mqtt_parser_data();
static void free_mqtt_parser_data(mqtt_parser_t*);

static fifo_t uart_fifo;
static uint8_t	uart_fifo_buf[FIFO_BUF_SIZE];
static void fifo_clear();

static uint32_t generate_msg_id(void);
static void sendLength(uint32_t len);
static void sendUTFString(uint8_t *string);

static void connect(uint8_t *ClientIdentifier, uint8_t UserNameFlag, uint8_t PasswordFlag, uint8_t *UserName, uint8_t *Password,\
					uint8_t CleanSession, uint8_t WillFlag, uint8_t WillQoS, uint8_t WillRetain, uint8_t *WillTopic, uint8_t *WillMessage);
static void onconnect();
static void ondisconnnect();
static void onmessages(uint8_t* recv_topic, uint8_t* recv_message);
static void publish(uint8_t DUP, uint8_t Qos, uint8_t RETAIN,\
					uint32_t MessageID, uint8_t *Topic, uint8_t *Message);
static void publishACK(uint32_t MessageID);
static void publishREC(uint32_t MessageID);
static void publishREL(uint8_t DUP, uint32_t MessageID);
static void publishCOMP(uint32_t MessageID);
static void subscribe(uint8_t DUP, uint32_t MessageID, uint8_t *SubTopic, uint8_t SubQoS);
//static void unsubscribe(uint8_t DUP, uint32_t MessageID, uint8_t *SubTopic);
//static void disconnect(void);

static uint32_t substring_to_integer(uint8_t* str_begin, uint8_t* str_end);
static void clear_all();

void task_gsm(ak_msg_t* msg){

	switch (msg->sig) {

	case LIGHT_GSM_INIT: {
		APP_DBG("LIGHT_GSM_INIT\n");

		sim_cfg();
		mqtt_parser_data_pool_init();

		current_mqtt_parser = malloc_mqtt_parser_data();
		//APP_DBG("\nget_mqtt:%d\n", (uint32_t)current_mqtt_parser);

		clear_all();

		ak_msg_t* smsg = get_pure_msg();
		set_msg_sig(smsg, LIGHT_GSM_VERIFY_REQ);
		task_post(LIGHT_TASK_GSM_ID, smsg);

	}
		break;

	case LIGHT_GSM_VERIFY_REQ: {
		APP_DBG("LIGHT_GSM_VERIFY_REQ\n");

		clear_all();

		str_cpy((int8_t*)uart_response_mng.expect_string[0], (const int8_t*)"OK");
		uart_response_mng.expect_num = 1;
		uart_response_mng.string_index = 0;
		uart_response_mng.timeout = 2000;
		uart_response_mng.next_sig = LIGHT_GSM_READY_REQ;
		uart_response_mng.next_sig_if_timeout = LIGHT_GSM_VERIFY_REQ;
		uart_response_mng.process = INIT_GSM_PROCESS;

		sim_puts("\r\nATE0\r\n");
		//sim_puts("AT+IPR=115200\r\n");

		timer_set(LIGHT_TASK_GSM_ID, uart_response_mng.next_sig_if_timeout, uart_response_mng.timeout, TIMER_ONE_SHOT);

	}
		break;

	case LIGHT_GSM_READY_REQ: {
		APP_DBG("LIGHT_GSM_READY_REQ\n");

		clear_all();

		str_cpy((int8_t*)uart_response_mng.expect_string[0], (const int8_t*)"0,1");
		uart_response_mng.expect_num = 1;
		uart_response_mng.string_index = 0;
		uart_response_mng.timeout = 2000;
		uart_response_mng.next_sig = LIGHT_GSM_TCP_INIT;
		uart_response_mng.next_sig_if_timeout = LIGHT_GSM_READY_REQ;
		uart_response_mng.process = INIT_GSM_PROCESS;

		sim_puts("\r\nAT+CREG?\r\n");

		timer_set(LIGHT_TASK_GSM_ID, uart_response_mng.next_sig_if_timeout, uart_response_mng.timeout, TIMER_ONE_SHOT);

	}
		break;

	case  LIGHT_GSM_TCP_INIT: {
		APP_DBG("LIGHT_GSM_TCP_INIT\n");

		clear_all();

		sim_puts("\r\nAT+CIPSHUT\r\n");
		delay(10);
		sim_puts("\r\nAT+CIPMUX=0\r\n");
		delay(10);
		sim_puts("\r\nAT+CIPMODE=1\r\n");
		delay(10);

		str_cpy((int8_t*)uart_response_mng.expect_string[0], (const int8_t*)"OK");
		uart_response_mng.expect_num = 1;
		uart_response_mng.string_index = 0;
		uart_response_mng.timeout = 3000;
		uart_response_mng.next_sig = LIGHT_GSM_TCP_CHECK_STATE;
		uart_response_mng.next_sig_if_timeout = LIGHT_GSM_TCP_INIT;
		uart_response_mng.process = INIT_GSM_PROCESS;

		sim_puts("\r\nAT+CGATT=1\r\n");

		timer_set(LIGHT_TASK_GSM_ID, uart_response_mng.next_sig_if_timeout, uart_response_mng.timeout, TIMER_ONE_SHOT);
	}
		break;

	case LIGHT_GSM_TCP_CHECK_STATE: {
		APP_DBG("LIGHT_GSM_TCP_CHECK_STATE\n");

		clear_all();

		uart_response_mng.process = TCP_STATE_PROCESS;

		sim_puts("\r\nAT+CIPSTATUS\r\n");

		timer_set(LIGHT_TASK_GSM_ID, LIGHT_GSM_TCP_CHECK_STATE, 1000, TIMER_ONE_SHOT);
	}
		break;

	case LIGHT_GSM_TCP_STATE_1: {
		APP_DBG("LIGHT_GSM_TCP_STATE_1\n");

		clear_all();

		uart_response_mng.process = TCP_STATE_PROCESS;

		//AT+CSTT="v-internet","",""
		sim_puts("\r\nAT+CSTT=\"");
		sim_puts(APN);		sim_puts("\",\"");
		sim_puts(APN_USR);	sim_puts("\",\"");
		sim_puts(APN_PWD);	sim_puts("\"\r\n");

		timer_set(LIGHT_TASK_GSM_ID, LIGHT_GSM_TCP_CHECK_STATE, 1000, TIMER_ONE_SHOT);

	}
		break;

	case LIGHT_GSM_TCP_STATE_2: {
		APP_DBG("LIGHT_GSM_TCP_STATE_2\n");

		clear_all();

		uart_response_mng.process = TCP_STATE_PROCESS;

		sim_puts("\r\nAT+CIICR\r\n");

		timer_set(LIGHT_TASK_GSM_ID, LIGHT_GSM_TCP_CHECK_STATE, 1000, TIMER_ONE_SHOT);

	}
		break;

	case LIGHT_GSM_TCP_STATE_3: {
		APP_DBG("LIGHT_GSM_TCP_STATE_3\n");

		clear_all();

		uart_response_mng.process = TCP_STATE_PROCESS;

		sim_puts("\r\nAT+CIFSR\r\n");

		timer_set(LIGHT_TASK_GSM_ID, LIGHT_GSM_TCP_CHECK_STATE, 1000, TIMER_ONE_SHOT);
	}
		break;

	case LIGHT_GSM_START_TCP: {
		APP_DBG("LIGHT_GSM_START_TCP\n");

		clear_all();

		str_cpy((int8_t*)uart_response_mng.expect_string[0], (const int8_t*)"CONNECT");
		uart_response_mng.expect_num = 1;
		uart_response_mng.string_index = 0;
		uart_response_mng.timeout = 20000;
		uart_response_mng.next_sig = LIGHT_GSM_TCP_CONNECTED;
		uart_response_mng.next_sig_if_timeout = LIGHT_GSM_TCP_CHECK_STATE;
		uart_response_mng.process = INIT_GSM_PROCESS;

		//AT+CIPSTART="TCP","118.69.135.199","1883"
		sim_puts("\r\nAT+CIPSTART=\"TCP\",\"");
		sim_puts(MQTT_HOST);	sim_puts("\",\"");
		sim_puts(MQTT_PORT);	sim_puts("\"\r\n");

	}
		break;

	case LIGHT_GSM_TCP_CONNECTED: {
		APP_DBG("LIGHT_GSM_TCP_CONNECTED\n");

		clear_all();

		uart_response_mng.process = MQTT_PROCESS;
		uart_response_mng.string_index = 0;
		current_mqtt_parser->parser_state = 0;

		fifo_clear();

		connect(clientid, 1, 1, username, password, 1, 0, 0, 0, 0, 0);

		//timer_set(LIGHT_TASK_GSM_ID, LIGHT_GSM_DATA_PROCESS, LIGHT_GSM_COMMON_INTERVAL, TIMER_ONE_SHOT);

		ak_msg_t* smsg = get_pure_msg();
		set_msg_sig(smsg, LIGHT_GSM_DATA_PROCESS);
		task_post(LIGHT_TASK_GSM_ID, smsg);

		//timer_set(LIGHT_TASK_GSM_ID, LIGHT_GSM_MQTT_PING_REQ, keepalive*1000, TIMER_PERIODIC);

	}
		break;

	case LIGHT_GSM_SHUT_TCP:{
		APP_DBG("LIGHT_GSM_SHUT_TCP\n");

		clear_all();

		sim_puts("\r\nAT+CIPSHUT\r\n");

		ak_msg_t* smsg = get_pure_msg();
		set_msg_sig(smsg, LIGHT_GSM_VERIFY_REQ);
		task_post(LIGHT_TASK_GSM_ID, smsg);

	}
		break;

	case LIGHT_GSM_MQTT_PING_REQ: {
		if(!mqtt.in_process) {
			APP_DBG("LIGHT_GSM_MQTT_PING\n");

			sim_putc(uint8_t(PINGREQ * 16));
			sendLength(0);

			timer_set(LIGHT_TASK_GSM_ID, LIGHT_GSM_MQTT_PING_TIMEOUT, LIGHT_GSM_MQTT_PING_REQ_INTERVAL, TIMER_ONE_SHOT);
		}
	}
		break;

	case LIGHT_GSM_MQTT_PING_TIMEOUT: {
		APP_DBG("LIGHT_GSM_MQTT_PING_TIMEOUT\n");

	}
		break;

	case LIGHT_GSM_DATA_PROCESS: {

		if(fifo_availble(&uart_fifo)) {
			uint8_t inChar;
			fifo_get(&uart_fifo, &inChar);

			current_mqtt_parser->msg_type = (inChar / 16) & 0x0F;
			//current_mqtt_parser->DUP = (inChar & DUP_Mask) / DUP_Mask;
			current_mqtt_parser->QoS = (inChar & QoS_Mask) / QoS_Scale;
			//current_mqtt_parser->RETAIN = (inChar & RETAIN_Mask);

			APP_DBG("msg_type:%d\n", current_mqtt_parser->msg_type);

			if ((current_mqtt_parser->msg_type >= CONNECT) && (current_mqtt_parser->msg_type <= DISCONNECT))
			{
				current_mqtt_parser->parser_remain_byte = true;
				current_mqtt_parser->control_pack_length = 0;
				current_mqtt_parser->parser_multiplier = 1;
				char Cchar = inChar;
				while ( current_mqtt_parser->parser_remain_byte )
				{
					if (fifo_availble(&uart_fifo))
					{
						fifo_get(&uart_fifo, &inChar);
						if ((((Cchar & 0xFF) == 'C') && ((inChar & 0xFF) == 'L') && (current_mqtt_parser->control_pack_length == 0)) || \
								(((Cchar & 0xFF) == '+') && ((inChar & 0xFF) == 'P') && (current_mqtt_parser->control_pack_length == 0)))
						{
							timer_remove_attr(LIGHT_TASK_GSM_ID, LIGHT_GSM_MQTT_PING_REQ);
							uart_response_mng.process = INIT_GSM_PROCESS;
							mqtt.is_connected = 0;

							current_mqtt_parser->parser_index = 0;
							//current_mqtt_parser->recv_payload[current_mqtt_parser->parser_index++] = Cchar;
							//current_mqtt_parser->recv_payload[current_mqtt_parser->parser_index++] = inChar;

							ondisconnnect();
						}
						else
						{
							if ((inChar & 128) == 128)
							{
								current_mqtt_parser->control_pack_length += (inChar & 127) *  current_mqtt_parser->parser_multiplier;
								current_mqtt_parser->parser_multiplier *= 128;
							}
							else
							{
								current_mqtt_parser->parser_remain_byte = false;
								current_mqtt_parser->control_pack_length += (inChar & 127) *  current_mqtt_parser->parser_multiplier;
								current_mqtt_parser->parser_multiplier *= 128;
							}
						}
					}
				}

				APP_DBG("length:%d\n", current_mqtt_parser->control_pack_length);

				current_mqtt_parser->parser_remain_byte = current_mqtt_parser->control_pack_length;

				current_mqtt_parser->parser_index = 0;

					while ((current_mqtt_parser->parser_remain_byte-- > 0) && (fifo_availble(&uart_fifo)))
					{
						fifo_get(&uart_fifo, &inChar);
						current_mqtt_parser->recv_payload[current_mqtt_parser->parser_index++] = inChar;
					}

					if (current_mqtt_parser->msg_type == CONNACK){
						//APP_DBG("CONNACK\n");
						uint32_t ConnectionAck = current_mqtt_parser->recv_payload[0] * 256 + current_mqtt_parser->recv_payload[1];
						if (ConnectionAck == 0){
							mqtt.is_connected = 1;
							onconnect();
						}
					}
					else if (current_mqtt_parser->msg_type == PUBLISH){
						//APP_DBG("PUBLISH\n");
						uint32_t topic_len = current_mqtt_parser->recv_payload[0] *256 + current_mqtt_parser->recv_payload[1];
						uint32_t index = 0;
						for (uint32_t i = 2; i < topic_len + 2; i++) {
							current_mqtt_parser->recv_topic[index++] = current_mqtt_parser->recv_payload[i];
						}

						current_mqtt_parser->recv_topic[index] = 0;
						current_mqtt_parser->recv_topic_length = index;

						index = 0;

						uint32_t msg_begin = topic_len + 2;
						uint32_t message_id = 0;

						if (current_mqtt_parser->QoS != 0){
							msg_begin += 2;
							message_id = current_mqtt_parser->recv_payload[topic_len + 2] * 256 + current_mqtt_parser->recv_payload[topic_len + 3];
						}

						for (uint32_t i = msg_begin; i < (current_mqtt_parser->control_pack_length); i++){
							current_mqtt_parser->recv_message[index++] = current_mqtt_parser->recv_payload[i];
						}

						current_mqtt_parser->recv_message[index] = 0;
						current_mqtt_parser->recv_message_length = index;

						if (current_mqtt_parser->QoS == 1){
							publishACK(message_id);// review
						}
						else if (current_mqtt_parser->QoS == 2){
							publishREC(message_id);// review
						}

						APP_DBG("MQTT onmessages\n");
						//onmessages(current_mqtt_parser->recv_topic, current_mqtt_parser->recv_message);
					}
					else if (current_mqtt_parser->msg_type == PUBREC){
						//APP_DBG("PUBREC\n");
						publishREL(0, current_mqtt_parser->recv_payload[0] * 256 + current_mqtt_parser->recv_payload[1]) ;
						//APP_DBG("Message ID:%d\n", inputString[0] * 256 + inputString[1]) ;
					}
					else if (current_mqtt_parser->msg_type == PUBREL){
						//APP_DBG("PUBREL\n");
						publishCOMP(current_mqtt_parser->recv_payload[0] * 256 + current_mqtt_parser->recv_payload[1]);
						//APP_DBG("Message ID:%d\n", inputString[0] * 256 + inputString[1]) ;
					}
					else if (current_mqtt_parser->msg_type == PUBACK){
						//APP_DBG("PUBACK\n");
					}
					else if (current_mqtt_parser->msg_type == PUBCOMP) {
						//APP_DBG("PUBCOMP\n");
					}
					else if(current_mqtt_parser->msg_type == SUBACK) {
						//APP_DBG("SUBACK\n");
					}
					else if(current_mqtt_parser->msg_type == UNSUBACK) {
						//APP_DBG("UNSUBACK\n");
						//APP_DBG("Message ID:%d\n", inputString[0] * 256 + inputString[1]) ;
					}
					else if(current_mqtt_parser->msg_type == PINGRESP) {
						//APP_DBG("PINGRESP\n");
						timer_remove_attr(LIGHT_TASK_GSM_ID, LIGHT_GSM_MQTT_PING_TIMEOUT);
					}

			}

		}

		timer_set(LIGHT_TASK_GSM_ID, LIGHT_GSM_DATA_PROCESS, LIGHT_GSM_COMMON_INTERVAL, TIMER_ONE_SHOT);

		//ak_msg_t* smsg = get_pure_msg();
		//set_msg_sig(smsg, LIGHT_GSM_DATA_PROCESS);
		//task_post(LIGHT_TASK_GSM_ID, smsg);
	}
		break;
#if 0
	case LIGHT_GSM_PARSER_PAYLOAD: {
		APP_DBG("LIGHT_GSM_PARSER_PAYLOAD\n");

		mqtt_parser_t* mqtt_parser = (mqtt_parser_t*)*((uint32_t*)get_data_common_msg(msg));

		if (mqtt_parser->msg_type == CONNACK){
			APP_DBG("CONNACK\n");
			uint32_t ConnectionAck = mqtt_parser->recv_payload[0] * 256 + mqtt_parser->recv_payload[1];
			if (ConnectionAck == 0){
				mqtt.is_connected = 1;
				onconnect();
			}
		}
		else if (mqtt_parser->msg_type == PUBLISH){
			APP_DBG("PUBLISH\n");
			uint32_t topic_len = mqtt_parser->recv_payload[0] *256 + mqtt_parser->recv_payload[1];
			uint32_t index = 0;
			for (uint32_t i = 2; i < topic_len + 2; i++) {
				mqtt_parser->recv_topic[index++] = mqtt_parser->recv_payload[i];
			}

			mqtt_parser->recv_topic[index] = 0;
			mqtt_parser->recv_topic_length = index;

			index = 0;

			uint32_t msg_begin = topic_len + 2;
			uint32_t message_id = 0;

			if (mqtt_parser->QoS != 0){
				msg_begin += 2;
				message_id = mqtt_parser->recv_payload[topic_len + 2] * 256 + mqtt_parser->recv_payload[topic_len + 3];
			}

			for (uint32_t i = msg_begin; i < (mqtt_parser->control_pack_length); i++){
				mqtt_parser->recv_message[index++] = mqtt_parser->recv_payload[i];
			}

			mqtt_parser->recv_message[index] = 0;
			mqtt_parser->recv_message_length = index;

			if (mqtt_parser->QoS == 1){
				publishACK(message_id);// review
			}
			else if (mqtt_parser->QoS == 2){
				publishREC(message_id);// review
			}

			onmessages(mqtt_parser->recv_topic, mqtt_parser->recv_message);
		}
		else if (mqtt_parser->msg_type == PUBREC){
			APP_DBG("PUBREC\n");
			publishREL(0, mqtt_parser->recv_payload[0] * 256 + mqtt_parser->recv_payload[1]) ;
			//APP_DBG("Message ID:%d\n", inputString[0] * 256 + inputString[1]) ;
		}
		else if (mqtt_parser->msg_type == PUBREL){
			APP_DBG("PUBREL\n");
			publishCOMP(mqtt_parser->recv_payload[0] * 256 + mqtt_parser->recv_payload[1]);
			//APP_DBG("Message ID:%d\n", inputString[0] * 256 + inputString[1]) ;
		}
		else if (mqtt_parser->msg_type == PUBACK){
			APP_DBG("PUBACK\n");
		}
		else if (mqtt_parser->msg_type == PUBCOMP) {
			APP_DBG("PUBCOMP\n");
		}
		else if(mqtt_parser->msg_type == SUBACK) {
			APP_DBG("SUBACK\n");
		}
		else if(mqtt_parser->msg_type == UNSUBACK) {
			APP_DBG("UNSUBACK\n");
			//APP_DBG("Message ID:%d\n", inputString[0] * 256 + inputString[1]) ;
		}
		else if(mqtt_parser->msg_type == PINGRESP) {
			APP_DBG("PINGRESP\n");
			timer_remove_attr(LIGHT_TASK_GSM_ID, LIGHT_GSM_MQTT_PING_TIMEOUT);
		}

		//APP_DBG("\nfree_mqtt:%d\n", (uint32_t)mqtt_parser);
		free_mqtt_parser_data(mqtt_parser);

	}
		break;
#endif
	case LIGHT_GSM_MQTT_PUB_MSG:{
		APP_DBG("LIGHT_GSM_MQTT_PUB_MSG\n");
		if(mqtt.is_connected && !mqtt.in_process){

			uint32_t dig;
			lora_pkg_t r_pkg;
			uint8_t buf_id[9];
			mem_cpy(&r_pkg, get_data_common_msg(msg), sizeof(lora_pkg_t));
			if(r_pkg.data.sen_data.curr_sen > 90){
				dig = 1;
			}
			else{
				dig = 0;
			}

			sprintf((char*)buf_id,"%8d",(int) r_pkg.hdr.src_addr);
			for(uint8_t i = 0; i<8; i++)
				if( buf_id[i] == ' ' )
					buf_id[i] = '0';

			/*	{"arrayOfLight": [{ "id": "00000001", "current": 111 , "status": 0}]}	*/
			mem_set(publish_buf, 0, 100);
			uint8_t id = sprintf((char*)publish_buf, \
								 "{\"arrayOfLight\":[{\"id\":\"%s\",\"current\":%d,\"status\":%d}]}",\
								 buf_id, (int)r_pkg.data.sen_data.curr_sen, (int)dig);
			publish_buf[id] = 0;

			publish(0, 0, 1, generate_msg_id(), pub_topic, publish_buf);

			//APP_DBG("%s\n", publish_buf);
		}
		else {
			APP_DBG("mqtt not connected\n");
		}
	}
		break;

	default:
		break;
	}
}


void sys_irq_sim(){
	uint8_t c = sim_getc();
//	USART_SendData(USART1, (uint8_t)c);
//	while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);

	if(uart_response_mng.process == INIT_GSM_PROCESS) {
		if(c != '\n' && uart_response_mng.string_index < UART_BUFFER_LENGTH) {
			uart_response_mng.response_string[uart_response_mng.string_index++] = c;
			uart_response_mng.response_string[uart_response_mng.string_index] = 0;
		}
		else {
			if(uart_response_mng.string_index <= 1) {
				uart_response_mng.string_index = 0;
			}
			else {
				uart_response_mng.response_string[uart_response_mng.string_index] = 0;

				for(uint32_t i = 0; i < uart_response_mng.expect_num; i++) {

					if(str_str(uart_response_mng.response_string, uart_response_mng.expect_string[i])) {

						uart_response_mng.process = IDLE_PROCESS;

						timer_remove_attr(LIGHT_TASK_GSM_ID, uart_response_mng.next_sig_if_timeout);

						ak_msg_t* smsg = get_pure_msg();
						set_msg_sig(smsg, uart_response_mng.next_sig);
						task_post(LIGHT_TASK_GSM_ID, smsg);

					}
				}
			}
		}
	}
	else if(uart_response_mng.process == TCP_STATE_PROCESS) {
		if(c != '\n' && uart_response_mng.string_index < UART_BUFFER_LENGTH) {
			uart_response_mng.response_string[uart_response_mng.string_index++] = c;
			uart_response_mng.response_string[uart_response_mng.string_index] = 0;
		}
		else {
			if(uart_response_mng.string_index <= 1) {
				uart_response_mng.string_index = 0;
			}
			else {
				uart_response_mng.response_string[uart_response_mng.string_index] = 0;

				if (str_str(uart_response_mng.response_string, (uint8_t*)"IP INITIAL") ){

					uart_response_mng.process = IDLE_PROCESS;

					timer_remove_attr(LIGHT_TASK_GSM_ID, uart_response_mng.next_sig_if_timeout);

					ak_msg_t* smsg = get_pure_msg();
					set_msg_sig(smsg, LIGHT_GSM_TCP_STATE_1);
					task_post(LIGHT_TASK_GSM_ID, smsg);

				}
				else if (str_str(uart_response_mng.response_string, (uint8_t*)"IP START") ){

					uart_response_mng.process = IDLE_PROCESS;

					timer_remove_attr(LIGHT_TASK_GSM_ID, uart_response_mng.next_sig_if_timeout);

					ak_msg_t* smsg = get_pure_msg();
					set_msg_sig(smsg, LIGHT_GSM_TCP_STATE_2);
					task_post(LIGHT_TASK_GSM_ID, smsg);

				}
				else if (str_str(uart_response_mng.response_string, (uint8_t*)"IP GPRSACT") || \
						 str_str(uart_response_mng.response_string, (uint8_t*)"IP CONFIG")){

					uart_response_mng.process = IDLE_PROCESS;

					timer_remove_attr(LIGHT_TASK_GSM_ID, uart_response_mng.next_sig_if_timeout);

					ak_msg_t* smsg = get_pure_msg();
					set_msg_sig(smsg, LIGHT_GSM_TCP_STATE_3);
					task_post(LIGHT_TASK_GSM_ID, smsg);
				}
				else if ((str_str(uart_response_mng.response_string, (uint8_t*)"IP STATUS") ) || \
						 (str_str(uart_response_mng.response_string, (uint8_t*)"TCP CLOSED") )){

					uart_response_mng.process = IDLE_PROCESS;

					timer_remove_attr(LIGHT_TASK_GSM_ID, uart_response_mng.next_sig_if_timeout);

					ak_msg_t* smsg = get_pure_msg();
					set_msg_sig(smsg, LIGHT_GSM_START_TCP);
					task_post(LIGHT_TASK_GSM_ID, smsg);

				}
				else if (str_str(uart_response_mng.response_string, (uint8_t*)"TCP CONNECTING") ){

					uart_response_mng.process = IDLE_PROCESS;

					timer_remove_attr(LIGHT_TASK_GSM_ID, uart_response_mng.next_sig_if_timeout);

					timer_set(LIGHT_TASK_GSM_ID, LIGHT_GSM_TCP_CHECK_STATE, 1000, TIMER_ONE_SHOT);

				}
				else if ((str_str(uart_response_mng.response_string, (uint8_t*)"CONNECT OK") ) || \
						 (str_str(uart_response_mng.response_string, (uint8_t*)"CONNECT FAIL") ) || \
						 (str_str(uart_response_mng.response_string, (uint8_t*)"PDP DEACT") )){

					uart_response_mng.process = IDLE_PROCESS;

					timer_remove_attr(LIGHT_TASK_GSM_ID, uart_response_mng.next_sig_if_timeout);

					ak_msg_t* smsg = get_pure_msg();
					set_msg_sig(smsg, LIGHT_GSM_SHUT_TCP);
					task_post(LIGHT_TASK_GSM_ID, smsg);
				}

			}
		}

	}
	else if(uart_response_mng.process == MQTT_PROCESS) {

		USART_SendData(USART1, (uint8_t)c);
		while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
		fifo_put(&uart_fifo, &c);

	}
}

void sendUTFString(uint8_t *string) {
	uint32_t localLength = str_len((int8_t*)string);
	sim_putc(uint8_t(localLength / 256));
	sim_putc(uint8_t(localLength % 256));
	sim_puts((const char*)string);
}

void sendLength(uint32_t len) {
	bool  length_flag = false;
	while (length_flag == false){
		if ((len / 128) > 0){
			sim_putc(uint8_t(len % 128 + 128));
			len /= 128;
		}
		else{
			length_flag = true;
			sim_putc(uint8_t(len));
		}
	}
}

void connect(uint8_t *ClientIdentifier, uint8_t UserNameFlag, uint8_t PasswordFlag, uint8_t *UserName, uint8_t *Password,\
			 uint8_t CleanSession, uint8_t WillFlag, uint8_t WillQoS, uint8_t WillRetain, uint8_t *WillTopic, uint8_t *WillMessage) {
	APP_DBG("MQTT connect\n");

	sim_putc(uint8_t(CONNECT * 16 ));
	uint8_t ProtocolName[7] = "MQIsdp";
	uint8_t ProtocolVersion = 3;
	int localLength = (2 + str_len((int8_t*)ProtocolName)) + 1 + 3 + (2 + str_len((int8_t*)ClientIdentifier));

	if (WillFlag != 0){
		localLength = localLength + 2 + str_len((int8_t*)WillTopic) + 2 + str_len((int8_t*)WillMessage);
	}

	if (UserNameFlag != 0){
		localLength = localLength + 2 + str_len((int8_t*)UserName);

		if (PasswordFlag != 0){
			localLength = localLength + 2 + str_len((int8_t*)Password);
		}
	}

	sendLength(localLength);
	sendUTFString(ProtocolName);
	sim_putc(uint8_t(ProtocolVersion));
	sim_putc(uint8_t(UserNameFlag * User_Name_Flag_Mask + PasswordFlag * Password_Flag_Mask + WillRetain * Will_Retain_Mask + WillQoS * Will_QoS_Scale + WillFlag * Will_Flag_Mask + CleanSession * Clean_Session_Mask));
	sim_putc(uint8_t(keepalive / 256));
	sim_putc(uint8_t(keepalive % 256));
	sendUTFString(ClientIdentifier);

	if (WillFlag != 0){
		sendUTFString(WillTopic);
		sendUTFString(WillMessage);
	}

	if (UserNameFlag != 0){
		sendUTFString(UserName);
		if (PasswordFlag != 0){
			sendUTFString(Password);
		}
	}
}


void onconnect() {
	APP_DBG("MQTT onconnect\n");

	//subscribe(0, generate_msg_id(), (uint8_t*)"SampleTopic", 1);
	subscribe(0, generate_msg_id(), sub_topic, 0);
	//publish(0, 0, 1, generate_msg_id(), (uint8_t*)"SampleTopic", (uint8_t*)"hello");
}

void ondisconnnect() {
	APP_DBG("MQTT ondisconnnect\n");

	ak_msg_t* smsg = get_pure_msg();
	set_msg_sig(smsg, LIGHT_GSM_VERIFY_REQ);
	task_post(LIGHT_TASK_GSM_ID, smsg);
}

void onmessages(uint8_t* recv_topic, uint8_t* recv_message) {

	//APP_DBG("\n%d:%s\n", topic_len, mqtt.recv_topic);
	APP_DBG("MQTT onmessages\n");
	//APP_DBG("len:%d\n", str_len((const int8_t*)mqtt.recv_message));
	//APP_DBG("mes:%s\n", recv_message);

	if(str_cmp((int8_t*)recv_topic, (int8_t*)sub_topic) == 0){
		/* *
		 * {"power": 0, "start_min": 27, "request": 1499764209,
		 * "start_hour": 9, "end_hour": 9, "mode": 0, "end_min": 26}
		 * */
		ctr_data_t r_ctr;

		if(str_str(recv_message, (uint8_t*)"\"mode\": 1")){
			r_ctr.mod = 1;
			if(str_str(recv_message, (uint8_t*)"\"power\": 1")){
				r_ctr.mod_data.power = 1;
				//APP_DBG("power:%d\n", r_ctr.mod_data.power);
			}
			else if(str_str(recv_message, (uint8_t*)"\"power\": 0")){
				r_ctr.mod_data.power = 0;
				//APP_DBG("power:%d\n", r_ctr.mod_data.power);
			}
			else {
				return;	/*invalid para*/
			}
		}
		else if(str_str(recv_message, (uint8_t*)"\"mode\": 0")){
			r_ctr.mod = 0;

			uint8_t* begin = str_str(recv_message, (uint8_t*)"start_hour") + 13;
			uint8_t* end = str_str(recv_message, (uint8_t*)", \"end_hour") -1;
			if(begin && end){
				r_ctr.mod_data.time_set.start_hour = substring_to_integer(begin, end);
				//APP_DBG("start_hour:%d\n", r_ctr.mod_data.time_set.start_hour);
			}
			else {
				return;/*invalid para*/
			}

			begin = str_str(recv_message, (uint8_t*)"start_min") + 12;
			end = str_str(recv_message, (uint8_t*)", \"request") -1;
			if(begin && end){
				r_ctr.mod_data.time_set.start_min = substring_to_integer(begin, end);
				//APP_DBG("start_min:%d\n", r_ctr.mod_data.time_set.start_min);
			}
			else {
				return;/*invalid para*/
			}

			begin = str_str(recv_message, (uint8_t*)"end_hour") + 11;
			end = str_str(recv_message, (uint8_t*)", \"mode") -1;
			if(begin && end){
				r_ctr.mod_data.time_set.end_hour = substring_to_integer(begin, end);
				//APP_DBG("end_hour:%d\n", r_ctr.mod_data.time_set.end_hour);
			}
			else {
				return;/*invalid para*/
			}

			begin = str_str(recv_message, (uint8_t*)"end_min") + 10;
			end = str_str(recv_message, (uint8_t*)"}") -1;
			if(begin && end){
				r_ctr.mod_data.time_set.end_min = substring_to_integer(begin, end);
				//APP_DBG("end_min:%d\n", r_ctr.mod_data.time_set.end_min);
			}
			else {
				return;/*invalid para*/
			}
		}

		ak_msg_t* sm = get_common_msg();
		set_data_common_msg(sm, (uint8_t*)&r_ctr, sizeof(ctr_data_t));
		set_msg_sig(sm, LIGHT_SWITCH_CONTROL);
		task_post(LIGHT_TASK_SWITCH_ID, sm);
	}

}

void publish(uint8_t DUP, uint8_t Qos, uint8_t RETAIN,\
			 uint32_t MessageID, uint8_t *Topic, uint8_t *Message){
	APP_DBG("MQTT publish\n");

	sim_putc(uint8_t(PUBLISH * 16 + DUP * DUP_Mask + Qos * QoS_Scale + RETAIN));
	int localLength = (2 + str_len((int8_t*)Topic));

	if (Qos > 0){
		localLength += 2;
	}
	localLength += str_len((int8_t*)Message);
	sendLength(localLength);
	sendUTFString(Topic);

	if (Qos > 0){
		sim_putc(uint8_t(MessageID / 256));
		sim_putc(uint8_t(MessageID % 256));
	}

	sim_puts((const char*)Message);
}

void publishACK(uint32_t MessageID) {
	sim_putc(uint8_t(PUBACK * 16));
	sendLength(2);
	sim_putc(uint8_t(MessageID / 256));
	sim_putc(uint8_t(MessageID % 256));
}

void publishREC(uint32_t MessageID) {
	sim_putc(uint8_t(PUBREC * 16));
	sendLength(2);
	sim_putc(uint8_t(MessageID / 256));
	sim_putc(uint8_t(MessageID % 256));
}

void publishREL(uint8_t DUP, uint32_t MessageID) {
	sim_putc(uint8_t(PUBREL * 16 + DUP * DUP_Mask + 1 * QoS_Scale));
	sendLength(2);
	sim_putc(uint8_t(MessageID / 256));
	sim_putc(uint8_t(MessageID % 256));
}

void publishCOMP(uint32_t MessageID) {
	sim_putc(uint8_t(PUBCOMP * 16));
	sendLength(2);
	sim_putc(uint8_t(MessageID / 256));
	sim_putc(uint8_t(MessageID % 256));
}

void subscribe(uint8_t DUP, uint32_t MessageID, uint8_t *SubTopic, uint8_t SubQoS) {
	APP_DBG("MQTT subscribe\n");
	sim_putc(uint8_t(SUBSCRIBE * 16 + DUP * DUP_Mask + 1 * QoS_Scale));
	int localLength = 2 + (2 + str_len((int8_t*)SubTopic)) + 1;
	sendLength(localLength);
	sim_putc(uint8_t(MessageID / 256));
	sim_putc(uint8_t(MessageID % 256));
	sendUTFString(SubTopic);
	sim_putc(SubQoS);

}

//void unsubscribe(uint8_t DUP, uint32_t MessageID, uint8_t *SubTopic) {
//	APP_DBG("MQTT unsubscribe\n");
//	sim_putc(uint8_t(UNSUBSCRIBE * 16 + DUP * DUP_Mask + 1 * QoS_Scale));
//	int localLength = (2 + str_len((int8_t*)SubTopic)) + 2;
//	sendLength(localLength);

//	sim_putc(uint8_t(MessageID / 256));
//	sim_putc(uint8_t(MessageID % 256));

//	sendUTFString(SubTopic);
//}

//void disconnect(void) {
//	APP_DBG("MQTT disconnect\n");
//	sim_putc(uint8_t(DISCONNECT * 16));
//	sendLength(0);
//	timer_remove_attr(LIGHT_TASK_GSM_ID, LIGHT_GSM_MQTT_PING);
//}

uint32_t generate_msg_id(void) {
	if (mqtt.last_msg_id < 65535){
		return ++mqtt.last_msg_id;
	}
	else{
		mqtt.last_msg_id = 0;
		return mqtt.last_msg_id;
	}
}

uint32_t substring_to_integer(uint8_t* str_begin, uint8_t* str_end){
	uint32_t res = 0; // Initialize result
	uint8_t* i;
	for (i = str_begin; i <= str_end; i++)
		res = res*10 + *i - '0';

	return res;
}

void clear_all() {
	timer_remove_attr(LIGHT_TASK_GSM_ID, uart_response_mng.next_sig_if_timeout);
	timer_remove_attr(LIGHT_TASK_GSM_ID, LIGHT_GSM_MQTT_PING_REQ);
	timer_remove_attr(LIGHT_TASK_GSM_ID, LIGHT_GSM_TCP_CHECK_STATE);

	mem_set(current_mqtt_parser, 0, sizeof(mqtt_parser_t));
	mem_set(&uart_response_mng, 0, sizeof(uart_response_mng_t));
	mem_set(&mqtt, 0, sizeof(mqtt_t));
}

static void mqtt_parser_data_pool_init() {
	uint32_t index = 0;

	free_list_mqtt_parser_data_pool = (mqtt_parser_t*)mqtt_parser_data_pool;

	for (index = 0; index < MQTT_PARSER_DATA_POOL_SIZE; index++) {
		if (index == (MQTT_PARSER_DATA_POOL_SIZE - 1)) {
			mqtt_parser_data_pool[index].next = MQTT_DATA_NULL;
		}
		else {
			mqtt_parser_data_pool[index].next = (mqtt_parser_t*)&mqtt_parser_data_pool[index + 1];
		}
	}
}

static mqtt_parser_t* malloc_mqtt_parser_data() {
	mqtt_parser_t* allocate_mqtt_data = MQTT_DATA_NULL;

	ENTRY_CRITICAL();
	allocate_mqtt_data = free_list_mqtt_parser_data_pool;

	if (allocate_mqtt_data == MQTT_DATA_NULL) {
		FATAL("MQ", 0x01);
	}
	else {
		free_list_mqtt_parser_data_pool = allocate_mqtt_data->next;
	}

	EXIT_CRITICAL();

	return allocate_mqtt_data;
}

static void free_mqtt_parser_data(mqtt_parser_t* mqtt_data) {
	ENTRY_CRITICAL();
	mqtt_data->next = free_list_mqtt_parser_data_pool;
	free_list_mqtt_parser_data_pool = mqtt_data;
	EXIT_CRITICAL();
}

static void fifo_clear() {
	fifo_init(&uart_fifo, mem_cpy, uart_fifo_buf, FIFO_BUF_SIZE, sizeof(uint8_t));
	mem_set( uart_fifo_buf, 0, FIFO_BUF_SIZE);
}
