/**
 ******************************************************************************
 * @author: ThanNT
 * @date:   13/08/2016
 ******************************************************************************
**/

/* kernel include */
#include "../ak/ak.h"
#include "../ak/message.h"
#include "../ak/timer.h"
#include "../ak/fsm.h"

/* driver include */
#include "../driver/led/led.h"
#include "../driver/onewire/onewire.h"
#include "../driver/EmonLib/EmonLib.h"

/* app include */
#include "app.h"
#include "app_dbg.h"
#include "app_flash.h"

#include "task_list.h"
#include "task_shell.h"
#include "task_life.h"
#include "task_switch.h"
#include "../lora/task_sx1276.h"

/* sys include */
#include "../sys/sys_irq.h"
#include "../sys/sys_io.h"
#include "../sys/sys_ctrl.h"
#include "../sys/sys_dbg.h"
#include "../sys/sys_arduino.h"

/* common include */
#include "../common/utils.h"

const char* app_version = "1.0.0";

//static void app_power_on_reset();
static void app_start_timer();
static void app_init_state_machine();
static void app_task_init();

/*****************************************************************************/
/* app main function.
 */
/*****************************************************************************/
int main_app() {
	APP_PRINT("main_app() entry OK\n");

	/******************************************************************************
	* init active kernel
	*******************************************************************************/
	ENTRY_CRITICAL();
	task_init();
	task_create(app_task_table);
	EXIT_CRITICAL();

	/******************************************************************************
	* init applications
	*******************************************************************************/
	/*********************
	* hardware configure *
	**********************/
	/* init watch dog timer */
	sys_ctrl_independent_watchdog_init();	/* 32s */
	sys_ctrl_soft_watchdog_init(200);		/* 20s */

	/* spi1 peripheral configure */
	spi1_cfg();

	/* relay init */
	relay_io_cfg();

	/*********************
	* software configure *
	**********************/

	/* sx1276 io init */
	SX1276IoInit();

	/* life led init */
	led_init(&led_life, led_life_init, led_life_on, led_life_off);

	/* increase start time */
	fatal_log_t app_fatal_log;
	eeprom_read(EEPROM_FATAL_LOG_ADDR, (uint8_t*)&app_fatal_log, sizeof(fatal_log_t));
	app_fatal_log.restart_times ++;
	eeprom_write(EEPROM_FATAL_LOG_ADDR, (uint8_t*)&app_fatal_log, sizeof(fatal_log_t));

	/* start timer for application */
	app_init_state_machine();
	app_start_timer();

	/******************************************************************************
	* app task initial
	*******************************************************************************/
	app_task_init();

	/******************************************************************************
	* run applications
	*******************************************************************************/
	return task_run();
}

/*****************************************************************************/
/* app initial function.
 */
/*****************************************************************************/

/* start software timer for application
 * used for app tasks
 */
void app_start_timer() {
	/* start timer to toggle life led */
	timer_set(AC_TASK_LIFE_ID, AC_LIFE_SYSTEM_CHECK, AC_LIFE_TASK_TIMER_LED_LIFE_INTERVAL, TIMER_PERIODIC);
}

/* init state machine for tasks
 * used for app tasks
 */
void app_init_state_machine() {
}

/* send first message to trigger start tasks
 * used for app tasks
 */
void app_task_init() {

	ak_msg_t* msg = get_pure_msg();
	set_msg_sig(msg, LIGHT_SWITCH_INIT);
	task_post(LIGHT_TASK_SWITCH_ID, msg);

}

/*****************************************************************************/
/* app common function
 */
/*****************************************************************************/

/* hardware timer interrupt 10ms
 * used for led, button polling
 */
void sys_irq_timer_10ms() {
	//	button_timer_polling(&btn_mode);
	//	button_timer_polling(&btn_up);
	//	button_timer_polling(&btn_down);
}

/* hardware timer interrupt 50ms
 * used for encode and decode ir
 */
void sys_irq_timer_50us() {
}

/* hardware rtc interrupt alarm
 * used for internal rtc alarm
 */
void sys_irq_timer_hs1101() {
}

/* hardware io interrupt at rev ir pin
 * used for decode ir
 */
void sys_irq_ir_io_rev() {
}

void sys_irq_sx1276ondio_0() {
	SX1276OnDio0Irq();
}

void sys_irq_sx1276ondio_1() {
	SX1276OnDio1Irq();
}

void sys_irq_sx1276ondio_2() {
	SX1276OnDio2Irq();
}

void sys_irq_sx1276ondio_3() {
	SX1276OnDio3Irq();
}

void sys_irq_sx1276ondio_4() {
	SX1276OnDio4Irq();
}

void sys_irq_sx1276ondio_5() {
	SX1276OnDio5Irq();
}


