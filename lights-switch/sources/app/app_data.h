#ifndef __APP_DATA_H__
#define __APP_DATA_H__
#include <stdint.h>
#include "app.h"


/* Define Interface */


/******************************************************************************
* Data type of RF24Network
*******************************************************************************/
#define RF24_DATA_COMMON_MSG_TYPE			(1)
#define RF24_DATA_PURE_MSG_TYPE				(2)
#define RF24_DATA_REMOTE_CMD_TYPE			(3)

/******************************************************************************
* Common define
*******************************************************************************/
#define IF_RETRY_COUNTER_MAX		3

/******************************************************************************
* Commom data structure for transceiver data
*******************************************************************************/
#define FIRMWARE_PSK		0x1A2B3C4D
#define FIRMWARE_LOK		0x1234ABCD

typedef struct {
	uint32_t psk;
	uint32_t bin_len;
	uint16_t checksum;
} firmware_header_t;


#define USE_LORA								1
#define USE_GSM								2

typedef struct {
	uint8_t interface;
} config_interface_t;

/*define for config using LORA*/
#define STREET_LIGHT_APP_ID						0x11111111

#define CONCENTRATOR_ADDR						0x00000000

#define SWITCH_ADDR								0x01000000

#define SENSOR_TYPE								(uint8_t)0
#define CONTROL_TYPE							(uint8_t)1
#define SYNC_TYPE								(uint8_t)2

#define AUTO_MOD								0
#define MANUAL_MOD								1

/*control data*/
typedef struct {
	uint8_t  start_hour;
	uint8_t  start_min;
	uint8_t  end_hour;
	uint8_t  end_min;
} __attribute__((__packed__))set_time_t;

typedef union {
	uint32_t power;
	set_time_t time_set;
} __attribute__((__packed__))mode_data_t;



/*data type*/
typedef struct {
	uint32_t ntc_sen;
	uint32_t curr_sen;
} __attribute__((__packed__))sen_data_t;

typedef struct {
	uint32_t mod;
	mode_data_t mod_data;
} __attribute__((__packed__))ctr_data_t;

typedef struct {
	uint32_t  hour;
	uint32_t  min;
} __attribute__((__packed__))sync_data_t;



/*header*/
typedef struct {
	uint32_t app_id;
	uint32_t src_addr;
	uint32_t des_addr;
	uint8_t type;
}__attribute__((__packed__))lora_hdr_t;

/*data*/
typedef union {
	sen_data_t sen_data;
	ctr_data_t ctr_data;
	sync_data_t sync_data;
} __attribute__((__packed__))data_t;

/*lora package */
typedef struct {
	lora_hdr_t hdr;
	data_t data;
} __attribute__((__packed__))lora_pkg_t;



#endif //__APP_DATA_H__
