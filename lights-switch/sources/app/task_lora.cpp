#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"
#include "../ak/timer.h"
#include "../common/utils.h"
#include "../common/xprintf.h"
#include "../common/fifo.h"

#include "../app/app.h"
#include "../app/app_dbg.h"
#include "../app/task_list.h"

#include "../sys/sys_ctrl.h"
#include "../sys/sys_irq.h"

#include "../platform/stm32l/io_cfg.h"
#include "../platform/stm32l/sx1276_cfg.h"

#include "../lora/radio.h"
#include "../lora/task_sx1276.h"

#include "task_lora.h"


#define MY_RX_TIMEOUT_VALUE                            0

/*!
 * Radio events function pointer
 */

static void on_tx_done( void );
static void on_rx_done( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr );
static void on_tx_timeout( void );
static void on_rx_timeout( void );
static void on_rx_error( void );

static RadioEvents_t radioevents;

void task_lora(ak_msg_t* msg){

	switch (msg->sig) {

	case LIGHT_LORA_INIT:{
		APP_PRINT("LIGHT_LORA_INIT\n\n");

		radioevents.TxDone = on_tx_done;
		radioevents.RxDone = on_rx_done;
		radioevents.TxTimeout = on_tx_timeout;
		radioevents.RxTimeout = on_rx_timeout;
		radioevents.RxError = on_rx_error;
		Radio.Init( &radioevents );
		Radio.SetChannel( RF_FREQUENCY );
		Radio.SetTxConfig( MODEM_LORA, TX_OUTPUT_POWER, 0, LORA_BANDWIDTH,
						   LORA_SPREADING_FACTOR, LORA_CODINGRATE,
						   LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON,
						   true, 0, 0, LORA_IQ_INVERSION_ON, 3000 );
		Radio.SetRxConfig( MODEM_LORA, LORA_BANDWIDTH, LORA_SPREADING_FACTOR,
						   LORA_CODINGRATE, 0, LORA_PREAMBLE_LENGTH,
						   LORA_SYMBOL_TIMEOUT, LORA_FIX_LENGTH_PAYLOAD_ON,
						   0, true, 0, 0, LORA_IQ_INVERSION_ON, true );
		Radio.Rx( MY_RX_TIMEOUT_VALUE );

	}
		break;

	case LIGHT_LORA_RXDONE:{
		APP_DBG("LIGHT_LORA_RXDONE\n\n");

		//		APP_DBG("[%08x:%08x:%08x:%d]\n\n",
		//				r_pkg->hdr.app_id,
		//				r_pkg->hdr.src_addr,
		//				r_pkg->hdr.des_addr,
		//				r_pkg->hdr.type);

		lora_pkg_t r_pkg;
		mem_cpy(&r_pkg, get_data_common_msg(msg), sizeof(lora_pkg_t));

		if(r_pkg.hdr.app_id == STREET_LIGHT_APP_ID){
			if( r_pkg.hdr.des_addr == SWITCH_ADDR){

				if(r_pkg.hdr.type == SENSOR_TYPE){
					msg_inc_ref_count(msg);
					set_msg_sig(msg, LIGHT_SWITCH_STATUS);
					task_post(LIGHT_TASK_SWITCH_ID, msg);
				}
				else if(r_pkg.hdr.type == CONTROL_TYPE){
					msg_inc_ref_count(msg);
					set_msg_sig(msg, LIGHT_SWITCH_CONTROL);
					task_post(LIGHT_TASK_SWITCH_ID, msg);
				}
				else if(r_pkg.hdr.type == SYNC_TYPE){
					msg_inc_ref_count(msg);
					set_msg_sig(msg, LIGHT_SWITCH_SET_TIME);
					task_post(LIGHT_TASK_SWITCH_ID, msg);
				}
			}
		}

		Radio.Rx( MY_RX_TIMEOUT_VALUE );

	}
		break;

	case LIGHT_LORA_TX:{
		APP_DBG("LIGHT_LORA_TX\n\n");

		lora_pkg_t r_pkg;
		mem_cpy(&r_pkg, get_data_common_msg(msg), sizeof(lora_pkg_t));

		Radio.Send((uint8_t*)&r_pkg, sizeof(lora_pkg_t));

	}
		break;

	case LIGHT_LORA_TXDONE:{
		APP_DBG("LIGHT_LORA_TXDONE\n\n");
		Radio.Rx( MY_RX_TIMEOUT_VALUE );
	}
		break;

	case LIGHT_LORA_TXTIMEOUT:{
		APP_DBG("LIGHT_LORA_TXTIMEOUT\n\n");
		Radio.Rx( MY_RX_TIMEOUT_VALUE );
	}
		break;

	case LIGHT_LORA_RXTIMEOUT:{
		APP_DBG("LIGHT_LORA_RXTIMEOUT\n\n");
		Radio.Rx( MY_RX_TIMEOUT_VALUE );
	}
		break;

	case LIGHT_LORA_RXERR:{
		APP_DBG("LIGHT_LORA_RXERR\n\n");
		Radio.Rx( MY_RX_TIMEOUT_VALUE );
	}
		break;

	default: break;
	}
}


void on_tx_done( void )
{
	Radio.Sleep( );
	ak_msg_t* msg = get_pure_msg();
	set_msg_sig(msg, LIGHT_LORA_TXDONE);
	task_post(LIGHT_TASK_LORA_ID, msg);
}

void on_tx_timeout( void )
{
	Radio.Sleep( );
	ak_msg_t* msg = get_pure_msg();
	set_msg_sig(msg, LIGHT_LORA_TXTIMEOUT);
	task_post(LIGHT_TASK_LORA_ID, msg);
}

void on_rx_done( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr )
{
	Radio.Sleep( );

	//APP_DBG("size:%d, rssi:%d, snr:%d\n\n", size, rssi, snr);

	ak_msg_t* smsg = get_common_msg();
	set_data_common_msg(smsg, payload, size);
	set_msg_sig(smsg, LIGHT_LORA_RXDONE);
	task_post(LIGHT_TASK_LORA_ID, smsg);
}

void on_rx_timeout( void )
{
	Radio.Sleep( );
	ak_msg_t* msg = get_pure_msg();
	set_msg_sig(msg, LIGHT_LORA_RXTIMEOUT);
	task_post(LIGHT_TASK_LORA_ID, msg);
}

void on_rx_error( void )
{
	Radio.Sleep( );
	ak_msg_t* msg = get_pure_msg();
	set_msg_sig(msg, LIGHT_LORA_RXERR);
	task_post(LIGHT_TASK_LORA_ID, msg);
}
