/**
 ******************************************************************************
 * @author: ThanNT
 * @date:   13/08/2016
 ******************************************************************************
**/

#ifndef APP_H
#define APP_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "app_eeprom.h"
#include "app_data.h"

/*****************************************************************************/
/*  life task define
 */
/*****************************************************************************/
/* define timer */
#define AC_LIFE_TASK_TIMER_LED_LIFE_INTERVAL		(1000)

/* define signal */
#define AC_LIFE_SYSTEM_CHECK						(0)

/*****************************************************************************/
/*  shell task define
 */
/*****************************************************************************/
/* define timer */

/* define signal */
#define AC_SHELL_LOGIN_CMD							(0)
#define AC_SHELL_REMOTE_CMD							(1)

/*****************************************************************************/
/*  time task define
 */
/*****************************************************************************/
/* define timer */
#define AC_RTC_UPDATE_TIME_INTERVAL					(120000)	/* 2' */
#define AC_RTC_INTINAL_INTERVAL						(100)		/* 100ms */

/* define signal */
#define AC_TIME_RTC_INTINAL							(1)
#define AC_TIME_RTC_UPDATE_STATUS					(2)
#define AC_TIME_STATUS_RES_OK						(3)
#define AC_TIME_STATUS_RES_ERR						(4)
#define AC_TIME_RTC_SETTING_REQ						(5)
#define AC_TIME_AC_SENSOR_TEMPERATURE_STATUS_RES	(6)

/* private define */
#define AC_RTC_STICK_TIME_MIN						(AC_RTC_UPDATE_TIME_INTERVAL / 60000)

/*****************************************************************************/
/* if task define
 */
/*****************************************************************************/
/* define timer */
#define AC_IF_TIMER_PACKET_TIMEOUT_INTERVAL			(500)

/* define signal */
#define AC_IF_PURE_MSG_IN							(1)
#define AC_IF_PURE_MSG_OUT							(2)
#define AC_IF_PURE_MSG_OUT_RES_OK					(3)
#define AC_IF_PURE_MSG_OUT_RES_NG					(4)
#define AC_IF_COMMON_MSG_IN							(5)
#define AC_IF_COMMON_MSG_OUT						(6)
#define AC_IF_COMMON_MSG_OUT_RES_OK					(7)
#define AC_IF_COMMON_MSG_OUT_RES_NG					(8)
#define AC_IF_PACKET_TIMEOUT						(9)

/* sx1276 task define*/
/*signal*/
#define LORA_SX1276_TXTIMEROUT						(0)
#define LORA_SX1276_RXTIMEROUT						(1)
#define LORA_SX1276_RXTIMEROUT_SYNCWORD				(2)

/* task switch define*/
/*timer*/
#define LIGHT_SWITCH_SYNC_TIME_REQ_INTERVAL			(30*60*1000)
#define LIGHT_SWITCH_CHECK_SETTING_TIME_INTERVAL	(10000)
/*signal*/
#define LIGHT_SWITCH_INIT							(1)
#define LIGHT_SWITCH_SYNC_TIME_REQ					(2)
#define LIGHT_SWITCH_CONTROL						(3)
#define LIGHT_SWITCH_STATUS							(4)
#define LIGHT_SWITCH_SET_TIME						(5)
#define LIGHT_SWITCH_CHECK_SETTING_TIMEOUT			(6)

/* task lora define*/
/*timer*/
/*signal*/
#define LIGHT_LORA_INIT								(1)
#define LIGHT_LORA_TXDONE							(2)
#define LIGHT_LORA_TXTIMEOUT						(3)
#define LIGHT_LORA_RXDONE							(4)
#define LIGHT_LORA_RXTIMEOUT						(5)
#define LIGHT_LORA_RXERR							(6)
#define LIGHT_LORA_TX								(7)

/*task sim*/
/*timer*/
#define LIGHT_GSM_COMMON_INTERVAL					(100)
#define LIGHT_GSM_RESPONSE_INTERVAL					(3000)
#define LIGHT_GSM_MQTT_PING_REQ_INTERVAL			(5000)
/*signal*/
#define LIGHT_GSM_INIT								(1)
#define LIGHT_GSM_VERIFY_REQ						(2)
#define LIGHT_GSM_READY_REQ							(3)
#define LIGHT_GSM_TCP_INIT							(4)
#define LIGHT_GSM_TCP_CHECK_STATE					(5)
#define LIGHT_GSM_TCP_STATE_1						(6)
#define LIGHT_GSM_TCP_STATE_2						(7)
#define LIGHT_GSM_TCP_STATE_3						(8)
#define LIGHT_GSM_TCP_STATE_4						(9)
#define LIGHT_GSM_TCP_STATE_5						(10)
#define LIGHT_GSM_TCP_STATE_6						(11)
#define LIGHT_GSM_START_TCP							(12)
#define LIGHT_GSM_TCP_CONNECTED						(13)
#define LIGHT_GSM_SHUT_TCP							(14)
#define LIGHT_GSM_DATA_PROCESS						(15)
#define LIGHT_GSM_MQTT_PING_REQ						(16)
#define LIGHT_GSM_MQTT_PING_REP						(17)
#define LIGHT_GSM_MQTT_PING_TIMEOUT					(18)
#define LIGHT_GSM_PARSER_PAYLOAD					(19)
#define LIGHT_GSM_MQTT_PUB_MSG						(20)



/*********/


/*****************************************************************************/
/*  global define variable
 */
/*****************************************************************************/
#define APP_OK									(0x00)
#define APP_NG									(0x01)

#define APP_FLAG_OFF							(0x00)
#define APP_FLAG_ON								(0x01)

/*****************************************************************************/
/*  app function declare
 */
/*****************************************************************************/
extern const char* app_version;

extern int  main_app();

#ifdef __cplusplus
}
#endif

#endif //APP_H
