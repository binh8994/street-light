#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"
#include "../ak/timer.h"
#include "../common/utils.h"

#include "../app/app.h"
#include "../app/app_dbg.h"
#include "../app/task_list.h"

#include "../sys/sys_ctrl.h"
#include "../platform/stm32l/io_cfg.h"

#include "../driver/ds1302/DS1302.h"

#include "../platform/stm32l/sx1276_cfg.h"
#include "../lora/radio.h"
#include "../lora/task_sx1276.h"

#include "task_switch.h"

static config_interface_t cfg;
DS1302 rtc_ds1302;

void task_switch(ak_msg_t* msg){

	switch (msg->sig) {

	case LIGHT_SWITCH_INIT:{
		APP_PRINT("LIGHT_SWITCH_INIT\n\n");

		eeprom_read(EEPROM_CONFIG_INTERFACE_ADDR, (uint8_t*)&cfg, sizeof(config_interface_t));

		if(cfg.interface == USE_LORA){
			ak_msg_t* msg = get_pure_msg();
			set_msg_sig(msg, LIGHT_LORA_INIT);
			task_post(LIGHT_TASK_LORA_ID, msg);

			/* gateway sync timer */
			timer_set(LIGHT_TASK_SWITCH_ID, LIGHT_SWITCH_SYNC_TIME_REQ, 1000, TIMER_ONE_SHOT);

			/* check setting timer */
			timer_set(LIGHT_TASK_SWITCH_ID, LIGHT_SWITCH_CHECK_SETTING_TIMEOUT, LIGHT_SWITCH_CHECK_SETTING_TIME_INTERVAL, TIMER_PERIODIC);

		}else if(cfg.interface == USE_GSM){

			//ak_msg_t* lmsg = get_pure_msg();
			//set_msg_sig(lmsg, LIGHT_LORA_INIT);
			//task_post(LIGHT_TASK_LORA_ID, lmsg);

			ak_msg_t* gmsg = get_pure_msg();
			set_msg_sig(gmsg, LIGHT_GSM_INIT);
			task_post(LIGHT_TASK_GSM_ID, gmsg);

			/* check setting timer */
			//timer_set(LIGHT_TASK_SWITCH_ID, LIGHT_SWITCH_CHECK_SETTING_TIMEOUT, LIGHT_SWITCH_CHECK_SETTING_TIME_INTERVAL, TIMER_PERIODIC);
		}

	}
		break;

	case LIGHT_SWITCH_SYNC_TIME_REQ:{
		APP_DBG("LIGHT_SWITCH_SYNC_TIME\n\n");

		Time t = rtc_ds1302.time();

		APP_DBG("time.hour: %d\n", t.hr);
		APP_DBG("time.min: %d\n", t.min);
		APP_DBG("time.sec: %d\n\n", t.sec);

		lora_pkg_t s_pkg;
		s_pkg.hdr.app_id = STREET_LIGHT_APP_ID;
		s_pkg.hdr.src_addr = SWITCH_ADDR;
		s_pkg.hdr.des_addr = CONCENTRATOR_ADDR;
		s_pkg.hdr.type = SYNC_TYPE;
		s_pkg.data.sync_data.hour = t.hr;
		s_pkg.data.sync_data.min = t.min;

		ak_msg_t* smsg = get_common_msg();
		set_data_common_msg(smsg, (uint8_t*)&s_pkg, sizeof(lora_pkg_t));
		set_msg_sig(smsg, LIGHT_LORA_TX);
		task_post(LIGHT_TASK_LORA_ID, smsg);

		/* gateway sync timer */
		timer_set(LIGHT_TASK_SWITCH_ID, LIGHT_SWITCH_SYNC_TIME_REQ, LIGHT_SWITCH_SYNC_TIME_REQ_INTERVAL, TIMER_PERIODIC);

	}
		break;

	case LIGHT_SWITCH_SET_TIME:{
		APP_DBG("LIGHT_SWITCH_SET_TIME\n\n");

		lora_pkg_t r_pkg;
		mem_cpy(&r_pkg, get_data_common_msg(msg), sizeof(lora_pkg_t));

		APP_DBG("[%08x:%08x:%08x:%d][%d:%d]\n\n",
				r_pkg.hdr.app_id,
				r_pkg.hdr.src_addr,
				r_pkg.hdr.des_addr,
				r_pkg.hdr.type,
				r_pkg.data.sync_data.hour,
				r_pkg.data.sync_data.min);

		Time t(2099,1,1, (uint8_t)r_pkg.data.sync_data.hour, (uint8_t)r_pkg.data.sync_data.min,0,Time::Day::kMonday);
		rtc_ds1302.time(t);

	}
		break;

	case LIGHT_SWITCH_CHECK_SETTING_TIMEOUT:{
		//APP_DBG("LIGHT_SWITCH_CHECK_SETTING_TIMEOUT\n\n");

		Time now = rtc_ds1302.time();
		APP_DBG("NOW:[%d:%d:%d]\n\n",now.hr, now.min, now.sec);

		ctr_data_t s_ctr;
		eeprom_read(EEPROM_SETTING_CTR_ADDR, (uint8_t*)&s_ctr, sizeof(ctr_data_t));

		if(s_ctr.mod == AUTO_MOD){
			//	APP_DBG("s_ctr.mod=AUTO_MOD\n\n");

			APP_DBG("READ EEP:[%d][%d:%d][%d:%d]\n\n",
					s_ctr.mod,
					s_ctr.mod_data.time_set.start_hour, s_ctr.mod_data.time_set.start_min,
					s_ctr.mod_data.time_set.end_hour, s_ctr.mod_data.time_set.end_min);

			uint32_t s_start_time = s_ctr.mod_data.time_set.start_hour*60 + s_ctr.mod_data.time_set.start_min;
			uint32_t s_end_time = s_ctr.mod_data.time_set.end_hour*60 + s_ctr.mod_data.time_set.end_min;
			uint32_t n_time = now.hr*60 + now.min;

			if(n_time >= s_end_time && n_time < s_start_time){
				if(read_status_relay()){
					relay_off();
					APP_DBG("TURN OFF RELAY\n\n");
				}
				else {
					APP_DBG("RELAY IS TURNED OFF\n\n");
				}
			}
			else{
				if(!read_status_relay()){
					relay_on();
					APP_DBG("TURN ON RELAY\n\n");
				}
				else {
					APP_DBG("RELAY IS TURNED ON\n\n");
				}
			}

		}
		else {
			APP_DBG("READ EEP:[%d][%d]\n\n", s_ctr.mod, s_ctr.mod_data.power);

			if( s_ctr.mod_data.power == 1 ){
				if(!read_status_relay()){
					relay_on();
					APP_DBG("TURN ON RELAY\n\n");
				}
				else {
					APP_DBG("RELAY IS TURNED ON\n\n");
				}
			}
			else if(s_ctr.mod_data.power == 0){
				if(read_status_relay()){
					relay_off();
					APP_DBG("TURN OFF RELAY\n\n");
				}
				else {
					APP_DBG("RELAY IS TURNED OFF\n\n");
				}
			}

		}

	}
		break;

	case LIGHT_SWITCH_STATUS:{
		APP_DBG("LIGHT_SWITCH_STATUS\n\n");

		if(cfg.interface == USE_LORA){
			lora_pkg_t r_pkg;
			mem_cpy(&r_pkg, get_data_common_msg(msg), sizeof(lora_pkg_t));

			//		APP_DBG("[%08x:%08x:%08x:%d][%d:%d]\n\n",
			//				r_pkg.hdr.app_id,
			//				r_pkg.hdr.src_addr,
			//				r_pkg.hdr.des_addr,
			//				r_pkg.hdr.type,
			//				r_pkg.data.sen_data.curr_sen,
			//				r_pkg.data.sen_data.ntc_sen);

			r_pkg.hdr.des_addr = CONCENTRATOR_ADDR;

			ak_msg_t* smsg = get_common_msg();
			set_data_common_msg(smsg, (uint8_t*)&r_pkg, sizeof(lora_pkg_t));
			set_msg_sig(smsg, LIGHT_LORA_TX);
			task_post(LIGHT_TASK_LORA_ID, smsg);

		}
		else if(cfg.interface == USE_GSM){

			lora_pkg_t r_pkg;
			mem_cpy(&r_pkg, get_data_common_msg(msg), sizeof(lora_pkg_t));

			APP_DBG("[%08x:%08x:%08x:%d][%d:%d]\n\n",
					r_pkg.hdr.app_id,
					r_pkg.hdr.src_addr,
					r_pkg.hdr.des_addr,
					r_pkg.hdr.type,
					r_pkg.data.sen_data.curr_sen,
					r_pkg.data.sen_data.ntc_sen);

			r_pkg.hdr.des_addr = CONCENTRATOR_ADDR;

			ak_msg_t* smsg = get_common_msg();
			set_data_common_msg(smsg, (uint8_t*)&r_pkg, sizeof(lora_pkg_t));
			set_msg_sig(smsg, LIGHT_GSM_MQTT_PUB_MSG);
			task_post(LIGHT_TASK_GSM_ID, smsg);
		}

	}
		break;

	case LIGHT_SWITCH_CONTROL:{
		APP_DBG("LIGHT_SWITCH_CONTROL\n\n");

		if(cfg.interface == USE_LORA){

			lora_pkg_t r_pkg;
			mem_cpy(&r_pkg, get_data_common_msg(msg), sizeof(lora_pkg_t));

			APP_DBG("[%08x:%08x:%08x:%d][%d]\n\n",
					r_pkg.hdr.app_id,
					r_pkg.hdr.src_addr,
					r_pkg.hdr.des_addr,
					r_pkg.hdr.type,
					r_pkg.data.ctr_data.mod);

			eeprom_write(EEPROM_SETTING_CTR_ADDR, (uint8_t*)&r_pkg.data.ctr_data.mod, sizeof(ctr_data_t));

			if(r_pkg.data.ctr_data.mod == MANUAL_MOD){
				ctr_data_t r_ctr;
				eeprom_read(EEPROM_SETTING_CTR_ADDR, (uint8_t*)&r_ctr, sizeof(ctr_data_t));
				APP_DBG("Write Done, Read again:[%d][%d]\n\n",
						r_ctr.mod,
						r_ctr.mod_data.power);

				if( r_pkg.data.ctr_data.mod_data.power == 1 ){
					relay_on();
					APP_DBG("TURN ON RELAY\n\n");
				}
				else if(r_pkg.data.ctr_data.mod_data.power == 0){
					relay_off();
					APP_DBG("TURN OFF RELAY\n\n");
				}
			}
			else if(r_pkg.data.ctr_data.mod == AUTO_MOD){
				ctr_data_t r_ctr;
				eeprom_read(EEPROM_SETTING_CTR_ADDR, (uint8_t*)&r_ctr, sizeof(ctr_data_t));

				APP_DBG("Write Done, Read again:[%d][%d:%d][%d:%d]\n\n",
						r_ctr.mod,
						r_ctr.mod_data.time_set.start_hour, r_ctr.mod_data.time_set.start_min,
						r_ctr.mod_data.time_set.end_hour, r_ctr.mod_data.time_set.end_min);
			}

			/*response again*/
			//		r_pkg.hdr.app_id = STREET_LIGHT_APP_ID;
			r_pkg.hdr.src_addr = SWITCH_ADDR;
			r_pkg.hdr.des_addr = CONCENTRATOR_ADDR;
			//		r_pkg.hdr.type = CONTROL_TYPE;

			ak_msg_t* smsg = get_common_msg();
			set_data_common_msg(smsg, (uint8_t*)&r_pkg, sizeof(lora_pkg_t));
			set_msg_sig(smsg, LIGHT_LORA_TX);
			task_post(LIGHT_TASK_LORA_ID, smsg);
		}
		else if(cfg.interface == USE_GSM){

			ctr_data_t r_ctr;
			uint8_t* rmsg = get_data_common_msg(msg);
			mem_cpy(&r_ctr, rmsg, sizeof(ctr_data_t));

			eeprom_write(EEPROM_SETTING_CTR_ADDR, (uint8_t*)&r_ctr, sizeof(ctr_data_t));

			if(r_ctr.mod == MANUAL_MOD){

				ctr_data_t read_ctr;
				eeprom_read(EEPROM_SETTING_CTR_ADDR, (uint8_t*)&read_ctr, sizeof(ctr_data_t));
				APP_DBG("Write Done, Read again:[%d][%d]\n\n",
						read_ctr.mod, read_ctr.mod_data.power);

				if( read_ctr.mod_data.power == 1 ){
					relay_on();
					APP_DBG("TURN ON RELAY\n\n");
				}
				else if(read_ctr.mod_data.power == 0){
					relay_off();
					APP_DBG("TURN OFF RELAY\n\n");
				}
			}
			else if(r_ctr.mod == AUTO_MOD){

				ctr_data_t read_ctr;
				eeprom_read(EEPROM_SETTING_CTR_ADDR, (uint8_t*)&read_ctr, sizeof(ctr_data_t));
				APP_DBG("Write Done, Read again:[%d][%d:%d][%d:%d]\n\n",
						r_ctr.mod,
						r_ctr.mod_data.time_set.start_hour, r_ctr.mod_data.time_set.start_min,
						r_ctr.mod_data.time_set.end_hour, r_ctr.mod_data.time_set.end_min);
			}
		}

	}
		break;

	default:
		break;
	}

}


