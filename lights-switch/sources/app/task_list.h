#ifndef __TASK_LIST_H__
#define __TASK_LIST_H__

#include "../ak/ak.h"
#include "../ak/task.h"

extern task_t app_task_table[];

/*****************************************************************************/
/*  DECLARE: Internal Task ID
 *  Note: Task id MUST be increasing order.
 */
/*****************************************************************************/
/**
  * SYSTEM TASKS
  **************/
#define TASK_TIMER_TICK_ID				0

/**
  * APP TASKS
  **************/
#define AC_TASK_SHELL_ID				1
#define AC_TASK_LIFE_ID					2
#define LIGHT_TASK_SX1276_ID			3
#define LIGHT_TASK_SWITCH_ID			4
#define LIGHT_TASK_GSM_ID				5
#define LIGHT_TASK_LORA_ID				6
/**
  * EOT task ID
  **************/
#define AK_TASK_EOT_ID					7

/*****************************************************************************/
/*  DECLARE: Task entry point
 */
/*****************************************************************************/
extern void task_shell(ak_msg_t*);
extern void task_life(ak_msg_t*);
extern void task_sx1276(ak_msg_t*);
extern void task_switch(ak_msg_t*);
extern void task_gsm(ak_msg_t*);
extern void task_lora(ak_msg_t*);
#endif //__TASK_LIST_H__
