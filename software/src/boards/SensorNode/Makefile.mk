CFLAGS += -I./src/boards/SensorNode
VPATH += src/boards/SensorNode
SOURCES += src/boards/SensorNode/adc-board.c
SOURCES += src/boards/SensorNode/board.c
SOURCES += src/boards/SensorNode/eeprom-board.c
SOURCES += src/boards/SensorNode/gpio-board.c
SOURCES += src/boards/SensorNode/gps-board.c
SOURCES += src/boards/SensorNode/i2c-board.c
SOURCES += src/boards/SensorNode/rtc-board.c
SOURCES += src/boards/SensorNode/spi-board.c
SOURCES += src/boards/SensorNode/sx1276-board.c
SOURCES += src/boards/SensorNode/uart-board.c
#SOURCES += src/boards/SensorNode/uart-usb-board.c

#
CFLAGS += -I./src/boards/SensorNode/cmsis
CFLAGS += -I./src/boards/SensorNode/cmsis/arm-gcc
#CFLAGS += -I./src/boards/SensorNode/cmsis/arm-std
VPATH += src/boards/SensorNode/cmsis
SOURCES += src/boards/SensorNode/cmsis/system_stm32l1xx.c

#
CFLAGS += -I./src/boards/SensorNode/usb/cdc/inc
VPATH += src/boards/SensorNode/usb/cdc/src
SOURCES += src/boards/SensorNode/usb/cdc/src/usbd_cdc_if.c
SOURCES += src/boards/SensorNode/usb/cdc/src/usbd_conf.c
SOURCES += src/boards/SensorNode/usb/cdc/src/usbd_desc.c

#
#CFLAGS += -I./src/boards/SensorNode/usb/dfu/inc
#VPATH += src/boards/SensorNode/usb/dfu/src
#SOURCES += src/boards/SensorNode/usb/dfu/src/usbd_conf.c
#SOURCES += src/boards/SensorNode/usb/dfu/src/usbd_desc.c
#SOURCES += src/boards/SensorNode/usb/dfu/src/usbd_dfu_flash.c

