CFLAGS += -I./src/boards/mcu/stm32
VPATH += src/boards/mcu/stm32
SOURCES += src/boards/mcu/stm32/sysIrqHandlers.c
SOURCES += src/boards/mcu/stm32/utilities.c

CFLAGS += -I./src/boards/mcu/stm32/cmsis

CFLAGS += -I./src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Inc
CFLAGS += -I./src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Inc/Legacy
VPATH += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_adc.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_adc_ex.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_comp.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_cortex.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_crc.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_cryp.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_cryp_ex.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_dac.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_dac_ex.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_dma.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_flash.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_flash_ex.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_flash_ramfunc.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_gpio.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_i2c.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_i2s.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_irda.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_iwdg.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_lcd.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_msp_template.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_nor.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_opamp.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_opamp_ex.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_pcd.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_pcd_ex.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_pwr.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_pwr_ex.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_rcc.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_rcc_ex.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_rtc.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_rtc_ex.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_sd.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_smartcard.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_spi.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_spi_ex.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_sram.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_tim.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_tim_ex.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_uart.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_usart.c
SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_wwdg.c
#SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_ll_adc.c
#SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_ll_comp.c
#SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_ll_crc.c
#SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_ll_dac.c
#SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_ll_dma.c
#SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_ll_exti.c
#SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_ll_fsmc.c
#SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_ll_gpio.c
#SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_ll_i2c.c
#SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_ll_opamp.c
#SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_ll_pwr.c
#SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_ll_rcc.c
#SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_ll_rtc.c
#SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_ll_sdmmc.c
#SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_ll_spi.c
#SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_ll_tim.c
#SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_ll_usart.c
#SOURCES += src/boards/mcu/stm32/STM32L1xx_HAL_Driver/Src/stm32l1xx_ll_utils.c


CFLAGS += -I./src/boards/mcu/stm32/STM32_USB_Device_Library/Class/CDC/Inc
#CFLAGS += -I./src/boards/mcu/stm32/STM32_USB_Device_Library/Class/DFU/Inc
CFLAGS += -I./src/boards/mcu/stm32/STM32_USB_Device_Library/Core/Inc
#SOURCES += src/boards/mcu/stm32/STM32_USB_Device_Library/Class/CDC/Src/usbd_cdc.c
#SOURCES += src/boards/mcu/stm32/STM32_USB_Device_Library/Class/DFU/Src/usbd_dfu.c
#SOURCES += src/boards/mcu/stm32/STM32_USB_Device_Library/Core/Src/usbd_core.c
#SOURCES += src/boards/mcu/stm32/STM32_USB_Device_Library/Core/Src/usbd_ctlreq.c
#SOURCES += src/boards/mcu/stm32/STM32_USB_Device_Library/Core/Src/usbd_ioreq.c
