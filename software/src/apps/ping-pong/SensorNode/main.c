/*q
 / _____)             _              | |
( (____  _____ ____ _| |_ _____  ____| |__
 \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 _____) ) ____| | | || |_| ____( (___| | | |
(______/|_____)_|_|_| \__)_____)\____)_| |_|
	(C)2013 Semtech

Description: Ping-Pong implementation

License: Revised BSD License, see LICENSE.TXT file include in the project

Maintainer: Miguel Luis and Gregory Cristian
*/
#include <string.h>
#include "board.h"
#include "radio.h"
#include "xprintf.h"
#include "utils.h"

#if defined( USE_BAND_433 )

#define RF_FREQUENCY                                434000000 // Hz

#elif defined( USE_BAND_780 )

#define RF_FREQUENCY                                780000000 // Hz

#elif defined( USE_BAND_868 )

#define RF_FREQUENCY                                868000000 // Hz

#elif defined( USE_BAND_915 )

#define RF_FREQUENCY                                915000000 // Hz

#else
#error "Please define a frequency band in the compiler options."
#endif

#define TX_OUTPUT_POWER                             14        // dBm

//#if defined( USE_MODEM_LORA )

#define LORA_BANDWIDTH                              0         // [0: 125 kHz,
//  1: 250 kHz,
//  2: 500 kHz,
//  3: Reserved]
#define LORA_SPREADING_FACTOR                       7         // [SF7..SF12]
#define LORA_CODINGRATE                             1         // [1: 4/5,
//  2: 4/6,
//  3: 4/7,
//  4: 4/8]
#define LORA_PREAMBLE_LENGTH                        8         // Same for Tx and Rx
#define LORA_SYMBOL_TIMEOUT                         5         // Symbols
#define LORA_FIX_LENGTH_PAYLOAD_ON                  false
#define LORA_IQ_INVERSION_ON                        false

/*#elif defined( USE_MODEM_FSK )

#define FSK_FDEV                                    25e3      // Hz
#define FSK_DATARATE                                50e3      // bps
#define FSK_BANDWIDTH                               50e3      // Hz
#define FSK_AFC_BANDWIDTH                           83.333e3  // Hz
#define FSK_PREAMBLE_LENGTH                         5         // Same for Tx and Rx
#define FSK_FIX_LENGTH_PAYLOAD_ON                   false

#else
	#error "Please define a modem in the compiler options."
#endif
*/
typedef enum
{
	LOWPOWER,
	RX,
	RX_TIMEOUT,
	RX_ERROR,
	TX,
	TX_TIMEOUT,
}States_t;

#define RX_TIMEOUT_VALUE                            2000
#define BUFFER_SIZE                                 64 // Define the payload size here

uint8_t PingMsg[] = "PING";
uint8_t PongMsg[] = "PONG";

uint16_t BufferSize = BUFFER_SIZE;
uint8_t Buffer[BUFFER_SIZE];

States_t State = LOWPOWER;

int8_t RssiValue = 0;
int8_t SnrValue = 0;

bool isMaster = !false;

/*=====BINHNT========*/
Gpio_t STM_B15;
#define FIFO_UARTRX_SIZE														64
#define FIFO_UARTTX_SIZE														64
#define DBG(fmt, ...)       xprintf((const char*)fmt, ##__VA_ARGS__)
uint8_t UartRxBuffer[FIFO_UARTRX_SIZE]; 
uint8_t UartTxBuffer[FIFO_UARTRX_SIZE]; 
static TimerEvent_t LedTimer;

void _init() {	/* dummy */}

void UartPut1( uint8_t c )
{
	UartPutChar(&Uart1, c);
}

void OnLedTimerEvent( void )
{
	static uint8_t ledState = 1;
	ledState = !ledState;
	GpioWrite( &STM_B15, ledState );
	TimerStart( &LedTimer );
}

//void UartMcuIrqNotify( UartNotifyId_t id )
//{
//	    uint8_t buffer[64];
//			uint16_t len;
//	    if( id == UART_NOTIFY_RX )
//	    {
//	        if( UartGetBuffer( &Uart1, buffer, 64, &len ) == 0 )
//	        {
//						UartPutBuffer(&Uart1, buffer, len);
//	        }
//	    }
//}
/*=====BINHNT========*/



/*!
 * Radio events function pointer
 */
static RadioEvents_t RadioEvents;

/*!
 * \brief Function to be executed on Radio Tx Done event
 */
void OnTxDone( void );

/*!
 * \brief Function to be executed on Radio Rx Done event
 */
void OnRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr );

/*!
 * \brief Function executed on Radio Tx Timeout event
 */
void OnTxTimeout( void );

/*!
 * \brief Function executed on Radio Rx Timeout event
 */
void OnRxTimeout( void );

/*!
 * \brief Function executed on Radio Rx Error event
 */
void OnRxError( void );

/** 
 * Main application entry point.
 */



int main( void )
{
	BoardInitMcu( );
	// BoardInitPeriph( );

	UartDeInit(&Uart1);
	UartInit(&Uart1, UART_1, UART_TX, UART_RX);
	UartConfig( &Uart1, RX_TX, 115200, UART_8_BIT, UART_1_STOP_BIT, NO_PARITY, NO_FLOW_CTRL );
	FifoInit( &Uart1.FifoRx, UartRxBuffer, FIFO_UARTRX_SIZE );
	FifoInit( &Uart1.FifoTx, UartTxBuffer, FIFO_UARTTX_SIZE );
	//Uart1.IrqNotify = UartMcuIrqNotify;
	xfunc_out = UartPut1;
	DBG("# Board Init\n");

	TimerInit( &LedTimer, OnLedTimerEvent );
	TimerSetValue( &LedTimer, 1000 );
	TimerStart( &LedTimer );
	GpioInit( &STM_B15, PB_15, PIN_OUTPUT, PIN_PUSH_PULL, PIN_NO_PULL, 0 );

	// Radio initialization
	RadioEvents.TxDone = OnTxDone;
	RadioEvents.RxDone = OnRxDone;
	RadioEvents.TxTimeout = OnTxTimeout;
	RadioEvents.RxTimeout = OnRxTimeout;
	RadioEvents.RxError = OnRxError;

	Radio.Init( &RadioEvents );

	Radio.SetChannel( RF_FREQUENCY );

	/*#if defined( USE_MODEM_LORA )*/

	Radio.SetTxConfig( MODEM_LORA, TX_OUTPUT_POWER, 0, LORA_BANDWIDTH,
					   LORA_SPREADING_FACTOR, LORA_CODINGRATE,
					   LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON,
					   true, 0, 0, LORA_IQ_INVERSION_ON, 3000 );

	Radio.SetRxConfig( MODEM_LORA, LORA_BANDWIDTH, LORA_SPREADING_FACTOR,
					   LORA_CODINGRATE, 0, LORA_PREAMBLE_LENGTH,
					   LORA_SYMBOL_TIMEOUT, LORA_FIX_LENGTH_PAYLOAD_ON,
					   0, true, 0, 0, LORA_IQ_INVERSION_ON, true );

	/*#elif defined( USE_MODEM_FSK )

	Radio.SetTxConfig( MODEM_FSK, TX_OUTPUT_POWER, FSK_FDEV, 0,
								  FSK_DATARATE, 0,
								  FSK_PREAMBLE_LENGTH, FSK_FIX_LENGTH_PAYLOAD_ON,
								  true, 0, 0, 0, 3000 );

	Radio.SetRxConfig( MODEM_FSK, FSK_BANDWIDTH, FSK_DATARATE,
								  0, FSK_AFC_BANDWIDTH, FSK_PREAMBLE_LENGTH,
								  0, FSK_FIX_LENGTH_PAYLOAD_ON, 0, true,
								  0, 0,false, true );

	#else
		#error "Please define a frequency band in the compiler options."
	#endif
	*/

	if(isMaster)
		Radio.Send( PingMsg, 4 );
	else
		Radio.Rx( RX_TIMEOUT_VALUE );
	while( 1 )
	{
		switch( State )
		{
		case RX:
			DBG("RXDone %d:%s\n", BufferSize, Buffer);
			DelayMs( 1 );
			if(isMaster){
				//DBG("Send\n");
				Radio.SetTxConfig( MODEM_LORA, TX_OUTPUT_POWER, 0, LORA_BANDWIDTH,
								   LORA_SPREADING_FACTOR, LORA_CODINGRATE,
								   LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON,
								   true, 0, 0, LORA_IQ_INVERSION_ON, 3000 );
				Radio.Send( PingMsg, 4 );
			}
			else{
				Radio.Rx( RX_TIMEOUT_VALUE );
			}
			State = LOWPOWER;
			break;
		case TX:
			DBG("TXDone\n");
			DelayMs( 1 );
			if(isMaster){
				//DBG("Send\n");
				Radio.SetTxConfig( MODEM_LORA, TX_OUTPUT_POWER, 0, LORA_BANDWIDTH,
								   LORA_SPREADING_FACTOR, LORA_CODINGRATE,
								   LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON,
								   true, 0, 0, LORA_IQ_INVERSION_ON, 3000 );
				Radio.Send( PingMsg, 4 );
			}
			else{
				Radio.Rx( RX_TIMEOUT_VALUE );
			}
			State = LOWPOWER;
			break;
		case RX_TIMEOUT:
			DBG("RXTimeout\n");
			DelayMs( 1 );
			if(isMaster){
				//DBG("Send\n");
				Radio.SetTxConfig( MODEM_LORA, TX_OUTPUT_POWER, 0, LORA_BANDWIDTH,
								   LORA_SPREADING_FACTOR, LORA_CODINGRATE,
								   LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON,
								   true, 0, 0, LORA_IQ_INVERSION_ON, 3000 );
				Radio.Send( PingMsg, 4 );
			}
			else{
				Radio.Rx( RX_TIMEOUT_VALUE );
			}
			State = LOWPOWER;
			break;
		case RX_ERROR:
			DBG("RXTimeout\n");
			DelayMs( 1 );
			if(isMaster){
				//DBG("Send\n");
				Radio.SetTxConfig( MODEM_LORA, TX_OUTPUT_POWER, 0, LORA_BANDWIDTH,
								   LORA_SPREADING_FACTOR, LORA_CODINGRATE,
								   LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON,
								   true, 0, 0, LORA_IQ_INVERSION_ON, 3000 );
				Radio.Send( PingMsg, 4 );
			}
			else{
				Radio.Rx( RX_TIMEOUT_VALUE );
			}
			State = LOWPOWER;
			break;
		case TX_TIMEOUT:
			DBG("TXTimeout\n");
			DelayMs( 1 );
			if(isMaster){
				//DBG("Send\n");
				Radio.SetTxConfig( MODEM_LORA, TX_OUTPUT_POWER, 0, LORA_BANDWIDTH,
								   LORA_SPREADING_FACTOR, LORA_CODINGRATE,
								   LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON,
								   true, 0, 0, LORA_IQ_INVERSION_ON, 3000 );
				Radio.Send( PingMsg, 4 );
			}
			else{
				Radio.Rx( RX_TIMEOUT_VALUE );
			}
			State = LOWPOWER;
			break;
		case LOWPOWER:
		default:
			// Set low power
			break;
		}

		TimerLowPowerHandler( );
	}

}

void OnTxDone( void )
{
	Radio.Sleep( );
	State = TX;
}

void OnRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr )
{
	Radio.Sleep( );
	BufferSize = size;
	memcpy( Buffer, payload, BufferSize );
	RssiValue = rssi;
	SnrValue = snr;
	State = RX;
}

void OnTxTimeout( void )
{
	Radio.Sleep( );
	State = TX_TIMEOUT;
}

void OnRxTimeout( void )
{
	Radio.Sleep( );
	State = RX_TIMEOUT;
}

void OnRxError( void )
{
	Radio.Sleep( );
	State = RX_ERROR;
}
