CFLAGS += -I./src/system
CFLAGS += -I./src/system/crypto

VPATH += src/system
VPATH += src/system/crypto

SOURCES += src/system/adc.c
SOURCES += src/system/delay.c
SOURCES += src/system/eeprom.c
SOURCES += src/system/fifo.c
SOURCES += src/system/gpio.c
SOURCES += src/system/gps.c
SOURCES += src/system/i2c.c
SOURCES += src/system/timer.c
SOURCES += src/system/uart.c
SOURCES += src/system/crypto/aes.c
SOURCES += src/system/crypto/cmac.c
