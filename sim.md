In order to perform a request, the library follows these steps:

Configure Bearer:

sim AT+CREG?
 -> try until 0,1 (connected to the network)

sim AT+SAPBR=3,1,"Contype","GPRS"
 -> wait for OK

sim AT+SAPBR=3,1,"APN","v-internet"
 -> wait for OK

sim AT+SAPBR=1,1
 -> wait for OK


HTTP GET:

sim AT+HTTPINIT
 -> wait for OK

sim AT+HTTPPARA="CID",1
 -> wait for OK

sim AT+HTTPPARA="URL","http://sim.comxa.com/sim.php"
-> wait for OK

sim AT+HTTPSSL=0
 -> wait for OK (1 when URL starts with "https://")

sim AT+HTTPACTION=0
 -> wait for 200

sim AT+HTTPREAD
 -> read buffer and parse it

sim AT+HTTPTERM
 -> wait for OK

sim AT+SAPBR=0,1

HTTP POST:

sim AT+HTTPINIT
 -> wait for OK

sim AT+HTTPPARA="CID",1
 -> wait for OK

sim AT+HTTPPARA="URL","http://sim.comxa.com/sim.php"
 -> wait for OK


sim AT+HTTPPARA="CONTENT","application/json"
 -> wait for OK

sim AT+HTTPDATA=34,10000
 -> wait for DOWNLOAD, then write the body and wait 10000

sim {"username":"usr","password":"pw"}

sim AT+HTTPSSL=0
 -> wait for OK (1 when URL starts with "https://")

sim AT+HTTPACTION=1
 -> wait for ,200

sim AT+HTTPREAD
 -> read buffer and parse it

sim AT+HTTPTERM
 -> wait for OK

sim AT+SAPBR=0,1



