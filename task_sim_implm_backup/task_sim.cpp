
#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"
#include "../ak/timer.h"
#include "../common/utils.h"
#include "../common/xprintf.h"

#include "../app/app.h"
#include "../app/app_dbg.h"
#include "../app/task_list.h"

#include "../sys/sys_ctrl.h"
#include "../sys/sys_irq.h"
#include "../platform/stm32l/io_cfg.h"

#include "../driver/ds1302/DS1302.h"
#include "task_sim.h"

#define RECV_BUFFER_SIZE			64

typedef struct {
	uint8_t data_index;
	uint8_t data[RECV_BUFFER_SIZE];
} recv_data_t;

recv_data_t recv_data;

void clear_data(recv_data_t * dt);

void task_sim(ak_msg_t* msg){

	switch (msg->sig) {

	case LIGHT_SWITCH_SIM_INIT:{
		APP_DBG("LIGHT_SWITCH_SIM_INIT\n");
		sim_cfg();


		clear_data(&recv_data);
		sim_puts((uint8_t*)"AT+CREG?\r\n");
		timer_set(LIGHT_TASK_SIM_ID, SIM_READY, SIM_COMMON_INTERVAL, TIMER_ONE_SHOT);
	}
		break;

	case SIM_READY:{
		APP_DBG("SIM_READY\n");

		if( str_str(recv_data.data, (uint8_t*)"0,1") ){
			clear_data(&recv_data);
			sim_puts((uint8_t*)"AT+SAPBR=3,1,\"Contype\",\"GPRS\"\r\n");
			timer_set(LIGHT_TASK_SIM_ID, SIM_SET_GPRS, SIM_COMMON_INTERVAL, TIMER_ONE_SHOT);
		}
		else{
			clear_data(&recv_data);
			sim_puts((uint8_t*)"AT+CREG?\r\n");
			timer_set(LIGHT_TASK_SIM_ID, SIM_READY, SIM_COMMON_INTERVAL, TIMER_ONE_SHOT);
		}
	}
		break;

	case SIM_SET_GPRS:{
		APP_DBG("SIM_SET_GPRS\n");

		if( str_str(recv_data.data, (uint8_t*)"OK") ){
			clear_data(&recv_data);
			sim_puts((uint8_t*)"AT+SAPBR=3,1,\"APN\",\"v-internet\"\r\n");
			timer_set(LIGHT_TASK_SIM_ID, SIM_SET_APN, SIM_COMMON_INTERVAL, TIMER_ONE_SHOT);
		}
		else{
			clear_data(&recv_data);
			sim_puts((uint8_t*)"AT+CREG?\r\n");
			timer_set(LIGHT_TASK_SIM_ID, SIM_READY, SIM_COMMON_INTERVAL, TIMER_ONE_SHOT);
		}
	}
		break;

	case SIM_SET_APN:{
		APP_DBG("SIM_SET_APN\n");

		if( str_str(recv_data.data, (uint8_t*)"OK") ){
			clear_data(&recv_data);
			sim_puts((uint8_t*)"AT+SAPBR=1,1\r\n");
			timer_set(LIGHT_TASK_SIM_ID, SIM_SET_ON_BEARER, SIM_SET_BEARER_INTERVAL, TIMER_ONE_SHOT);
		}
		else{
			clear_data(&recv_data);
			sim_puts((uint8_t*)"AT+CREG?\r\n");
			timer_set(LIGHT_TASK_SIM_ID, SIM_READY, SIM_COMMON_INTERVAL, TIMER_ONE_SHOT);
		}
	}
		break;

	case SIM_SET_ON_BEARER:{
		APP_DBG("SIM_SET_ON_BEARER\n");

		if( str_str(recv_data.data, (uint8_t*)"OK") ){
			clear_data(&recv_data);
			sim_puts((uint8_t*)"AT+HTTPINIT\r\n");
			timer_set(LIGHT_TASK_SIM_ID, SIM_HTTP_INIT, SIM_COMMON_INTERVAL, TIMER_ONE_SHOT);
		}
		else{
			//clear_data(&recv_data);
			//sim_puts((uint8_t*)"AT+SAPBR=1,1\r\n");
			//timer_set(LIGHT_TASK_SIM_ID, SIM_SET_ON_BEARER, SIM_SET_BEARER_INTERVAL, TIMER_ONE_SHOT);
		}
	}
		break;

	case SIM_HTTP_INIT:{
		APP_DBG("SIM_HTTP_INIT\n");

		if( str_str(recv_data.data, (uint8_t*)"OK") ){
			clear_data(&recv_data);
			sim_puts((uint8_t*)"AT+HTTPPARA=\"CID\",1\r\n");
			timer_set(LIGHT_TASK_SIM_ID, SIM_HTTP_SET_CID, SIM_COMMON_INTERVAL, TIMER_ONE_SHOT);
		}
		else{
			//clear_data(&recv_data);
			//sim_puts((uint8_t*)"AT+HTTPINIT\r\n");
			//timer_set(LIGHT_TASK_SIM_ID, SIM_HTTP_INIT, SIM_COMMON_INTERVAL, TIMER_ONE_SHOT);
		}
	}
		break;

	case SIM_HTTP_SET_CID:{
		APP_DBG("SIM_HTTP_SET_CID\n");

		if( str_str(recv_data.data, (uint8_t*)"OK") ){
			clear_data(&recv_data);
			sim_puts((uint8_t*)"AT+HTTPPARA=\"URL\",\"http://sim.comxa.com/sim.php\"\r\n");
			timer_set(LIGHT_TASK_SIM_ID, SIM_HTTP_SET_URL, SIM_COMMON_INTERVAL, TIMER_ONE_SHOT);
		}
		else{
			//clear_data(&recv_data);
			//sim_puts((uint8_t*)"AT+HTTPPARA=\"CID\",1\r\n");
			//timer_set(LIGHT_TASK_SIM_ID, SIM_HTTP_SET_CID, SIM_COMMON_INTERVAL, TIMER_ONE_SHOT);
		}
	}
		break;

	case SIM_HTTP_SET_URL:{
		APP_DBG("SIM_HTTP_SET_URL\n");

		if( str_str(recv_data.data, (uint8_t*)"OK") ){
			clear_data(&recv_data);
			sim_puts((uint8_t*)"AT+HTTPPARA=\"CONTENT\",\"application/json\"\r\n");
			timer_set(LIGHT_TASK_SIM_ID, SIM_HTTP_SET_CONTENT, SIM_COMMON_INTERVAL, TIMER_ONE_SHOT);
		}
		else{
			//clear_data(&recv_data);
			//sim_puts((uint8_t*)"AT+HTTPPARA=\"URL\",\"http://sim.comxa.com/sim.php\"\r\n");
			//timer_set(LIGHT_TASK_SIM_ID, SIM_HTTP_SET_URL, SIM_COMMON_INTERVAL, TIMER_ONE_SHOT);
		}

	}
		break;

	case SIM_HTTP_SET_CONTENT:{
		APP_DBG("SIM_HTTP_SET_CONTENT\n");

		if( str_str(recv_data.data, (uint8_t*)"OK") ){
			clear_data(&recv_data);
			sim_puts((uint8_t*)"AT+HTTPDATA=30,10000\r\n");
			timer_set(LIGHT_TASK_SIM_ID, SIM_HTTP_SET_DATA_LEN, SIM_SET_DATA_LEN_INTERVAL, TIMER_ONE_SHOT);
		}
		else{
			//clear_data(&recv_data);
			//sim_puts((uint8_t*)"AT+HTTPPARA=\"CONTENT\",\"application/json\"\r\n");
			//timer_set(LIGHT_TASK_SIM_ID, SIM_HTTP_SET_CONTENT, SIM_COMMON_INTERVAL, TIMER_ONE_SHOT);
		}

	}
		break;

	case SIM_HTTP_SET_DATA_LEN:{
		APP_DBG("SIM_HTTP_SET_DATA_LEN\n");

		if( str_str(recv_data.data, (uint8_t*)"DOWNLOAD") ){
			clear_data(&recv_data);
			sim_puts((uint8_t*)"{\"username\":usr,\"password\":pw}\r\n");
			timer_set(LIGHT_TASK_SIM_ID, SIM_HTTP_SET_DATA, SIM_SET_DATA_INTERVAL, TIMER_ONE_SHOT);
		}
		else{
			//clear_data(&recv_data);
			//sim_puts((uint8_t*)"AT+HTTPDATA=30,10000\r\n");
			//timer_set(LIGHT_TASK_SIM_ID, SIM_HTTP_SET_DATA_LEN, SIM_SET_DATA_LEN_INTERVAL, TIMER_ONE_SHOT);
		}
	}
		break;

	case SIM_HTTP_SET_DATA:{
		APP_DBG("SIM_HTTP_SET_DATA\n");

		if( str_str(recv_data.data, (uint8_t*)"OK") ){
			clear_data(&recv_data);
			sim_puts((uint8_t*)"AT+HTTPSSL=0\r\n");
			timer_set(LIGHT_TASK_SIM_ID, SIM_HTTP_SET_SSL, SIM_COMMON_INTERVAL, TIMER_ONE_SHOT);
		}
		else{
			clear_data(&recv_data);
			sim_puts((uint8_t*)"{\"username\":usr,\"password\":pw}\r\n");
			timer_set(LIGHT_TASK_SIM_ID, SIM_HTTP_SET_DATA, SIM_SET_DATA_INTERVAL, TIMER_ONE_SHOT);
		}
	}
		break;

	case SIM_HTTP_SET_SSL:{
		APP_DBG("SIM_HTTP_SET_SSL\n");

		if( str_str(recv_data.data, (uint8_t*)"OK") ){
			clear_data(&recv_data);
			sim_puts((uint8_t*)"AT+HTTPACTION=1\r\n");
			timer_set(LIGHT_TASK_SIM_ID, SIM_HTTP_ACTION, SIM_HTTP_ACTION_INTERVAL, TIMER_ONE_SHOT);
		}
		else{
			//clear_data(&recv_data);
			//sim_puts((uint8_t*)"AT+HTTPSSL=0\r\n");
			//timer_set(LIGHT_TASK_SIM_ID, SIM_HTTP_SET_SSL, SIM_COMMON_INTERVAL, TIMER_ONE_SHOT);
		}
	}
		break;

	case SIM_HTTP_ACTION:{
		APP_DBG("SIM_HTTP_ACTION\n");

		if( str_str(recv_data.data, (uint8_t*)"HTTPACTION") ){
			clear_data(&recv_data);
			sim_puts((uint8_t*)"AT+HTTPREAD\r\n");
			timer_set(LIGHT_TASK_SIM_ID, SIM_HTTP_READ, SIM_COMMON_INTERVAL, TIMER_ONE_SHOT);
		}
		else{
			//clear_data(&recv_data);
			//sim_puts((uint8_t*)"AT+HTTPACTION=1\r\n");
			//timer_set(LIGHT_TASK_SIM_ID, SIM_HTTP_ACTION, SIM_HTTP_ACTION_INTERVAL, TIMER_ONE_SHOT);
		}
	}
		break;

	case SIM_HTTP_READ:{
		APP_DBG("SIM_HTTP_READ\n");

		if( str_str(recv_data.data, (uint8_t*)"OK") ){
			clear_data(&recv_data);
			sim_puts((uint8_t*)"AT+HTTPTERM\r\n");
			timer_set(LIGHT_TASK_SIM_ID, SIM_HTTP_TERMINATE, SIM_COMMON_INTERVAL, TIMER_ONE_SHOT);
		}
		else{
			//clear_data(&recv_data);
			//sim_puts((uint8_t*)"AT+HTTPREAD\r\n");
			//timer_set(LIGHT_TASK_SIM_ID, SIM_HTTP_READ, SIM_COMMON_INTERVAL, TIMER_ONE_SHOT);
		}
	}
		break;

	case SIM_HTTP_TERMINATE:{
		APP_DBG("SIM_HTTP_TERMINATE\n");

		if( str_str(recv_data.data, (uint8_t*)"OK") ){
			clear_data(&recv_data);
			sim_puts((uint8_t*)"AT+SAPBR=0,1\r\n");
			timer_set(LIGHT_TASK_SIM_ID, SIM_SET_OFF_BEARER, SIM_SET_BEARER_INTERVAL, TIMER_ONE_SHOT);
		}
		else{
			//clear_data(&recv_data);
			//sim_puts((uint8_t*)"AT+HTTPTERM\r\n");
			//timer_set(LIGHT_TASK_SIM_ID, SIM_HTTP_TERMINATE, SIM_COMMON_INTERVAL, TIMER_ONE_SHOT);
		}
	}
		break;

	case SIM_SET_OFF_BEARER:{
		APP_DBG("SIM_SET_OFF_BEARER\n");

		if( str_str(recv_data.data, (uint8_t*)"OK") ){
			APP_DBG("\nDONE!\n");
		}
		else{
			clear_data(&recv_data);
			sim_puts((uint8_t*)"AT+SAPBR=0,1\r\n");
			timer_set(LIGHT_TASK_SIM_ID, SIM_SET_OFF_BEARER, SIM_SET_BEARER_INTERVAL, TIMER_ONE_SHOT);
		}

	}
		break;

	default:
		break;
	}
}


void sys_irq_sim(){
	uint8_t c = sim_getc();

	recv_data.data[recv_data.data_index] = c;
	recv_data.data_index++;
	recv_data.data[recv_data.data_index] = 0;

	USART_SendData(USART1, (uint8_t)c);
	while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
}

void clear_data(recv_data_t * dt){
	mem_set(dt, 0, sizeof(recv_data_t));
}

